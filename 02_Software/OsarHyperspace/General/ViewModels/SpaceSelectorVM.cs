﻿/*****************************************************************************************************************************
 * @file        SpaceSelector.cs                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.12.2020                                                                                                   *
 * @brief       Implementation of the view model for the corresponding view. Using the GalaSoft.MvvmLight.Messaging feature. *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.General.ViewModels
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using OsarHyperspace.General.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General.ViewModels
{
  public class SpaceSelectorVM : ViewModelBase
  {
    public SpaceSelectorModel spaceSelectorData = new SpaceSelectorModel();

    /**
     * @brief Constructor
     */
    public SpaceSelectorVM()
    {
    }

    /**
     * @brief       API to report that the space selection is done
     */
    public object ReportSpaceSelectionDone()
    {
      var msg = new GoToPageMessage() { Command = "SpaceSelectionDone" };
      Messenger.Default.Send<GoToPageMessage>(msg);
      return null;
    }
  }
}
/**
* @}
*/
