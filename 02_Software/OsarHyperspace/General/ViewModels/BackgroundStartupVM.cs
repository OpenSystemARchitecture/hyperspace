﻿/*****************************************************************************************************************************
 * @file        BackgroundStartupVM.cs                                                                                       *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.12.2020                                                                                                   *
 * @brief       Implementation of the view model for the corresponding view. Using the GalaSoft.MvvmLight.Messaging feature. *
 *                                                                                                                           *
 * @details     Actions to be done:                                                                                          *
 *              - Download latest RteLib Dll                                                                                 *
 *              - Download latest OsarResources Dll                                                                          *
 *              - Download latest OsarSourceFileLib Dll                                                                      *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.General.ViewModels
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using OsarHyperspace.General.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using OsarHyperspace.General;
using System.ComponentModel;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General.ViewModels
{
  public class BackgroundStartupVM : BaseViewModel
  {
    string activeAction = "";
    List<string> CommandStrings = new List<string> {
      { "Check for update tool"},
      { "Download RteLib dll"},
      { "Download OsarResources dll"},
      { "Download OsarSourceFileLib dll"},
      { "Background Tasks Done"}};

    ThreadStart backgroundWorkerThreadDel;
    Thread backgroundWorkerThread;


    enum Commands
    {
      CheckUpdateTool = 0,
      DownloadRteLib,
      DownloadOsarResources,
      DownloadOsarSourceFileLib,
      Done,
    }

    enum CommandStatus
    {
      In_Progress = 0,
      Failed,
      Done,
      Nothing
    }

    public BackgroundStartupVM() : base()
    {
      backgroundWorkerThreadDel = new ThreadStart(BackgroundWorkerTask);
      backgroundWorkerThread = new Thread(backgroundWorkerThreadDel);
      backgroundWorkerThread.SetApartmentState(ApartmentState.STA);
      backgroundWorkerThread.Start();
    }

    #region Properties
    public string ActiveAction
    {
      get { return activeAction; }
      set { activeAction = value; }
    }
    #endregion

    /**
     * @brief     Task to download the dependency tools
     */
    private void BackgroundWorkerTask()
    {
      Commands activeCommand = Commands.CheckUpdateTool;
      Process wget = new Process();
      ProcessStartInfo startInfo = new ProcessStartInfo();
      startInfo.WindowStyle = ProcessWindowStyle.Hidden;
      startInfo.FileName = "wget.exe";
      startInfo.UseShellExecute = true;
      startInfo.RedirectStandardOutput = false;
      startInfo.WorkingDirectory = HyperspaceConfig.Instance().GetAbsoluteHyperspaceConfigDirectory;
      wget.StartInfo = startInfo;

      try
      {
        /* Check for wget tool */
        activeCommand = Commands.CheckUpdateTool;
        SetCommand(activeCommand, CommandStatus.In_Progress);
        startInfo.Arguments = "wget";
        wget.Start();
        SetCommand(activeCommand, CommandStatus.Done);

        /* Download RteLib */
        activeCommand = Commands.DownloadRteLib;
        SetCommand(activeCommand, CommandStatus.In_Progress);
        startInfo.Arguments = GeneralRessource.HyperspaceUpdateServer +
          GeneralRessource.HyperspaceUpdateServer_LatestRteLibPath +
          " -r --no-parent -nH --cut-dirs=9 --proxy=off -R \"index.html * \"";
        wget.Start();
        wget.WaitForExit();
        if (0 == wget.ExitCode)
        { SetCommand(activeCommand, CommandStatus.Done); }
        else
        {
          SetCommand(activeCommand, CommandStatus.Failed);
          HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
            HyperspaceExceptions.W0003, HyperspaceExceptions.W0003_HsLibraryUpdateFailed + "RteLib.dll");
        }

        /* Download OsarResources */
        activeCommand = Commands.DownloadOsarResources;
        SetCommand(activeCommand, CommandStatus.In_Progress);
        startInfo.Arguments = GeneralRessource.HyperspaceUpdateServer +
          GeneralRessource.HyperspaceUpdateServer_LatestOsarResourcesPath +
          " -r --no-parent -nH --cut-dirs=9 --proxy=off -R \"index.html * \"";
        wget.Start();
        wget.WaitForExit();
        if (0 == wget.ExitCode)
        { SetCommand(activeCommand, CommandStatus.Done); }
        else
        {
          SetCommand(activeCommand, CommandStatus.Failed);
          HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
            HyperspaceExceptions.W0003, HyperspaceExceptions.W0003_HsLibraryUpdateFailed + "RteLib.dll");
        }

        /* Download OsarSourceFileLib */
        activeCommand = Commands.DownloadOsarSourceFileLib;
        SetCommand(activeCommand, CommandStatus.In_Progress);
        startInfo.Arguments = GeneralRessource.HyperspaceUpdateServer +
          GeneralRessource.HyperspaceUpdateServer_LatestOsarResourcesPath +
          " -r --no-parent -nH --cut-dirs=9 --proxy=off -R \"index.html * \"";
        wget.Start();
        wget.WaitForExit();
        if (0 == wget.ExitCode)
        { SetCommand(activeCommand, CommandStatus.Done); }
        else
        {
          SetCommand(activeCommand, CommandStatus.Failed);
          HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
            HyperspaceExceptions.W0003, HyperspaceExceptions.W0003_HsLibraryUpdateFailed + "RteLib.dll");
        }
      }
      catch(System.ComponentModel.Win32Exception)
      {
        /* Download tool not found */
        SetCommand(activeCommand, CommandStatus.Failed);
        HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
          General.HyperspaceExceptions.W0000, HyperspaceExceptions.W0000_UpdateToolNotFound);

        /* Download tool failure */
        SetCommand(activeCommand, CommandStatus.Failed);
        HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
          General.HyperspaceExceptions.W0001, HyperspaceExceptions.W0001_HsLibraryUpdateSkipped);
      }
      catch(Exception)
      {
        SetCommand(activeCommand, CommandStatus.Failed);
        HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
          General.HyperspaceExceptions.W0002, HyperspaceExceptions.W0002_UnknownUpdateToolFailure);

        /* Download tool failure */
        SetCommand(activeCommand, CommandStatus.Failed);
        HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
          General.HyperspaceExceptions.W0001, HyperspaceExceptions.W0001_HsLibraryUpdateSkipped);
      }

      SetCommand(Commands.Done, CommandStatus.Nothing);
      Thread.Sleep(200);

      // Report finish to main view
      ReportBackgroundStuffDone();
    }

    /**
     * @brief     Helper function to set a command
     * @param[in] command: Defined command to be set
     * @param[in] status: Command status
     */
    private void SetCommand(Commands command, CommandStatus status)
    {
      SetCommandString(CommandStrings[Convert.ToInt32(command)], status);
    }

    /**
     * @brief     Helper function to set an unified info string
     * @param[in] Command
     * @Param[in] Command status
     */
    private void SetCommandString(string command, CommandStatus status)
    {
      string commandString = "";
      commandString += command;
      commandString += new string(' ', ( 31 - command.Length));
      switch (status)
      {
        case CommandStatus.In_Progress:
        commandString += "> In Progress";
        break;

        case CommandStatus.Done:
        commandString += "> Done";
        break;

        case CommandStatus.Failed:
        commandString += "> Failed";
        break;
      }

      ActiveAction = commandString;

      OnPropertyChanged(new PropertyChangedEventArgs("ActiveAction"));
    }

    /**
     * @brief       API to report that the background stuff is done
     */
    public object ReportBackgroundStuffDone()
    {
      var msg = new GoToPageMessage() { Command = "BackgroundStartupDone" };
      Messenger.Default.Send<GoToPageMessage>(msg);
      return null;
    }

  }
}
/**
* @}
*/
