﻿/*****************************************************************************************************************************
 * @file        HyperspaceConfig.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the different Hyperspace tool-chain configuration settings                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.General
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General
{
  /**
    * @brief    Static public Hyperspace internal configuration file
    * @note     Implementation as singleton
    */
  public class HyperspaceConfig
  {
    private static HyperspaceConfig instance = null;
    private string lastUsedProjectFile = "";

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /**
      * @brief      Static HyperspaceConfig class constructor
      * @param      None
      * @retval     None
      * @note       Read the Hyperspace internal configuration file
      */
    public static HyperspaceConfig Instance()
    {
      if(null == instance)
      {
        instance = new HyperspaceConfig(true);
      }
      return instance;
    }

    /**
      * @brief      Functionality to store the Hyperspace internal configuration file
      * @param      None
      * @retval     None
      * @note       Store the Hyperspace internal configuration file
      */
    public void StoreHyperspaceConfigFile()
    {
      XmlSerializer writer = new XmlSerializer(this.GetType());
      StreamWriter file = new StreamWriter(General.GeneralRessource.HyperspaceConfigFile);
      writer.Serialize(file, this);
      file.Close();
    }

    /// <summary>
    /// Element access interface LastUsedProjectFile
    /// </summary>
    public string LastUsedProjectFile
    {
      get { return lastUsedProjectFile; }
      set { lastUsedProjectFile = value; }
    }

    /// <summary>
    /// Getter for the Hyperspace Config File Directory
    /// </summary>
    public string GetAbsoluteHyperspaceConfigDirectory
    {
      get { return Directory.GetCurrentDirectory(); }
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /**
      * @brief      Constructor
      * @param      bool decide if config file shall be read
      * @retval     None
      * @note       Read the Hyperspace internal configuration file
      */
    private HyperspaceConfig(bool readConfigFile)
    {
      if(true == readConfigFile)
      {
        ReadHyperspaceConfigFile();
      }
    }

    /**
      * @brief      Constructor
      * @param      None
      * @retval     None
      */
    private HyperspaceConfig()
    {
    }

    /**
      * @brief      Functionality to read the Hyperspace internal configuration file
      * @param      None
      * @retval     None
      * @note       Read the Hyperspace internal configuration file
      */
    private void ReadHyperspaceConfigFile()
    {
      /* Check if config file dose exist */
      if(File.Exists(General.GeneralRessource.HyperspaceConfigFile))
      {
        HyperspaceConfig tempConfig;
        XmlSerializer reader = new XmlSerializer(this.GetType());
        StreamReader file = new StreamReader(General.GeneralRessource.HyperspaceConfigFile);
        tempConfig = (HyperspaceConfig)reader.Deserialize(file);
        file.Close();

        this.LastUsedProjectFile = tempConfig.LastUsedProjectFile;
      }
      else
      {
        /* Create default Project Configuration */
        this.LastUsedProjectFile = General.GeneralRessource.DefHyperspaceConfig_LastProjectFile;

        /* Store active file configuration */
        StoreHyperspaceConfigFile();
      }
    }
  }
}
/**
* @}
*/
