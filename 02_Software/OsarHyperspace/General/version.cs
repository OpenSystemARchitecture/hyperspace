/******************************************************************************
 * @file      version.cs
 * @author    OSAR Team S.Reinemuth
 * @proj      OsarHyperspace
 * @date      Sunday, January 17, 2021
 * @version   Application v. 1.11.1.7
 * @version   Generator   v. 1.2.5.1
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.11.1.7")]

namespace OsarHyperspace.General
{
  static public class OsarHyperspaceVersionClass
  {
    public static int major { get; set; }  //Version of the program
    public static int minor { get; set; }  //Sub version of the program
    public static int patch { get; set; }  //Debug patch of the program
    public static int build { get; set; }  //Count program builds

    static OsarHyperspaceVersionClass()
    {
			major = 1;
			minor = 11;
			patch = 1;
			build = 7;
    }

    public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
  }
}
//!< @version 0.1.0	->	Project setup.
//!< @version 0.2.0	->	Adding startup action selector GUI and space selector GUI.
//!< @version 0.3.0	->	Adding basic module selector functionality.
//!< @version 0.4.0	->	Dynamic loading of module libraries and showing its configuration view is working.
//!< @version 0.5.0	->	Adding project setting interfaces to convert relative project setting paths into absolute ones. Now relative path usage in project setting files is possible.
//!< @version 0.5.1	->	Adding Configuration Space Menu Point: Project Settings together with internal Project Settings "General" View.
//!< @version 0.5.2	->	Adding module configuration to project setting menu.
//!< @version 0.5.3	->	Adding new module validation and generation UI. This UI is accessible using the Menu- or Symbol-Bar.
//!< @version 0.5.4	->	Adding Logging Sub System. Attach Module Generation and Validation to Logging Sub System.
//!< @version 1.0.0	->	Adding menu item help.about. Now full initial functionality is available.
//!< @version 1.0.1	->	Fix startup welcome string.
//!< @version 1.0.2	->	Adding exception handling for third party module access.
//!< @version 1.1.0	->	Adding new Configuration-Space startup window. Adapting system startup behavior.
//!< @version 1.1.1	->	Make module selector and log output re-sizable.
//!< @version 1.2.0	->	Add generation priority setting. Also call generator now due to their priority.
//!< @version 1.3.0	->	Adding creation of log file for project logs and system logs. Fix status update during generation / validation.
//!< @version 1.4.0	->	Added automatically update op the used libraries.
//!< @version 1.4.1	->	Adding initial layout of the design space.
//!< @version 1.4.2	->	Changed file locations of different functionalities, to cleanup project structure.
//!< @version 1.4.3	->	Create basic Design Space Project Settings Menu.
//!< @version 1.4.4	->	Add central data storage for the design space.
//!< @version 1.5.0	->	Adding Display of Data Bar
//!< @version 1.6.0	->	Adding Element Menu. Adding Handling of Data-Types (Creation / Modification / Deletion).
//!< @version 1.6.1	->	Bugfix for design space config in hyperspace config file. Create default value.
//!< @version 1.7.0	->	Adding Element Menu. Adding Handling of Sender Receiver Interface Blueprint (Creation / Modification / Deletion).
//!< @version 1.7.1	->	Bugfix for naming changes in rte-lib. Add exception handling for generation and validation also as for load and store of rte file.
//!< @version 1.8.0	->	Adding Element Menu. Adding Handling of Client Server Interface Blueprint (Creation / Modification / Deletion).
//!< @version 1.9.0	->	Adding Element Menu. Adding Handling of Components (Creation / Modification / Deletion).
//!< @version 1.10.0	->	Update to RteLib version 1.0.0
//!< @version 1.11.0	->	Adding Element Menu. Adding Handling of Port Connections (Creation / Deletion).
//!< @version 1.11.1	->	Fix MiB Modify element bug. Store now just one element!
