﻿/*****************************************************************************************************************************
 * @file        HyperspaceSystem.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        12.09.2019                                                                                                   *
 * @brief       Implementation of the different Hyperspace global system behaviors. Implementation as singleton              *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.General
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarHyperspace.ConfigurationSpace.MainWindow;
using OsarHyperspace.ConfigurationSpace.MainWindow.Views;
using OsarHyperspace.General.Interfaces;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General
{
  /// <summary>
  /// Singleton instance of the Hyperspace System class. Should be accessed for system global event handling's
  /// </summary>
  public class HyperspaceSystem
  {
    private static HyperspaceSystem instance = null;
    private List<Tuple<string, string>> globleSystemErrorList = new List<Tuple<string, string>>();
    private List<Tuple<string, string>> globleSystemWarningList = new List<Tuple<string, string>>();
    private ConfigurationSpace.MainWindow.ConfigurationSpace hyperspaceCfgSpace;
    private DesignSpace.MainWindow.DesignSpace hyperspaceDesignSpace;
    //TODO Add also design main window view

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/

    /// <summary>
    /// Singleton interface to access Hyperspace System
    /// </summary>
    /// <returns></returns>
    public static HyperspaceSystem Instance()
    {
      if(null == instance)
      {
        instance = new HyperspaceSystem();
      }

      return instance;
    }
    
    /// <summary>
    /// Interface to inform the system about a change in the project configuration
    /// </summary>
    public void EventProjectConfigurationChanged()
    {
      if (null != hyperspaceCfgSpace)
      {
        // Inform Configuration space about a project configuration change.
        hyperspaceCfgSpace.EventProjectConfigurationChanged();
      }
    }

    

    #region Hyperspace Configuration-Space
    /// <summary>
    /// Setter for the configuration space main window view
    /// </summary>
    public ConfigurationSpace.MainWindow.ConfigurationSpace HyperspaceConfigurationSpace
    { set => hyperspaceCfgSpace = value; }

    /// <summary>
    /// Getter for Configuration Space Project Log Instance
    /// </summary>
    /// <returns></returns>
    public ProjectLoggingInterface ConfigSpaceProjectLogInstance()
    {
      return hyperspaceCfgSpace.GetProjectLogInstance();
    }

    /// <summary>
    /// Getter for Configuration Space System Log Instance
    /// </summary>
    /// <returns></returns>
    public SystemLoggingInterface ConfigSpaceSystemLogInstance()
    {
      return hyperspaceCfgSpace.GetSystemLogInstance();
    }

    /// <summary>
    /// Interface to close the Configuration-Space Main Window
    /// </summary>
    /// <returns></returns>
    public void ShutdownConfigurationSpace()
    {
      hyperspaceCfgSpace.Close();
    }
    #endregion

    #region Hyperspace Design-Space
    /// <summary>
    /// Setter for the design space main window view
    /// </summary>
    public DesignSpace.MainWindow.DesignSpace HyperspaceDesignSpace
    { set => hyperspaceDesignSpace = value; }

    /// <summary>
    /// Getter for Design Space Project Log Instance
    /// </summary>
    /// <returns></returns>
    public ProjectLoggingInterface DesignSpaceProjectLogInstance()
    {
      return hyperspaceDesignSpace.GetProjectLogInstance();
    }

    /// <summary>
    /// Getter for Design Space System Log Instance
    /// </summary>
    /// <returns></returns>
    public SystemLoggingInterface DesignSpaceSystemLogInstance()
    {
      return hyperspaceDesignSpace.GetSystemLogInstance();
    }

    /// <summary>
    /// Interface to close the Design-Space Main Window
    /// </summary>
    /// <returns></returns>
    public void ShutdownDesignSpace()
    {
      hyperspaceDesignSpace.Close();
    }

    /// <summary>
    /// Interface to inform the system about an update on the rte Data bases
    /// Note:
    /// Used in design Space
    /// </summary>
    public void EventRteDataBaseUpdated()
    {
      hyperspaceDesignSpace.EventRteDataBaseUpdated();
    }
    #endregion

    #region Global System Errors And Warnings
    /// <summary>
    /// Getter for the globleSystemErrorList attribute
    /// </summary>
    public List<Tuple<string, string>> GlobleSystemErrorList
    {
      get { return globleSystemErrorList; }
    }

    /// <summary>
    /// Getter for the globleSystemWarningList attribute
    /// </summary>
    public List<Tuple<string, string>> GlobleSystemWarningList
    {
      get { return globleSystemWarningList; }
    }

    /// <summary>
    /// Interface to add a new Global System Error Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewGlobalSystemErrorLog(string logName, string logContent)
    {
      globleSystemErrorList.Add(new Tuple<string, string>(logName, logContent));
    }

    /// <summary>
    /// Interface to add a new Global System Warning Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewGlobalSystemWarningLog(string logName, string logContent)
    {
      globleSystemWarningList.Add(new Tuple<string, string>(logName, logContent));
    }
    #endregion

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Private default constructor
    /// </summary>
    private HyperspaceSystem()
    {
    }
  }
}
/**
* @}
*/
