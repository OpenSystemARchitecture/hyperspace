﻿/*****************************************************************************************************************************
 * @file        CfgSpace_ModuleCfg.cs                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        28.03.2019                                                                                                   *
 * @brief       Implementation of the Data module for the used modules from the configuration space                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.General
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteConfig;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General
{
  /// <summary>
  /// Implementation of the Data Model for the Used Module Structure
  /// </summary>
  public class CfgSpace_ModuleCfg
  {

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Interface for Module Name
    /// </summary>
    public String ModuleName { get; set; }

    /// <summary>
    /// Interface for Module Type
    /// </summary>
    public RteConfigModuleTypes ModuleType { get; set; }

    /// <summary>
    /// Interface for the module base folder
    /// </summary>
    public String ModuleBaseFolder { get; set; }

    /// <summary>
    /// Interface for the Module Library Path
    /// </summary>
    public String ModuleLibPath { get; set; }

    /// <summary>
    /// Interface for the module generation priority
    /// </summary>
    public UInt16 ModuleGenerationPriority { get; set; }

    /// <summary>
    /// Interface for the Configuration File Path
    /// </summary>
    public String CfgFilePath { get; set; }

    /// <summary>
    /// Interface to select if the module shall be generated and validated
    /// </summary>
    public bool ValidateAndGenerateModule { get; set; }
  }
}
/**
* @}
*/
