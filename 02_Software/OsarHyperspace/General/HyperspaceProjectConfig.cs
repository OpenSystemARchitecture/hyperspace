﻿/*****************************************************************************************************************************
 * @file        HyperspaceProjectConfig.cs                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the different Hyperspace project configuration settings                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.General
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using OsarHyperspace.ConfigurationSpace.MainWindow;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General
{
  public class DesignSpaceConfig
  {
    public string rteConfigFilePath = "";
    public string rteInterfaceFilePath = "";
    public string rteTypesFilePath = "";
  }

  /**
    * @brief    Static public Hyperspace internal configuration file
    * @note     Implementation as singleton
    */
  public class HyperspaceProjectConfig
  {
    private static HyperspaceProjectConfig instance = null;
    private string hyperspaceProjectConfigFilePath = "";
    private string baseProjectFolderPath = "";
    private string projectName = "";
    private List<CfgSpace_ModuleCfg> cfgSpaceModuleList = new List<CfgSpace_ModuleCfg>();
    private DesignSpaceConfig designSpaceConfig = new DesignSpaceConfig();

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    #region Project Element Properties Interfaces
    /// <summary>
    /// Element access interface for the projectName
    /// </summary>
    public string ProjectName { get => projectName; set => projectName = value; }

    /// <summary>
    /// Element access interface BaseProjectFolderPath
    /// </summary>
    public string BaseProjectFolderPath
    {
      get { return baseProjectFolderPath; }
      set { baseProjectFolderPath = value; }
    }

    /// <summary>
    /// Element access interface for CfgSpaceModuleList
    /// </summary>
    public List<CfgSpace_ModuleCfg> CfgSpaceModuleList
    {
      get { return cfgSpaceModuleList; }
      set { cfgSpaceModuleList = value; }
    }

    /// <summary>
    /// Element access interface for DesignSpaceConfiguration
    /// </summary>
    public DesignSpaceConfig DesignSpaceConfiguration
    {
      get { return designSpaceConfig; }
      set { designSpaceConfig = value; }
    }
    #endregion

    #region General Interfaces
    /**
    * @brief      Static HyperspaceConfig class constructor
    * @param      None
    * @retval     None
    * @note       Read the Hyperspace internal configuration file
    */
    public static HyperspaceProjectConfig Instance()
    {
      if (null == instance)
      {
        /* Throw not initialized exception */
        throw new NullReferenceException();
      }
      else
      {
        return instance;
      }
    }

    /**
    * @brief      Static HyperspaceConfig class constructor
    * @param      None
    * @retval     None
    * @note       Read the Hyperspace internal configuration file
    */
    public static HyperspaceProjectConfig Instance(string hyperspaceProjectFilePath)
    {
      if(null == instance)
      {
        instance = new HyperspaceProjectConfig(hyperspaceProjectFilePath);
      }

      return instance;
    }

    /**
      * @brief      Functionality to store the Hyperspace project configuration file
      * @param      None
      * @retval     None
      * @note       Store the Hyperspace internal configuration file
      */
    public void StoreHyperspaceProjectConfigFile()
    {
      XmlSerializer writer = new XmlSerializer(this.GetType());
      StreamWriter file = new StreamWriter(hyperspaceProjectConfigFilePath);
      writer.Serialize(file, this);
      file.Close();
    }

    /**
      * @brief      Functionality to read the Hyperspace project configuration file
      * @param      None
      * @retval     None
      * @note       Read the Hyperspace internal configuration file
      */
    public void ReadHyperspaceProjectConfigFile()
    {
      /* Check if config file dose exist */
      if (File.Exists(hyperspaceProjectConfigFilePath))
      {
        HyperspaceProjectConfig tempConfig;
        XmlSerializer reader = new XmlSerializer(this.GetType());
        StreamReader file = new StreamReader(hyperspaceProjectConfigFilePath);
        tempConfig = (HyperspaceProjectConfig)reader.Deserialize(file);
        file.Close();

        this.BaseProjectFolderPath = tempConfig.BaseProjectFolderPath;
        this.CfgSpaceModuleList = tempConfig.CfgSpaceModuleList;
        this.ProjectName = tempConfig.ProjectName;
        this.designSpaceConfig = tempConfig.designSpaceConfig;
      }
      else
      {
        /* Create default Project Configuration */
        this.BaseProjectFolderPath = "C:";

        //DEBUG: Adding module for debugging >> Remove after debugging
        //CfgSpace_ModuleCfg detTestModule = new CfgSpace_ModuleCfg();
        //detTestModule.CfgFilePath = "C:\\Temp\\DemoHyperspaceProject\\BSW\\Det\\01_Generator\\DetConfigXml.xml";
        //detTestModule.ModuleLibPath = "C:\\Temp\\DemoHyperspaceProject\\BSW\\Det\\01_Generator\\ModuleLibrary.dll";
        //detTestModule.ModuleBaseFolder = "C:\\Temp\\DemoHyperspaceProject\\BSW\\Det";
        //detTestModule.ModuleName = "Det";
        //detTestModule.ModuleType = RteLib.RteConfig.RteConfigModuleTypes.BSW;
        //CfgSpaceModuleList.Add(detTestModule);

        /* Store active file configuration */
        StoreHyperspaceProjectConfigFile();
      }
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Interface to request the absolute base project path. If the path is already absolute,
    /// it would be return without any modification. If an relative path is given,
    /// it would be converted to an absolute by following sequence:
    /// "hyperspaceProjectConfigFilePath" + "baseProjectFolderPath"
    /// </summary>
    /// <returns>Absolute path string</returns>
    public string GetAbsoluteBaseProjectPath()
    {
      string retVal, path;

      if (false == Path.IsPathRooted(baseProjectFolderPath))
      {
        path = Path.GetDirectoryName(hyperspaceProjectConfigFilePath);
        path = path + ".\\" + baseProjectFolderPath;
        var folderUri = new Uri(path , UriKind.Absolute);
        retVal = folderUri.AbsolutePath;
      }
      else
      {
        retVal = baseProjectFolderPath;
      }

      return retVal;
    }

    /// <summary>
    /// Interface to create an relative path from an absolute one based on the Project File Path
    /// </summary>
    /// <param name="absoutePath"></param>
    /// <returns></returns>
    public string CreateRelativePathToProjectConfigFile(string absoutePath)
    {
      string retVal = absoutePath;
      if (true == System.IO.Path.IsPathRooted(absoutePath))
      {
        var fileUri = new Uri(absoutePath, UriKind.Absolute);

        var referenceUri = new Uri(GetAbsoluteBaseProjectPath(), UriKind.Absolute);
        string relativePath = "./" + referenceUri.MakeRelativeUri(fileUri).ToString();

        // Set path in project file
        absoutePath = relativePath;
      }

      return absoutePath;
    }
    #endregion

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /**
      * @brief      Constructor
      * @param      None
      * @retval     None
      */
    private HyperspaceProjectConfig()
    { }

    /**
      * @brief      Constructor
      * @param      bool decide if config file shall be read
      * @retval     None
      * @note       Read the Hyperspace project configuration file
      */
    private HyperspaceProjectConfig(string hyperspaceProjectFilePath)
    {
      if ("" == hyperspaceProjectFilePath)
      {
        throw new ArgumentOutOfRangeException();
      }
      else
      {
        hyperspaceProjectConfigFilePath = hyperspaceProjectFilePath;
        ReadHyperspaceProjectConfigFile();
      }
    }
  }
}
/**
* @}
*/
