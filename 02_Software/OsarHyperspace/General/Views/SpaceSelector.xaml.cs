﻿/*****************************************************************************************************************************
 * @file        SpaceSelector.xaml.cs                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.12.2020                                                                                                   *
 * @brief       Implementation of the view for the space selection.                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.General.Views
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General.Views
{
  /// <summary>
  /// Interaction logic for SpaceSelector.xaml
  /// </summary>
  public partial class SpaceSelector : UserControl
  {
    private ViewModels.SpaceSelectorVM spaceSelectorVM = new ViewModels.SpaceSelectorVM();

    /**
     * @brief     Constructor
     */
    public SpaceSelector()
    {
      InitializeComponent();
    }

    #region UI Elements
    /**
      * @brief      Button Start Design Space
      * @param      None
      * @retval     None
      */
    private void B_DesignSpace_Click(object sender, RoutedEventArgs e)
    {
      spaceSelectorVM.spaceSelectorData.StartDesignSpace = true;
      spaceSelectorVM.ReportSpaceSelectionDone();
    }

    /**
      * @brief      Button Start Configuration Space
      * @param      None
      * @retval     None
      */
    private void B_ConfigurationSpace_Click(object sender, RoutedEventArgs e)
    {
      spaceSelectorVM.spaceSelectorData.StartConfigurationSpace = true;
      spaceSelectorVM.ReportSpaceSelectionDone();
    }
    #endregion

    #region Attributes
    /**
     * @brief Getter for the space selector data
     */
    public Models.SpaceSelectorModel SpaceSelectorData
    {
      get { return spaceSelectorVM.spaceSelectorData; }
    }
    #endregion
  }
}
/**
* @}
*/
