﻿/*****************************************************************************************************************************
 * @file        HyperspaceConfigSelector.xaml.cs                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the selection for the used Hyperspace project config                                       *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.General.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using OsarHyperspace.General.ViewModels;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General.Views
{
  /// <summary>
  /// Interaction logic for HyperspaceConfigSelector.xaml
  /// </summary>
  public partial class HyperspaceConfigSelector : UserControl
  {
    private HyperspaceConfigSelectorVM hsCfgFileSelectorVm = new HyperspaceConfigSelectorVM();

    public HyperspaceConfigSelector()
    {
      InitializeComponent();
    }

    #region UI Elements
    /**
      * @brief      Button Create new Project
      * @param      None
      * @retval     None
      * @note       Specify the Project storage location
      */
    private void B_CreateNewProject_Click(object sender, RoutedEventArgs e)
    {
      /* Open Store File selection dialog */
      SaveFileDialog saveFileDialog = new SaveFileDialog();
      saveFileDialog.DefaultExt = General.GeneralRessource.OsarHyperspaceProjectFileEnding;

      saveFileDialog.Filter = "Project File |*" + General.GeneralRessource.OsarHyperspaceProjectFileEnding;
      saveFileDialog.Title = "Select storage point of Hyperspace Project File";
      if (true == saveFileDialog.ShowDialog())
      {
        General.HyperspaceProjectConfig.Instance(saveFileDialog.FileName);
        General.HyperspaceConfig.Instance().LastUsedProjectFile = saveFileDialog.FileName;
        General.HyperspaceConfig.Instance().StoreHyperspaceConfigFile();
      }

      hsCfgFileSelectorVm.ReportHsCfgFileSelectionDone();
    }

    /**
      * @brief      Select existing Hyperspace Project
      * @param      None
      * @retval     None
      */
    private void B_SelectProject_Click(object sender, RoutedEventArgs e)
    {
      /* Open File selection dialog */
      Microsoft.Win32.OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.DefaultExt = General.GeneralRessource.OsarHyperspaceProjectFileEnding;
      openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(General.HyperspaceConfig.Instance().LastUsedProjectFile);
      openFileDialog.FileName = System.IO.Path.GetFileName(General.HyperspaceConfig.Instance().LastUsedProjectFile);
      openFileDialog.Filter = "Project File |*" + General.GeneralRessource.OsarHyperspaceProjectFileEnding;
      openFileDialog.Title = "Select Hyperspace Project File";

      if (true == openFileDialog.ShowDialog())
      {
        /* Store new last used project file */
        General.HyperspaceConfig.Instance().LastUsedProjectFile = openFileDialog.FileName;
        General.HyperspaceConfig.Instance().StoreHyperspaceConfigFile();

        /* Load Project file */
        General.HyperspaceProjectConfig.Instance(openFileDialog.FileName);
      }

      hsCfgFileSelectorVm.ReportHsCfgFileSelectionDone();
    }
    #endregion
  }
}
/**
* @}
*/
