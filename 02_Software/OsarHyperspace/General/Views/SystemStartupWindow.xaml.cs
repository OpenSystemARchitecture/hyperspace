﻿/*****************************************************************************************************************************
 * @file        SystemStartupWindow.xaml.cs                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the System Startup window. Using the GalaSoft.MvvmLight.Messaging feature.                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.General.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OsarHyperspace.General.ViewModels;
using OsarHyperspace.General.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General.Views
{
  /// <summary>
  /// Interaction logic for SystemStartupWindow.xaml
  /// </summary>
  public partial class SystemStartupWindow : Window
  {
    private SpaceSelector spaceSelectorView = new SpaceSelector();
    private HyperspaceConfigSelector hsCfgSelectorView = new HyperspaceConfigSelector();
    private BackgroundStartup backgroundStartup = new BackgroundStartup();

    /**
     * @brief     Constructor
     */
    public SystemStartupWindow()
    {
      InitializeComponent();

      //CC_SystemStartup.DataContext = hsCfgSelectorView;
      this.WindowStyle = WindowStyle.None;
      CC_SystemStartup.DataContext = backgroundStartup;

      Messenger.Default.Register<GoToPageMessage>(this, (action) => ReceiveMessage(action));
    }

    #region Access Attributes
    /**
     * @brief     API to access the spaceSelector information
     */
    public Models.SpaceSelectorModel SpaceSelectorData
    {
      get { return spaceSelectorView.SpaceSelectorData; }
    }
    #endregion

    /**
     * @brief   Message receiver from the different ContentControls
     * @details Using GalaSoft.MvvmLight.Messaging mechanism
     *          Perform also content change in this function
     */
    private object ReceiveMessage(GoToPageMessage action)
    {
      switch (action.Command)
      {
        case "BackgroundStartupDone":
          {
            Dispatcher.Invoke(() =>
            {
              this.WindowStyle = WindowStyle.ToolWindow;
              CC_SystemStartup.DataContext = hsCfgSelectorView;
            });
            
          }
        break;

        case "HsCfgFileSelectionDone":
          {
            CC_SystemStartup.DataContext = spaceSelectorView;
          }
        break;
        case "SpaceSelectionDone":
          {
            // Everything done >> Close window
            this.Close();
          }
        break;
        default:
          {
            // Failure >> Close window
            this.Close();
          }
        break;
      }
      return null;
    }
  }
}
/**
* @}
*/
