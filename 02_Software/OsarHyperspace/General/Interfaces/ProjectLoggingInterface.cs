﻿/*****************************************************************************************************************************
 * @file        CS_ProjectLoggingInterface.cs                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        20.09.2019                                                                                                   *
 * @brief       Definition of the project logging interfaces                                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.General.Interfaces
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General.Interfaces
{
  /// <summary>
  /// Interface defining necessary system APIs to access the project logging system
  /// </summary>
  public interface ProjectLoggingInterface
  {
    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Interface to add a new Project Error Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    void AddNewProjectErrorLog(string logName, string logContent);

    /// <summary>
    /// Interface to add a new Project Warning Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    void AddNewProjectWarningLog(string logName, string logContent);

    /// <summary>
    /// Interface to add a new Project Info Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    void AddNewProjectInfoLog(string logName, string logContent);

    /// <summary>
    /// Interface to add a new Project Logging Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    void AddNewProjectLoggingLog(string logName, string logContent);

    /// <summary>
    /// Interface to add a new Project "all types" Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    void AddNewProjectAllTypesLog(string logName, string logContent);

    /// <summary>
    /// Interface to clear the project logs
    /// </summary>
    void ClearProjectLogs();
    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/