﻿/*****************************************************************************************************************************
 * @file        PageChangeMessage.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.12.2020                                                                                                   *
 * @brief       Implementation of functionalities from the main module.                                                      *
 *                                                                                                                           *
 * @details     Implementation of the message exchange data type for the GalaSoft.MvvmLight.Messaging                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.General.Models
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsarHyperspace.General.Models
{
  public class GoToPageMessage
  {
    /**
     * @brief   Attribute for the command string
     */
    public string Command { get; set; }
  }
}
/**
* @}
*/
