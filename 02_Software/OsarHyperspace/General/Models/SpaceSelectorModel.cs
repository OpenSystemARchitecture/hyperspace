﻿/*****************************************************************************************************************************
 * @file        SpaceSelectorModel.cs                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.12.2020                                                                                                   *
 * @brief       Implementation of the data set for the space selection.                                                      *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.General.Models
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsarHyperspace.General.Models
{
  public class SpaceSelectorModel
  {
    bool startConfigurationSpace = false;
    bool startDesignSpace = false;

    /**
     * @brief     Constructor
     */
    public SpaceSelectorModel()
    {
    }

    #region Access Attributes
    /**
     * @brief     API to access the startConfigurationSpace Flag
     */
    public bool StartConfigurationSpace
    {
      get { return startConfigurationSpace; }
      set { startConfigurationSpace = value; }
    }

    /**
     * @brief     API to access the startDesignSpace Flag
     */
    public bool StartDesignSpace
    {
      get { return startDesignSpace; }
      set { startDesignSpace = value; }
    }
    #endregion

  }
}
/**
* @}
*/