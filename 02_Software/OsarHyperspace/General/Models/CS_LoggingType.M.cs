﻿/*****************************************************************************************************************************
 * @file        CS_LoggingTypeM.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        20.09.2019                                                                                                   *
 * @brief       Implementation of the logging type model.                                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.General.Models
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.General.Models
{
  /// <summary>
  /// Enumeration which defines the logging type
  /// </summary>
  public enum LoggingType
  {
    ERROR = 0,
    WARNING = 1
  }

  /// <summary>
  /// Enumeration which defines logging events
  /// </summary>
  public enum LoggingControlEvents
  {
    NEW_PROJECT_LOG = 0,
    NEW_SYSTEM_LOG
  }
}
/**
* @}
*/
