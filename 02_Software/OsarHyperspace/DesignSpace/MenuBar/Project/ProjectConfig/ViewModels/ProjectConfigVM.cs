﻿/*****************************************************************************************************************************
 * @file        ProjectConfigVM.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       View Model of the new elements menu window                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Project.ProjectConfig.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using OsarHyperspace.General;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Project.ProjectConfig.ViewModels
{
  public class ProjectConfigVM : BaseViewModel
  {
    private object dataTypeContent;                                         //!< View object element for sub data type views

    private RtePresentation.RteConfig.Views.RteConnectionV rteConnectionV;


    /// <summary>
    /// Constructor
    /// </summary>
    public ProjectConfigVM()
    {
      rteConnectionV = new RtePresentation.RteConfig.Views.RteConnectionV(
        General.Models.DesignSpaceDataCenter.ModuleInternalBehaviorList,
        General.Models.DesignSpaceDataCenter.RteTypesDb,
        General.Models.DesignSpaceDataCenter.RteInterfaceDb,
        General.Models.DesignSpaceDataCenter.RteConfigDb);

      dataTypeContent = rteConnectionV;
    }

    #region Properties
    /// <summary>
    /// Getter for the sub data type view
    /// </summary>
    public object SubElementView
    {
      get { return dataTypeContent; }
    }
    #endregion
  }
}
/**
* @}
*/