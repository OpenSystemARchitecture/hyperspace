﻿/*****************************************************************************************************************************
 * @file        ProjectConfigV.xaml.cs                                                                                       *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       Window of Project Config menu part                                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Project.ProjectConfig
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Project.ProjectConfig
{
  /// <summary>
  /// Interaction logic for ProjectConfigV.xaml
  /// </summary>
  public partial class ProjectConfigV : Window
  {
    ViewModels.ProjectConfigVM usedProjectConfigVM;
    public ProjectConfigV()
    {
      usedProjectConfigVM = new ViewModels.ProjectConfigVM();

      InitializeComponent();

      this.DataContext = usedProjectConfigVM;
    }

    #region UI Actions
    /// <summary>
    /// User interface for button OK clicked event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_OK_Click(object sender, RoutedEventArgs e)
    {
      OsarHyperspace.General.HyperspaceSystem.Instance().EventRteDataBaseUpdated();
      this.Close();
    }
    #endregion
  }
}
/**
* @}
*/