﻿/*****************************************************************************************************************************
 * @file        DS_GeneralVM.cs                                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        22.12.2020                                                                                                   *
 * @brief       Implementation of the General Project Settings View Model                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/

/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Project.ProjectSettings.ViewModels
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Project.ProjectSettings.ViewModels
{
  public class DS_GeneralVM : BaseViewModel
  {
    OsarHyperspace.General.HyperspaceProjectConfig projectConfig;
    OsarHyperspace.General.DesignSpaceConfig designSpaceCfg;

    /// <summary>
    /// Constructor
    /// </summary>
    public DS_GeneralVM()
    {
      // Get Ref of Project Settings
      projectConfig = General.Models.DesignSpaceDataCenter.ProjectConfig;
      designSpaceCfg = projectConfig.DesignSpaceConfiguration;
    }

    /// <summary>
    /// Interface to initiate the shutdown procedure.
    /// Store all data in files
    /// </summary>
    public void Shutdown()
    {
      // Trigger project store operation
      projectConfig.StoreHyperspaceProjectConfigFile();
    }

    #region Properties Interfaces
    /// <summary>
    /// Element access interface for the projectName
    /// </summary>
    public string ProjectName { 
      get => projectConfig.ProjectName; 
      set => projectConfig.ProjectName = value; 
    }

    /// <summary>
    /// Element access interface ProjectBaseFolderPath
    /// </summary>
    public string ProjectBaseFolderPath
    {
      get { return projectConfig.BaseProjectFolderPath; }
      set { projectConfig.BaseProjectFolderPath = value; }
    }

    /// <summary>
    /// Element access interface RteModuleConfigFilePath
    /// </summary>
    public string RteModuleConfigFilePath
    {
      get { return designSpaceCfg.rteConfigFilePath; }
      set 
      { 
        designSpaceCfg.rteConfigFilePath = value;
        projectConfig.DesignSpaceConfiguration = designSpaceCfg;
      }
    }

    /// <summary>
    /// Element access interface RteInterfaceFilePath
    /// </summary>
    public string RteInterfaceFilePath
    {
      get { return designSpaceCfg.rteInterfaceFilePath; }
      set
      {
        designSpaceCfg.rteInterfaceFilePath = value;
        projectConfig.DesignSpaceConfiguration = designSpaceCfg;
      }
    }

    /// <summary>
    /// Element access interface RteTypesFilePath
    /// </summary>
    public string RteTypesFilePath
    {
      get { return designSpaceCfg.rteTypesFilePath; }
      set
      {
        designSpaceCfg.rteTypesFilePath = value;
        projectConfig.DesignSpaceConfiguration = designSpaceCfg;
      }
    }
    #endregion
  }
}
/**
* @}
*/