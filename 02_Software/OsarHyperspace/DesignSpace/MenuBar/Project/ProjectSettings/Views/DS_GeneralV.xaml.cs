﻿/*****************************************************************************************************************************
 * @file        DS_GeneralV.xaml.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        21.12.2020                                                                                                   *
 * @brief       Implementation of the General Project Settings View                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/

/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Project.ProjectSettings.Views
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.DesignSpace.MenuBar.Project.ProjectSettings.ViewModels;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Project.ProjectSettings.Views
{
  /// <summary>
  /// Interaction logic for DS_GeneralV.xaml
  /// </summary>
  public partial class DS_GeneralV : UserControl
  {
    DS_GeneralVM usedViewModel;

    /// <summary>
    /// Constructor
    /// </summary>
    public DS_GeneralV()
    {
      InitializeComponent();

      usedViewModel = new DS_GeneralVM();

      this.DataContext = usedViewModel;
    }

    /// <summary>
    /// Interface to signalize the window closing event
    /// </summary>
    public void CloseWindow()
    {
      usedViewModel.Shutdown();
    }

    #region UI Actions
    /// <summary>
    /// Event handler for Button "Select Base Folder Path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SelectBaseFolderPath_Click(object sender, RoutedEventArgs e)
    {
      using (var fbd = new System.Windows.Forms.FolderBrowserDialog())
      {
        System.Windows.Forms.DialogResult result = fbd.ShowDialog();

        if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
        {
          // Set path in project file
          usedViewModel.ProjectBaseFolderPath = fbd.SelectedPath;
          TB_ProjectBaseFolderPath.Text = fbd.SelectedPath;
        }
      }
    }

    /// <summary>
    /// Event handler for Button "Set Base Folder Path as relative path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SetBaseFolderPathAsRelative_Click(object sender, RoutedEventArgs e)
    {
      if(true == System.IO.Path.IsPathRooted(usedViewModel.ProjectBaseFolderPath))
      {
        usedViewModel.ProjectBaseFolderPath = OsarHyperspace.General.HyperspaceProjectConfig.Instance().
          CreateRelativePathToProjectConfigFile(usedViewModel.ProjectBaseFolderPath + "/");

        TB_ProjectBaseFolderPath.Text = usedViewModel.ProjectBaseFolderPath;
      }
    }


    /// <summary>
    /// Event handler for Button "Select Rte Config File Path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SelectRteModuleConfigFilePath_Click(object sender, RoutedEventArgs e)
    {
      /* Open File selection dialog */
      Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
      openFileDialog.DefaultExt = ".xml";
      openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(OsarHyperspace.General.HyperspaceConfig.Instance().
        LastUsedProjectFile);
      openFileDialog.FileName = "";
      openFileDialog.Filter = "Config File |*" + ".xml";
      openFileDialog.Title = "Select Rte Module Config File";

      if (true == openFileDialog.ShowDialog())
      {
        /* Store new last used project file */
        usedViewModel.RteModuleConfigFilePath = openFileDialog.FileName;
        TB_RteModuleConfigFilePath.Text = openFileDialog.FileName;
      }

    }

    /// <summary>
    /// Event handler for Button "Set Rte Config File relative path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SetRteModuleConfigFilePathAsRelative_Click(object sender, RoutedEventArgs e)
    {
      usedViewModel.RteModuleConfigFilePath = OsarHyperspace.General.HyperspaceProjectConfig.Instance().
        CreateRelativePathToProjectConfigFile(usedViewModel.RteModuleConfigFilePath);

      TB_RteModuleConfigFilePath.Text = usedViewModel.RteModuleConfigFilePath;
    }

    /// <summary>
    /// Event handler for Button "Select Rte Interface File Path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SelectRteInterfaceFilePath_Click(object sender, RoutedEventArgs e)
    {
      /* Open File selection dialog */
      Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
      openFileDialog.DefaultExt = ".xml";
      openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(OsarHyperspace.General.HyperspaceConfig.Instance().
        LastUsedProjectFile);
      openFileDialog.FileName = "";
      openFileDialog.Filter = "Config File |*" + ".xml";
      openFileDialog.Title = "Select Rte Interface Config File";

      if (true == openFileDialog.ShowDialog())
      {
        /* Store new last used project file */
        usedViewModel.RteInterfaceFilePath = openFileDialog.FileName;
        TB_RteInterfaceFilePath.Text = openFileDialog.FileName;
      }
    }

    /// <summary>
    /// Event handler for Button "Set Rte Interface File relative path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SetRteInterfaceFilePathAsRelative_Click(object sender, RoutedEventArgs e)
    {
      usedViewModel.RteInterfaceFilePath = OsarHyperspace.General.HyperspaceProjectConfig.Instance().
        CreateRelativePathToProjectConfigFile(usedViewModel.RteInterfaceFilePath);
      TB_RteInterfaceFilePath.Text = usedViewModel.RteInterfaceFilePath;

    }

    /// <summary>
    /// Event handler for Button "Select Rte Type File Path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SelectRteTypesFilePath_Click(object sender, RoutedEventArgs e)
    {
      /* Open File selection dialog */
      Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
      openFileDialog.DefaultExt = ".xml";
      openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(
        OsarHyperspace.General.HyperspaceConfig.Instance().LastUsedProjectFile);
      openFileDialog.FileName = "";
      openFileDialog.Filter = "Config File |*" + ".xml";
      openFileDialog.Title = "Select Rte Types Config File";

      if (true == openFileDialog.ShowDialog())
      {
        /* Store new last used project file */
        usedViewModel.RteTypesFilePath = openFileDialog.FileName;
        TB_RteTypesFilePath.Text = openFileDialog.FileName;
      }
    }

    /// <summary>
    /// Event handler for Button "Set Rte Type File relative path"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SetRteTypesFilePathAsRelative_Click(object sender, RoutedEventArgs e)
    {
      usedViewModel.RteTypesFilePath = OsarHyperspace.General.HyperspaceProjectConfig.Instance().
        CreateRelativePathToProjectConfigFile(usedViewModel.RteTypesFilePath);

      TB_RteTypesFilePath.Text = usedViewModel.RteTypesFilePath;
    }
    #endregion
  }
}
/**
* @}
*/