﻿/*****************************************************************************************************************************
 * @file        NewElementsV.xaml.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.12.2020                                                                                                   *
 * @brief       Window of the new elements menu part                                                                         *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Element.NewElements
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.DesignSpace.MenuBar.Element.NewElements.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Element.NewElements
{
  /// <summary>
  /// Interaction logic for NewElements.xaml
  /// </summary>
  public partial class NewElementsV : Window
  {
    ViewModels.NewElementsVM newElementsVM;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="elementType"></param>
    public NewElementsV(ElementType elementType)
    {
      newElementsVM = new ViewModels.NewElementsVM(elementType);
      InitializeComponent();

      this.DataContext = newElementsVM;
    }

    #region UI Actions
    /// <summary>
    /// User interface for button AddNewElement clicked event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewElement_Click(object sender, RoutedEventArgs e)
    {
      if(true == newElementsVM.AddElement())
      {
        OsarHyperspace.General.HyperspaceSystem.Instance().EventRteDataBaseUpdated();
        this.Close();
      }
    }
    #endregion
  }
}
/**
* @}
*/