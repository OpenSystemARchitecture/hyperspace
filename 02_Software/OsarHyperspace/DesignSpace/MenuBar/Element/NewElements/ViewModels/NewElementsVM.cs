﻿/*****************************************************************************************************************************
 * @file        NewElementsVM.cs                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.12.2020                                                                                                   *
 * @brief       View Model of the new elements menu window                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Element.NewElements.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using OsarHyperspace.General;
using OsarHyperspace.DesignSpace.MenuBar.Element.NewElements.Models;
using System.Windows;
using System.Windows.Controls;

using RtePresentation.RteTypes.Views;
using RteLib.RteTypes;

using RtePresentation.RteInterface.Views;
using RteLib.RteInterface;

using RtePresentation.RteModuleInternalBehavior.Views;
using RteLib.RteModuleInternalBehavior;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Element.NewElements.ViewModels
{
  public class NewElementsVM : BaseViewModel
  {
    private object dataTypeContent;                                 //!< View object element for sub data type views
    ElementType usedElementType;

    // Data Type View and Handling
    private RteAdvancedDataTypeV advancedDataTypeContent;                           //!< View object data type element for sub data type views
    private RteAdvancedDataTypeStructure newDataType;

    // Sender Receiver Interface Blueprint View and Handling
    private RteSRInterfaceBlueprintV srInterfaceBlueprintContent;                   //!< View object SR interface blueprint views
    private RteSRInterfaceBlueprint newSRInterfaceBlueprint;

    // Client Server Interface Blueprint View and Handling
    private RteCSInterfaceBlueprintV csInterfaceBlueprintContent;                   //!< View object CS interface blueprint views
    private RteCSInterfaceBlueprint newCSInterfaceBlueprint;

    // Module internal Behavior View and Handling
    private RteModuleInternalBehaviorV miBContent;                                  //!< View object module internal behavior views
    private RteModuleInternalBehavior newMiB;

    //TODO Add further elements

    /// <summary>
    /// Constructor
    /// </summary>
    public NewElementsVM(ElementType elementType)
    {
      usedElementType = elementType;
      dataTypeContent = new ContentControl();

      switch (usedElementType)
      {
        case ElementType.NEW_DATA_TYPE:
        newDataType = new RteAdvancedDataTypeStructure();
        advancedDataTypeContent = new RteAdvancedDataTypeV(newDataType, General.Models.DesignSpaceDataCenter.RteTypesDb);
        dataTypeContent = advancedDataTypeContent;
        break;

        case ElementType.NEW_CS_INTERFACE_BLUEPRINT:
        newCSInterfaceBlueprint = new RteCSInterfaceBlueprint();
        csInterfaceBlueprintContent = new RteCSInterfaceBlueprintV(newCSInterfaceBlueprint, General.Models.DesignSpaceDataCenter.RteTypesDb);
        dataTypeContent = csInterfaceBlueprintContent;
        break;

        case ElementType.NEW_SR_INTERFACE_BLUEPRINT:
        newSRInterfaceBlueprint = new RteSRInterfaceBlueprint();
        srInterfaceBlueprintContent = new RteSRInterfaceBlueprintV(newSRInterfaceBlueprint, General.Models.DesignSpaceDataCenter.RteTypesDb);
        dataTypeContent = srInterfaceBlueprintContent;
        break;

        case ElementType.NEW_MODULE:
        newMiB = new RteModuleInternalBehavior();
        miBContent = new RteModuleInternalBehaviorV(newMiB,
          General.Models.DesignSpaceDataCenter.RteTypesDb,
          General.Models.DesignSpaceDataCenter.RteInterfaceDb,
          General.Models.DesignSpaceDataCenter.RteConfigDb);
        dataTypeContent = miBContent;
        break;
      }
    }

    #region Properties
    /// <summary>
    /// Getter for the sub data type view
    /// </summary>
    public object SubElementView
    {
      get { return dataTypeContent; }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to add the element to the data base
    /// </summary>
    public bool AddElement()
    {
      bool retVal = false;
      switch (usedElementType)
      {
        case ElementType.NEW_DATA_TYPE:
        retVal = Hlp_AddANewAdvancedDataType();
        break;

        case ElementType.NEW_CS_INTERFACE_BLUEPRINT:
        retVal = Hlp_AddANewCSInterfaceBlueprint();
        break;

        case ElementType.NEW_SR_INTERFACE_BLUEPRINT:
        retVal = Hlp_AddANewSRInterfaceBlueprint();
        break;

        case ElementType.NEW_MODULE:
        retVal = Hlp_AddANewModuleInternalBehavior();
        break;
      }

      return retVal;
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function which adds a new data type element to the data base
    /// </summary>
    private bool Hlp_AddANewAdvancedDataType()
    {
      bool retVal = false;
      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = General.Models.DesignSpaceDataCenter.RteTypesDb.ValidateRteDataTypeElement(newDataType);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach(string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("New Data Type Validation", errorStr);
        }

        if(0 == validationErrors.Count)
        {
          General.Models.DesignSpaceDataCenter.RteTypesDb.AddAdvancedRteType(newDataType);
          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }
        
      }
      catch(ArgumentException ex)
      {
        MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK);
      }
      catch(Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    /// <summary>
    /// Helper function which adds a new sender receiver interface blueprint the data base
    /// </summary>
    private bool Hlp_AddANewSRInterfaceBlueprint()
    {
      bool retVal = false;
      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = General.Models.DesignSpaceDataCenter.RteInterfaceDb.ValidateRteSRInterfaceBlueprintElement(
          newSRInterfaceBlueprint, ref General.Models.DesignSpaceDataCenter.RteTypesDb);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach (string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("New SR Blueprint Validation", errorStr);
        }

        if (0 == validationErrors.Count)
        {
          General.Models.DesignSpaceDataCenter.RteInterfaceDb.AddRteSRInterface(newSRInterfaceBlueprint);
          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }
      }
      catch (ArgumentException ex)
      {
        MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    /// <summary>
    /// Helper function which adds a new sender receiver interface blueprint the data base
    /// </summary>
    private bool Hlp_AddANewCSInterfaceBlueprint()
    {
      bool retVal = false;
      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = General.Models.DesignSpaceDataCenter.RteInterfaceDb.ValidateRteCSInterfaceBlueprintElement(
          newCSInterfaceBlueprint, ref General.Models.DesignSpaceDataCenter.RteTypesDb);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach (string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("New CS Blueprint Validation", errorStr);
        }

        if (0 == validationErrors.Count)
        {
          General.Models.DesignSpaceDataCenter.RteInterfaceDb.AddRteCSInterface(newCSInterfaceBlueprint);
          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }
      }
      catch (ArgumentException ex)
      {
        MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    /// <summary>
    /// Helper function which adds a new module internal behavior file
    /// </summary>
    private bool Hlp_AddANewModuleInternalBehavior()
    {
      bool retVal = false;
      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = newMiB.ValidateModuleInternalBehavior(
          ref General.Models.DesignSpaceDataCenter.RteConfigDb,
          ref General.Models.DesignSpaceDataCenter.RteInterfaceDb,
          ref General.Models.DesignSpaceDataCenter.RteTypesDb);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach (string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("MiB Validation", errorStr);
        }

        if (0 == validationErrors.Count)
        {
          // Add Module internal behavior
          General.Models.DesignSpaceDataCenter.ModuleInternalBehaviorList.Add(newMiB);
          //TODO >> Update of Sender and Server Ports needed

          // Add new module entry
          CfgSpace_ModuleCfg newModule = new CfgSpace_ModuleCfg();
          newModule.ModuleName = newMiB.ModuleName;
          newModule.ModuleType = newMiB.ModuleType;
          newModule.ModuleBaseFolder = newModule.ModuleType + "\\" + newModule.ModuleName;
          General.Models.DesignSpaceDataCenter.ProjectConfig.CfgSpaceModuleList.Add(newModule);

          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }
      }
      catch (ArgumentException ex)
      {
        MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    #endregion
  }
}
/**
* @}
*/