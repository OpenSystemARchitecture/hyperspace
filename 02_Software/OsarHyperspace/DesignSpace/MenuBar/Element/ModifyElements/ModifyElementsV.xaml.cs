﻿/*****************************************************************************************************************************
 * @file        ModifyElementsV.xaml.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.12.2020                                                                                                   *
 * @brief       Window of modify elements menu part                                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Element.ModifyElements
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteTypes;
using RteLib.RteModuleInternalBehavior;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Element.ModifyElements
{
  /// <summary>
  /// Interaction logic for ModifyElementsV.xaml
  /// </summary>
  public partial class ModifyElementsV : Window
  {
    ViewModels.ModifyElementsVM usedModifyElementsVM;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="dataType"></param>
    public ModifyElementsV(RteAdvancedDataTypeStructure dataType)
    {
      usedModifyElementsVM = new ViewModels.ModifyElementsVM(dataType);
      InitializeComponent();

      this.DataContext = usedModifyElementsVM;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="srInterfaceBlueprint"></param>
    public ModifyElementsV(RteSRInterfaceBlueprint srInterfaceBlueprint)
    {
      usedModifyElementsVM = new ViewModels.ModifyElementsVM(srInterfaceBlueprint);
      InitializeComponent();

      this.DataContext = usedModifyElementsVM;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="csInterfaceBlueprint"></param>
    public ModifyElementsV(RteCSInterfaceBlueprint csInterfaceBlueprint)
    {
      usedModifyElementsVM = new ViewModels.ModifyElementsVM(csInterfaceBlueprint);
      InitializeComponent();

      this.DataContext = usedModifyElementsVM;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="mibModule"></param>
    public ModifyElementsV(RteModuleInternalBehavior mibModule)
    {
      usedModifyElementsVM = new ViewModels.ModifyElementsVM(mibModule);
      InitializeComponent();

      this.DataContext = usedModifyElementsVM;
    }

    #region UI Actions
    /// <summary>
    /// UI Button clicked event interface for ModifyElement
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_ModifyElement_Click(object sender, RoutedEventArgs e)
    {
      if (true == usedModifyElementsVM.UpdateElement())
      {
        OsarHyperspace.General.HyperspaceSystem.Instance().EventRteDataBaseUpdated();
        this.Close();
      }
    }
    #endregion
  }
}
/**
* @}
*/