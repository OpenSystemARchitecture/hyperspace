﻿/*****************************************************************************************************************************
 * @file        ModifyElementsVM.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.12.2020                                                                                                   *
 * @brief       View Model of the modify elements menu window                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.DesignSpace.MenuBar.Element.NewElements.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.DesignSpace.MenuBar.Element.NewElements.Models;
using OsarHyperspace.General;
using RteLib.RteInterface;
using RteLib.RteTypes;
using RteLib.RteModuleInternalBehavior;
using RtePresentation.RteInterface.Views;
using RtePresentation.RteTypes.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RtePresentation.RteModuleInternalBehavior.Views;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar.Element.ModifyElements.ViewModels
{
  public class ModifyElementsVM : BaseViewModel
  {
    private object dataTypeContent;                             //!< View object element for sub data type views
    ElementType usedElementType;

    // Data Type View and Handling
    private RteAdvancedDataTypeV elementContent;                //!< View object data type element for sub data type views
    private RteAdvancedDataTypeStructure newDataType;

    // Sender Receiver Interface Blueprint View and Handling
    private RteSRInterfaceBlueprintV srInterfaceBlueprintContent;   //!< View object SR interface blueprint views
    private RteSRInterfaceBlueprint newSRInterfaceBlueprint;

    // Client Server Interface Blueprint View and Handling
    private RteCSInterfaceBlueprintV csInterfaceBlueprintContent;   //!< View object CS interface blueprint views
    private RteCSInterfaceBlueprint newCSInterfaceBlueprint;

    // Module internal Behavior View and Handling
    private RteModuleInternalBehaviorV miBContent;                                  //!< View object module internal behavior views
    private RteModuleInternalBehavior newMiB;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="dataType"></param>
    public ModifyElementsVM(RteAdvancedDataTypeStructure dataType)
    {
      usedElementType = ElementType.NEW_DATA_TYPE;
      newDataType = dataType;

      elementContent = new RteAdvancedDataTypeV(newDataType, General.Models.DesignSpaceDataCenter.RteTypesDb);
      dataTypeContent = elementContent;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="srInterfaceBlueprint"></param>
    public ModifyElementsVM(RteSRInterfaceBlueprint srInterfaceBlueprint)
    {
      usedElementType = ElementType.NEW_SR_INTERFACE_BLUEPRINT;
      newSRInterfaceBlueprint = srInterfaceBlueprint;

      srInterfaceBlueprintContent = new RteSRInterfaceBlueprintV(newSRInterfaceBlueprint, General.Models.DesignSpaceDataCenter.RteTypesDb);
      dataTypeContent = srInterfaceBlueprintContent;
    }

    public ModifyElementsVM(RteCSInterfaceBlueprint csInterfaceBlueprint)
    {
      usedElementType = ElementType.NEW_CS_INTERFACE_BLUEPRINT;
      newCSInterfaceBlueprint = csInterfaceBlueprint;

      csInterfaceBlueprintContent = new RteCSInterfaceBlueprintV(newCSInterfaceBlueprint, General.Models.DesignSpaceDataCenter.RteTypesDb);
      dataTypeContent = csInterfaceBlueprintContent;
    }

    public ModifyElementsVM(RteModuleInternalBehavior mibModule)
    {
      usedElementType = ElementType.NEW_MODULE;
      newMiB = mibModule;

      miBContent = new RteModuleInternalBehaviorV(newMiB,
        General.Models.DesignSpaceDataCenter.RteTypesDb,
        General.Models.DesignSpaceDataCenter.RteInterfaceDb,
        General.Models.DesignSpaceDataCenter.RteConfigDb);
      dataTypeContent = miBContent;
    }
    //TODO add other constructors


    #region Properties
    /// <summary>
    /// Getter for the sub data type view
    /// </summary>
    public object SubElementView
    {
      get { return dataTypeContent; }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to update the element and evaluate it
    /// </summary>
    /// <returns></returns>
    public bool UpdateElement()
    {
      bool retVal = false;
      switch (usedElementType)
      {
        case ElementType.NEW_DATA_TYPE:
        retVal = Hlp_UpdateTheAdvancedDataType();
        break;

        case ElementType.NEW_CS_INTERFACE_BLUEPRINT:
        retVal = Hlp_UpdateTheCSInterfaceBlueprint();
        break;

        case ElementType.NEW_SR_INTERFACE_BLUEPRINT:
        retVal = Hlp_UpdateTheSRInterfaceBlueprint();
        break;

        case ElementType.NEW_MODULE:
        retVal = Hlp_UpdateTheModuleInternalBehavior();
        break;
      }

      return retVal;
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function which evaluated the updated element
    /// </summary>
    /// <returns></returns>
    private bool Hlp_UpdateTheAdvancedDataType()
    {
      bool retVal = false;
      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = General.Models.DesignSpaceDataCenter.RteTypesDb.ValidateRteDataTypeElement(newDataType);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach (string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("Data Type Validation", errorStr);
        }

        if (0 == validationErrors.Count)
        {
          // Nothing else to do here
          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }

      }
      catch (Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    /// <summary>
    /// Helper function which evaluated the updated element
    /// </summary>
    /// <returns></returns>
    private bool Hlp_UpdateTheSRInterfaceBlueprint()
    {
      bool retVal = false;

      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = General.Models.DesignSpaceDataCenter.RteInterfaceDb.ValidateRteSRInterfaceBlueprintElement(
          newSRInterfaceBlueprint, ref General.Models.DesignSpaceDataCenter.RteTypesDb);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach (string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("SR Blueprint Validation", errorStr);
        }

        if (0 == validationErrors.Count)
        {
          // Nothing else to do here
          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }

      }
      catch (Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    /// <summary>
    /// Helper function which evaluated the updated element
    /// </summary>
    /// <returns></returns>
    private bool Hlp_UpdateTheCSInterfaceBlueprint()
    {
      bool retVal = false;

      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = General.Models.DesignSpaceDataCenter.RteInterfaceDb.ValidateRteCSInterfaceBlueprintElement(
          newCSInterfaceBlueprint, ref General.Models.DesignSpaceDataCenter.RteTypesDb);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach (string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("CS Blueprint Validation", errorStr);
        }

        if (0 == validationErrors.Count)
        {
          // Nothing else to do here
          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }

      }
      catch (Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    /// <summary>
    /// Helper function which adds a new module internal behavior file
    /// </summary>
    private bool Hlp_UpdateTheModuleInternalBehavior()
    {
      bool retVal = false;
      try
      {
        List<string> validationErrors = new List<string>();
        validationErrors = newMiB.ValidateModuleInternalBehavior(
          ref General.Models.DesignSpaceDataCenter.RteConfigDb,
          ref General.Models.DesignSpaceDataCenter.RteInterfaceDb,
          ref General.Models.DesignSpaceDataCenter.RteTypesDb);
        HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().ClearProjectLogs();
        foreach (string errorStr in validationErrors)
        {
          HyperspaceSystem.Instance().DesignSpaceProjectLogInstance().
            AddNewProjectErrorLog("MiB Validation", errorStr);
        }

        if (0 == validationErrors.Count)
        {
          //TODO >> Update of Sender and Server Ports needed

          // Update configuration
          CfgSpace_ModuleCfg newModule = new CfgSpace_ModuleCfg();
          newModule.ModuleName = newMiB.ModuleName;
          newModule.ModuleType = newMiB.ModuleType;
          newModule.ModuleBaseFolder = newModule.ModuleType + "\\" + newModule.ModuleName;

          bool elementFound = false;
          foreach(CfgSpace_ModuleCfg configuredModule in General.Models.DesignSpaceDataCenter.ProjectConfig.CfgSpaceModuleList)
          {
            if(configuredModule.ModuleBaseFolder == newModule.ModuleBaseFolder)
            {
              elementFound = true;
              break;
            }
          }
          if (false == elementFound)
          {
            // Element does not exist >> Add the new configured module
            General.Models.DesignSpaceDataCenter.ProjectConfig.CfgSpaceModuleList.Add(newModule);
          }

          retVal = true;
        }
        else
        {
          MessageBox.Show("Validation errors. Check project log", "Alert", MessageBoxButton.OK);
        }
      }
      catch (ArgumentException ex)
      {
        MessageBox.Show(ex.Message, "Alert", MessageBoxButton.OK);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Unknown error" + ex.Message, "Alert", MessageBoxButton.OK);
      }

      return retVal;
    }

    #endregion
  }
}
