﻿/*****************************************************************************************************************************
 * @file        MenuBar.xaml.cs                                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        21.12.2020                                                                                                   *
 * @brief       Implementation of the UI Menu Bar for the Design Space.                                                      *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.DesignSpace.MenuBar
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.General;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MenuBar
{
  /// <summary>
  /// Interaction logic for MenuBar.xaml
  /// </summary>
  public partial class MenuBar : UserControl
  {
    public MenuBar()
    {
      InitializeComponent();
    }

    #region Menu File
    //========================================================================================================================
    //=============================================== Menu "File" ============================================================
    //========================================================================================================================
    /// <summary>
    /// Interface to store the current project configuration
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void StoreCurrentConfiguration_Clicked(object sender, EventArgs e)
    {
      General.Models.DesignSpaceDataCenter.StoreDataBases();
    }

    /// <summary>
    /// Interface to exit the current application
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void ExitApplication_Clicked(object sender, EventArgs e)
    {
      HyperspaceSystem.Instance().ShutdownDesignSpace();
    }
    #endregion

    #region Menu Project
    //========================================================================================================================
    //============================================== Menu "Project" ==========================================================
    //========================================================================================================================
    /// <summary>
    /// Interface to start the project settings window as dialog
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void ButtonMenuProject_ProjectSetting_Clicked(object sender, EventArgs e)
    {
      Project.ProjectSettings.ProjectSettings menuProjectSettingsView = new Project.ProjectSettings.ProjectSettings();
      menuProjectSettingsView.ShowDialog();
    }

    /// <summary>
    /// Interface to start the Port connections window as dialog
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void ButtonMenuProject_PortConnections_Clicked(object sender, EventArgs e)
    {
      Project.ProjectConfig.ProjectConfigV menuProjectConfigView = new Project.ProjectConfig.ProjectConfigV();
      menuProjectConfigView.ShowDialog();
    }
    #endregion

    #region Menu Element
    //========================================================================================================================
    //============================================== Menu "Element" ==========================================================
    //========================================================================================================================
    /// <summary>
    /// Interface to start the add new element window for a new data type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMenuElement_NewComponent_Clicked(object sender, RoutedEventArgs e)
    {
      Element.NewElements.NewElementsV menuNewElementView =
        new Element.NewElements.NewElementsV(Element.NewElements.Models.ElementType.NEW_MODULE);

      menuNewElementView.Show();
    }

    /// <summary>
    /// Interface to start the add new element window for a new data type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMenuElement_NewDataType_Clicked(object sender, RoutedEventArgs e)
    {
      Element.NewElements.NewElementsV menuNewElementView = 
        new Element.NewElements.NewElementsV(Element.NewElements.Models.ElementType.NEW_DATA_TYPE);

      menuNewElementView.Show();
    }

    /// <summary>
    /// Interface to start the add new element window for a new sender receiver interface blueprint
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMenuElement_NewSRInterfaceBlueprint_Clicked(object sender, RoutedEventArgs e)
    {
      Element.NewElements.NewElementsV menuNewElementView =
        new Element.NewElements.NewElementsV(Element.NewElements.Models.ElementType.NEW_SR_INTERFACE_BLUEPRINT);

      menuNewElementView.Show();
    }

    /// <summary>
    /// Interface to start the add new element window for a new client server interface blueprint
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMenuElement_NewCSInterfaceBlueprint_Clicked(object sender, RoutedEventArgs e)
    {
      Element.NewElements.NewElementsV menuNewElementView =
        new Element.NewElements.NewElementsV(Element.NewElements.Models.ElementType.NEW_CS_INTERFACE_BLUEPRINT);

      menuNewElementView.Show();
    }



    #endregion

    #region Engine / Helper functions
    //========================================================================================================================
    //================================================== Other ===============================================================
    //========================================================================================================================
    /// <summary>
    /// Interface to show that the request function is not available. This function starts an error MessageBox with
    /// the corresponding error message.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FunctionNotImplemented(object sender, EventArgs e)
    {
      MessageBox.Show("Function not implemented!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
    }

    /// <summary>
    /// Interface to show basic system information system 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMenueHelp_About_Click(object sender, RoutedEventArgs e)
    {
      string content = "";
      content += GeneralRessource.CreatorInformation;
      content += "\n";
      content += "\n";

      content += "Version:\nv." + OsarHyperspaceVersionClass.getCurrentVersion();
      content += "\n";
      content += "\n";

      content += "License Information:\n";
      content += GeneralRessource.LicenseInformation;

      MessageBox.Show(content, "About Information", MessageBoxButton.OK, MessageBoxImage.Information);
    }


    #endregion


  }
}
