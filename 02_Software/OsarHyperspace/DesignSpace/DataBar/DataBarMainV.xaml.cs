﻿/*****************************************************************************************************************************
 * @file        DataBarMainV.xaml.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        21.12.2020                                                                                                   *
 * @brief       Implementation of the Data Bar main View                                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/

/**
* @addtogroup OsarHyperspace.DesignSpace.DataBar
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.DataBar
{
  /// <summary>
  /// Interaction logic for DataBarMainV.xaml
  /// </summary>
  public partial class DataBarMainV : UserControl
  {
    ViewModel.DS_DataBarMainVM usedVm = new ViewModel.DS_DataBarMainVM();
    public DataBarMainV()
    {
      InitializeComponent();

      this.DataContext = usedVm;
    }

    #region UI Actions
    /// <summary>
    /// Interface for reaction on click event of expander
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void B_Expander_Clicked(object sender, RoutedEventArgs e)
    {
      EventHandling_UpdateDataBase();
    }


    #region Component
    /// <summary>
    /// Context Menu Component New clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_Component_New_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.NewElements.NewElementsV newView =
        new MenuBar.Element.NewElements.NewElementsV(MenuBar.Element.NewElements.Models.ElementType.NEW_MODULE);
      newView.Show();
    }

    /// <summary>
    /// Context Menu Component Modify clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_Component_Modify_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.ModifyElements.ModifyElementsV newView =
        new MenuBar.Element.ModifyElements.ModifyElementsV(usedVm.SelectedModuleInternalBehavior);

      newView.Show();
    }

    /// <summary>
    /// Context Menu Component Remove clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_Component_Remove_Clicked(object sender, EventArgs e)
    {
      usedVm.RemoveSelectedModuleInternalBehavior();
    }


    #endregion

    #region Advanced Data Type
    /// <summary>
    /// Context Menu AdvancedDataType New clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_AdvancedDataType_New_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.NewElements.NewElementsV newAdvancedDataTypeView =
        new MenuBar.Element.NewElements.NewElementsV(MenuBar.Element.NewElements.Models.ElementType.NEW_DATA_TYPE);
      newAdvancedDataTypeView.Show();
    }

    /// <summary>
    /// Context Menu AdvancedDataType Modify clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_AdvancedDataType_Modify_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.ModifyElements.ModifyElementsV modifyAdvancedDataTypeView =
        new MenuBar.Element.ModifyElements.ModifyElementsV(usedVm.SelectedAdvancedDataType);

      modifyAdvancedDataTypeView.Show();
    }

    /// <summary>
    /// Context Menu AdvancedDataType Remove clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_AdvancedDataType_Remove_Clicked(object sender, EventArgs e)
    {
      usedVm.RemoveSelectedAdvancedDataType();
    }
    #endregion

    #region Client Server Interface Blueprint


    /// <summary>
    /// Context Menu ClientServerBlueprint New clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_ClientServerBlueprint_New_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.NewElements.NewElementsV newCSInterfaceBlueprintView =
        new MenuBar.Element.NewElements.NewElementsV(
          MenuBar.Element.NewElements.Models.ElementType.NEW_CS_INTERFACE_BLUEPRINT);
      newCSInterfaceBlueprintView.Show();
    }

    /// <summary>
    /// Context Menu ClientServerBlueprint Modify clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_ClientServerBlueprint_Modify_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.ModifyElements.ModifyElementsV modifyCSInterfaceBlueprintView =
        new MenuBar.Element.ModifyElements.ModifyElementsV(usedVm.SelectedCSInterfaceBlueprint);

      modifyCSInterfaceBlueprintView.Show();
    }

    /// <summary>
    /// Context Menu ClientServerBlueprint Remove clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_ClientServerBlueprint_Remove_Clicked(object sender, EventArgs e)
    {
      usedVm.RemoveSelectedCSInterfaceBlueprint();
    }

    #endregion

    #region Sender Receiver Interface Blueprint
    /// <summary>
    /// Context Menu SenderReceiverBlueprint New clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_SenderReceiverBlueprint_New_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.NewElements.NewElementsV newSRInterfaceBlueprintView =
        new MenuBar.Element.NewElements.NewElementsV(
          MenuBar.Element.NewElements.Models.ElementType.NEW_SR_INTERFACE_BLUEPRINT);
      newSRInterfaceBlueprintView.Show();
    }

    /// <summary>
    /// Context Menu SenderReceiverBlueprint Modify clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_SenderReceiverBlueprint_Modify_Clicked(object sender, EventArgs e)
    {
      MenuBar.Element.ModifyElements.ModifyElementsV modifySRInterfaceBlueprintView =
        new MenuBar.Element.ModifyElements.ModifyElementsV(usedVm.SelectedSRInterfaceBlueprint);

      modifySRInterfaceBlueprintView.Show();
    }

    /// <summary>
    /// Context Menu SenderReceiverBlueprint Remove clicked event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CM_SenderReceiverBlueprint_Remove_Clicked(object sender, EventArgs e)
    {
      usedVm.RemoveSelectedSRInterfaceBlueprint();
    }
    #endregion

    #endregion

    #region Actions
    /// <summary>
    /// Interface to react on updated data bases
    /// </summary>
    public void EventHandling_UpdateDataBase()
    {
      // Update data base
      LV_AvailableComponents.Items.Refresh();
      LV_AvailableAdvancedDataTypes.Items.Refresh();
      LV_AvailableBaseDataTypes.Items.Refresh();
      LV_AvailableServerPorts.Items.Refresh();
      LV_AvailableSenderPorts.Items.Refresh();
      LV_AvailableCSInterfaceBlueprints.Items.Refresh();
      LV_AvailableSRInterfaceBlueprints.Items.Refresh();
    }
    #endregion
  }
}
/**
* @}
*/