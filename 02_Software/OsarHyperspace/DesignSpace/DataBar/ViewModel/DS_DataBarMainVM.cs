﻿/*****************************************************************************************************************************
 * @file        DS_DataBarMainVM.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        22.12.2020                                                                                                   *
 * @brief       Implementation of the view model for the Data Bar Main view.                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.DesignSpace.DataBar.ViewModel
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteTypes;
using RteLib.RteInterface;
using RteLib.RteConfig;
using RteLib.RteModuleInternalBehavior;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using OsarHyperspace.General;
using System.Collections.ObjectModel;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.DataBar.ViewModel
{
  public class DS_DataBarMainVM
  {
    private RteTypes rteTypesDb;
    private RteConfig rteConfigDb;
    private RteInterfaces rteInterfaceDb;
    private List<RteModuleInternalBehavior> rteMibModuleDb;
    private ObservableCollection<RteAvailableServerPort> availableServerPorts;
    private ObservableCollection<RteAvailableSRPort> availableSenderPorts;

    private RteAdvancedDataTypeStructure selectedAdvancedDataType;
    private RteSRInterfaceBlueprint selectedSRInterfaceBlueprint;
    private RteCSInterfaceBlueprint selectedCSInterfaceBlueprint;
    private RteModuleInternalBehavior selectedModuleInternalBehavior;


    /// <summary>
    /// Constructor
    /// </summary>
    public DS_DataBarMainVM()
    {
      rteTypesDb = DesignSpace.General.Models.DesignSpaceDataCenter.RteTypesDb;
      rteConfigDb = DesignSpace.General.Models.DesignSpaceDataCenter.RteConfigDb;
      rteInterfaceDb = DesignSpace.General.Models.DesignSpaceDataCenter.RteInterfaceDb;
      rteMibModuleDb = DesignSpace.General.Models.DesignSpaceDataCenter.ModuleInternalBehaviorList;
      availableServerPorts = DesignSpace.General.Models.DesignSpaceDataCenter.AvailableServerPorts;
      availableSenderPorts = DesignSpace.General.Models.DesignSpaceDataCenter.AvailableSenderPorts;
    }

    #region Element Attribute Interfaces
    /// <summary>
    /// Getter for the AvailableAdvancedDataTypes
    /// </summary>
    public List<RteAdvancedDataTypeStructure> AvailableAdvancedDataTypes
    {
      get { return rteTypesDb.AdvancedDataTypeList; }
    }

    public RteAdvancedDataTypeStructure SelectedAdvancedDataType
    {
      get { return selectedAdvancedDataType; }
      set { selectedAdvancedDataType = value; }
    }

    /// <summary>
    /// Getter for the AvailableBaseDataTypes
    /// </summary>
    public List<RteBaseDataTypeStructure> AvailableBaseDataTypes
    {
      get { return rteTypesDb.BaseDataTypeList; }
    }

    /// <summary>
    /// Getter for the AvailableBaseDataTypes
    /// </summary>
    public List<RteCSInterfaceBlueprint> AvailableCSInterfaceBlueprints
    {
      get { return rteInterfaceDb.ClientServerBlueprintList; }
    }

    /// <summary>
    /// Getter and Setter for the selectedCSInterfaceBlueprint
    /// </summary>
    public RteCSInterfaceBlueprint SelectedCSInterfaceBlueprint
    {
      get { return selectedCSInterfaceBlueprint; }
      set { selectedCSInterfaceBlueprint = value; }
    }

    /// <summary>
    /// Getter for the AvailableSRInterfaceBlueprints
    /// </summary>
    public List<RteSRInterfaceBlueprint> AvailableSRInterfaceBlueprints
    {
      get { return rteInterfaceDb.SenderReceiverBlueprintList; }
    }

    /// <summary>
    /// Getter and Setter for the selectedSRInterfaceBlueprint
    /// </summary>
    public RteSRInterfaceBlueprint SelectedSRInterfaceBlueprint
    {
      get { return selectedSRInterfaceBlueprint; }
      set { selectedSRInterfaceBlueprint = value; }
    }


    /// <summary>
    /// Getter for the AvailableComponents
    /// </summary>
    public List<RteModuleInternalBehavior> AvailableComponents
    {
      get { return rteMibModuleDb; }
    }

    /// <summary>
    /// Getter and Setter for the selectedModuleInternalBehavior
    /// </summary>
    public RteModuleInternalBehavior SelectedModuleInternalBehavior
    {
      get { return selectedModuleInternalBehavior; }
      set { selectedModuleInternalBehavior = value; }
    }

    /// <summary>
    /// Getter for the AvailableServerPorts
    /// </summary>
    public ObservableCollection<RteAvailableServerPort> AvailableServerPorts
    {
      get { return availableServerPorts; }
    }

    /// <summary>
    /// Getter for the AvailableSenderPorts
    /// </summary>
    public ObservableCollection<RteAvailableSRPort> AvailableSenderPorts
    {
      get { return availableSenderPorts; }
    }

    #endregion

    #region Actions
    /// <summary>
    /// Action interface to remove the selected advanced data type
    /// </summary>
    public void RemoveSelectedAdvancedDataType()
    {
      rteTypesDb.AdvancedDataTypeList.Remove(selectedAdvancedDataType);
      OsarHyperspace.General.HyperspaceSystem.Instance().EventRteDataBaseUpdated();
    }

    /// <summary>
    /// Action interface to remove the selected client server receiver interface blueprint
    /// </summary>
    public void RemoveSelectedCSInterfaceBlueprint()
    {
      rteInterfaceDb.ClientServerBlueprintList.Remove(selectedCSInterfaceBlueprint);
      OsarHyperspace.General.HyperspaceSystem.Instance().EventRteDataBaseUpdated();
    }

    /// <summary>
    /// Action interface to remove the selected sender receiver interface blueprint
    /// </summary>
    public void RemoveSelectedSRInterfaceBlueprint()
    {
      rteInterfaceDb.SenderReceiverBlueprintList.Remove(selectedSRInterfaceBlueprint);
      OsarHyperspace.General.HyperspaceSystem.Instance().EventRteDataBaseUpdated();
    }

    /// <summary>
    /// Action interface to remove the selected module internal behavior
    /// >> Remove it also from the project config
    /// </summary>
    public void RemoveSelectedModuleInternalBehavior()
    {
      foreach(CfgSpace_ModuleCfg module in General.Models.DesignSpaceDataCenter.ProjectConfig.CfgSpaceModuleList)
      {
        if ( ( module.ModuleName == selectedModuleInternalBehavior.ModuleName) &&
             ( module.ModuleType == selectedModuleInternalBehavior.ModuleType ) )
        {
          // Remove found module from config
          General.Models.DesignSpaceDataCenter.ProjectConfig.CfgSpaceModuleList.Remove(module);
          break;
        }
      }

      // Remove module internal behavior config
      rteMibModuleDb.Remove(SelectedModuleInternalBehavior);

      OsarHyperspace.General.HyperspaceSystem.Instance().EventRteDataBaseUpdated();
      // Update of available ports needed
    }

    #endregion

  }

}
/**
* @}
*/