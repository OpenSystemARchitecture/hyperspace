﻿/*****************************************************************************************************************************
 * @file        DesignSpaceEntry.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.12.2020                                                                                                   *
 * @brief       Entry-Point for the Design-Space part of the Hyperspace-Tool                                                 *
 *                                                                                                                           *
 * @details     Implementation of general starting point >> Setup of necessary elements. >> Check required pre-settings.     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/


/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.DesignSpace.Startup.Engine
* @{
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.Startup.Engine
{
  public class DesignSpaceEntry
  {
    private MainWindow.DesignSpace hyperspaceDesignSpace;
    private Views.DesignStartupWindow startupWindow;

    /**
     * @brief Constructor
     */
    public DesignSpaceEntry()
    {
      startupWindow = new Views.DesignStartupWindow();
    }

    /**
     * @brief     Startup function for the design space
     */
    public void StartupDesignSpace()
    {
      /* Start with Startup Screen */
      startupWindow.ShowDialog();

      /* Start Configuration Space GUI */
      hyperspaceDesignSpace = new MainWindow.DesignSpace();
      hyperspaceDesignSpace.InitializeWindow();
      hyperspaceDesignSpace.ShowDialog();
    }
  }
}
/**
* @}
*/