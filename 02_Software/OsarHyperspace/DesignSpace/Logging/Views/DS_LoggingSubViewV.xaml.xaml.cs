﻿/*****************************************************************************************************************************
 * @file        DS_LoggingSubViewV.xaml.cs                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        21.09.2019                                                                                                   *
 * @brief       Implementation of Logging Sub View.                                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.DesignSpace.Logging.Views
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.General.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.Logging.Views
{
  /// <summary>
  /// Interaction logic for DS_LoggingSubViewV.xaml
  /// </summary>
  public partial class DS_LoggingSubViewV : UserControl
  {
    public DS_LoggingSubViewV()
    {
      InitializeComponent();
    }


    /// <summary>
    /// Interface to add a new log message to log window
    /// </summary>
    /// <param name="logType"></param>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewLogMessage(LoggingType logType, string logName, string logContent)
    {
      // Use Dispatcher to access element from different Tasks / Threads
      SP_LoogingEntries.Dispatcher.Invoke(new Action(() =>
      {
        DS_LoggingEntryV logEntry = new DS_LoggingEntryV(logType, logName, logContent);
        SP_LoogingEntries.Children.Add(logEntry);
      }));
    }

    /// <summary>
    /// Interface to clear all log messages
    /// </summary>
    public void ClearLogMessages()
    {
      // Use Dispatcher to access element from different Tasks / Threads
      SP_LoogingEntries.Dispatcher.Invoke(new Action(() =>
      {
        SP_LoogingEntries.Children.Clear();
      }));
    }
  }
}
/**
* @}
*/