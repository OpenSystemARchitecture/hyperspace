﻿/*****************************************************************************************************************************
 * @file        DesignSpaceDataCenter.cs                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        22.12.2020                                                                                                   *
 * @brief       Implementation of the central data center for the design space.                                              *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.DesignSpace.General.Models
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using RteLib;
using RteLib.RteModuleInternalBehavior;
using RteLib.RteConfig;
using RteLib.RteInterface;
using RteLib.RteTypes;
using System.Collections.ObjectModel;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.General.Models
{
  public static class DesignSpaceDataCenter
  {
    // Database files data
    private static HyperspaceProjectConfig projectConfig;
    private static RteConfig rteConfig;
    private static RteInterfaces rteInterfces;
    private static RteTypes rteTypes;
    private static List<RteModuleInternalBehavior> mibList;

    // Virtual data 
    private static ObservableCollection<RteAvailableServerPort> availableServerPorts;
    private static ObservableCollection<RteAvailableSRPort> availableSenderPorts;

    /// <summary>
    /// Constructor
    /// </summary>
    static DesignSpaceDataCenter()
    {
      projectConfig = HyperspaceProjectConfig.Instance();
      availableServerPorts = new ObservableCollection<RteAvailableServerPort>();
      availableSenderPorts = new ObservableCollection<RteAvailableSRPort>();
    }

    #region Element Properties Interfaces
    /// <summary>
    /// Getter to reference of the hyperspace project config
    /// </summary>
    public static ref HyperspaceProjectConfig ProjectConfig
    {
      get { return ref projectConfig; }
    }

    /// <summary>
    /// Getter to the reference of the rte types data base
    /// </summary>
    public static ref RteTypes RteTypesDb
    {
      get { return ref rteTypes; }
    }

    /// <summary>
    /// Getter to the reference of the rte interface data base
    /// </summary>
    public static ref RteInterfaces RteInterfaceDb
    {
      get { return ref rteInterfces; }
    }

    /// <summary>
    /// Getter to the reference of the rte config data base
    /// </summary>
    public static ref RteConfig RteConfigDb
    {
      get { return ref rteConfig; }
    }

    /// <summary>
    /// Getter to the reference of the module internal behavior list
    /// </summary>
    public static ref List<RteModuleInternalBehavior> ModuleInternalBehaviorList
    {
      get { return ref mibList; }
    }

    /// <summary>
    /// Getter to the reference of the available server ports
    /// </summary>
    public static ref ObservableCollection<RteAvailableServerPort> AvailableServerPorts
    {
      get { return ref availableServerPorts; }
    }

    /// <summary>
    /// Getter to the reference of the available server ports
    /// </summary>
    public static ref ObservableCollection<RteAvailableSRPort> AvailableSenderPorts
    {
      get { return ref availableSenderPorts; }
    }
    #endregion

    #region Action Interfaces
    /// <summary>
    /// Interface to trigger the auto detection of the required rte configuration.
    /// If RTE config is available a true is returned.
    /// If Rte auto detection was successful, an true is returned.
    /// </summary>
    /// <returns></returns>
    static public bool AutoDetectRequiredRteConfig()
    {
      bool retVal = true;
      bool moduleFound = false;
      CfgSpace_ModuleCfg rteModule = new CfgSpace_ModuleCfg();

      if ( "" != General.Models.DesignSpaceDataCenter.ProjectConfig.BaseProjectFolderPath )
      {
        if( ( "" == General.Models.DesignSpaceDataCenter.ProjectConfig.DesignSpaceConfiguration.rteConfigFilePath ) ||
            ( "" == General.Models.DesignSpaceDataCenter.ProjectConfig.DesignSpaceConfiguration.rteInterfaceFilePath ) ||
            ( "" == General.Models.DesignSpaceDataCenter.ProjectConfig.DesignSpaceConfiguration.rteTypesFilePath ))
        {
          foreach(CfgSpace_ModuleCfg hsModule in projectConfig.CfgSpaceModuleList)
          {
            if(DesignSpaceGeneral.DesignSpace_Rte_ModuleName == hsModule.ModuleName)
            {
              rteModule = hsModule;
              moduleFound = true;
              break;
            }
          }

          if(true == moduleFound)
          {
            DesignSpaceConfig newDesignSpaceConfig = new DesignSpaceConfig();
            string path = "";

            /* Check if Config File Exists */
            path = projectConfig.GetAbsoluteBaseProjectPath() + rteModule.ModuleBaseFolder + rteModule.CfgFilePath;
            if (true == File.Exists(path))
            {
              newDesignSpaceConfig.rteConfigFilePath = projectConfig.CreateRelativePathToProjectConfigFile(path);

              // Load Rte Config File
              rteConfig = new RteLib.RteConfig.RteConfig();
              rteConfig.ReadActiveRteConfigDataBaseFromXml(path);
            }
            else
            { retVal = false; }

            /* Check if Interface File Exists */
            path = Path.GetDirectoryName(projectConfig.GetAbsoluteBaseProjectPath() + rteModule.ModuleBaseFolder +
              rteModule.CfgFilePath) + rteConfig.PathToRteInterfaceFile;
            if (true == File.Exists(path))
            {
              newDesignSpaceConfig.rteInterfaceFilePath = projectConfig.CreateRelativePathToProjectConfigFile(path);
            }
            else
            { retVal = false; }

            /* Check if Types File Exists */
            path = Path.GetDirectoryName(projectConfig.GetAbsoluteBaseProjectPath() + rteModule.ModuleBaseFolder + 
              rteModule.CfgFilePath) + rteConfig.PathToRteTypeFile;
            if (true == File.Exists(path))
            {
              newDesignSpaceConfig.rteTypesFilePath = projectConfig.CreateRelativePathToProjectConfigFile(path);
            }
            else
            { retVal = false; }

            projectConfig.DesignSpaceConfiguration = newDesignSpaceConfig;

          }
          else
          { retVal = false; }
        }
      }
      else
      { retVal = false; }

      return retVal;
    }

    /// <summary>
    /// Interface to load all data sets from the different modules
    /// </summary>
    static public void LoadWorkspaceDataFromConfigFiles()
    {
      string path;
      // Load Rte Config File
      try
      {
        path = projectConfig.GetAbsoluteBaseProjectPath() + projectConfig.DesignSpaceConfiguration.rteConfigFilePath;
        if (true == File.Exists(path))
        {
          rteConfig = new RteLib.RteConfig.RteConfig();
          rteConfig.ReadActiveRteConfigDataBaseFromXml(path);

          // Check Config File Version
          if (rteConfig.XmlFileVersion.MajorVersion !=
            Convert.ToUInt16(DesignSpaceGeneral.DesignSpace_SupportedConfigFile_MajorVersion))
          {
            HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(HyperspaceExceptions.W0007, HyperspaceExceptions.W0007_Msg);
            rteConfig = new RteLib.RteConfig.RteConfig();
          }
        }
        else
        {
          HyperspaceSystem.Instance().AddNewGlobalSystemErrorLog(HyperspaceExceptions.E1000, HyperspaceExceptions.E1000_DS_FailedToLoadRteConfigDb);
        }
      }
      catch (Exception ex)
      {
        HyperspaceSystem.Instance().AddNewGlobalSystemErrorLog(HyperspaceExceptions.E1000, HyperspaceExceptions.E1000_DS_FailedToLoadRteConfigDb + 
            " Exception message: " + ex.Message);
      }

      // Load Rte Interface File
      try
      {
        path = projectConfig.GetAbsoluteBaseProjectPath() + projectConfig.DesignSpaceConfiguration.rteInterfaceFilePath;
        if (true == File.Exists(path))
        {
          rteInterfces = new RteLib.RteInterface.RteInterfaces();
          rteInterfces.ReadActiveRteInterfaceDataBaseFromXml(path);

          // Check Interface File Version
          if (rteInterfces.XmlFileVersion.MajorVersion !=
            Convert.ToUInt16(DesignSpaceGeneral.DesignSpace_SupportedInterfaceFile_MajorVersion))
          {
            HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(HyperspaceExceptions.W0005, HyperspaceExceptions.W0005_Msg);
            rteInterfces = new RteLib.RteInterface.RteInterfaces();
          }
        }
        else
        {
          HyperspaceSystem.Instance().AddNewGlobalSystemErrorLog(HyperspaceExceptions.E1001, HyperspaceExceptions.E1001_DS_FailedToLoadRteInterfaceDb);
        }
      }
      catch (Exception ex)
      {
        HyperspaceSystem.Instance().AddNewGlobalSystemErrorLog(HyperspaceExceptions.E1001, HyperspaceExceptions.E1001_DS_FailedToLoadRteInterfaceDb +
            " Exception message: " + ex.Message);
      }

      // Load Rte Types File
      try
      {
        path = projectConfig.GetAbsoluteBaseProjectPath() + projectConfig.DesignSpaceConfiguration.rteTypesFilePath;
        if (true == File.Exists(path))
        {
          rteTypes = new RteLib.RteTypes.RteTypes();
          rteTypes.ReadActiveRteTypesDataBaseFromXml(path);

          // Check Types File Version
          if (rteTypes.XmlFileVersion.MajorVersion !=
            Convert.ToUInt16(DesignSpaceGeneral.DesignSpace_SupportedTypesFile_MajorVersion))
          {
            HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(HyperspaceExceptions.W0006, HyperspaceExceptions.W0006_Msg);
            rteTypes = new RteLib.RteTypes.RteTypes();
            rteTypes.SetupDefaultConfiguration();
          }
        }
        else
        {
          HyperspaceSystem.Instance().AddNewGlobalSystemErrorLog(HyperspaceExceptions.E1002, HyperspaceExceptions.E1002_DS_FailedToLoadRteTypesDb);
        }
      }
      catch (Exception ex)
      {
        HyperspaceSystem.Instance().AddNewGlobalSystemErrorLog(HyperspaceExceptions.E1002, HyperspaceExceptions.E1002_DS_FailedToLoadRteTypesDb +
            " Exception message: " + ex.Message);
      }

      //Load Hyperspace Configured Modules
      mibList = new List<RteModuleInternalBehavior>();
      foreach (CfgSpace_ModuleCfg hsModule in projectConfig.CfgSpaceModuleList)
      {
        try
        {
          // Check for Rte Module >> Nothing to do here
          if (DesignSpaceGeneral.DesignSpace_Rte_ModuleName == hsModule.ModuleName)
          { continue; /*Skipp this module */ }

          path = projectConfig.GetAbsoluteBaseProjectPath() + hsModule.ModuleBaseFolder +
            DesignSpaceGeneral.DesignSpace_ModuleMiBFileLocation;
          if (true == File.Exists(path))
          {
            RteModuleInternalBehavior newMibFile = new RteModuleInternalBehavior();
            newMibFile.ReadActiveRteModuleInternalBehaviorToXml(path);

            // Check Mib File Version
            if(newMibFile.XmlFileVersion.MajorVersion != 
              Convert.ToUInt16(DesignSpaceGeneral. DesignSpace_SupportedMiBFile_MajorVersion))
            {
              HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(HyperspaceExceptions.W0004, HyperspaceExceptions.W0004_Msg +
               hsModule.ModuleName);
            }
            else
            {
              mibList.Add(newMibFile);
            }
          }
          else
          {
            HyperspaceSystem.Instance().AddNewGlobalSystemWarningLog(
              HyperspaceExceptions.E1003, HyperspaceExceptions.E1003_DS_FailedToLoadHsModuleDb +
              hsModule.ModuleName + " >> Check MiB file path: " + path);
          }
        }
        catch (Exception ex)
        {
          HyperspaceSystem.Instance().AddNewGlobalSystemErrorLog(HyperspaceExceptions.E1003, HyperspaceExceptions.E1003_DS_FailedToLoadHsModuleDb +
              hsModule.ModuleName + " >> Exception message: " + ex.Message);
        }
      }

      /* Setup available ports lists */
      UpdateAvailablePorts();
    }

    /// <summary>
    /// Interface to store the different databases
    /// </summary>
    /// <exception cref="FileNotFoundException"></exception>
    static public void StoreDataBases()
    {
      string path;

      /* Check if Config File Exists */
      try
      {
        path = projectConfig.GetAbsoluteBaseProjectPath() + projectConfig.DesignSpaceConfiguration.rteConfigFilePath;
        if (true == File.Exists(path))
        {
          // Load Rte Config File
          rteConfig.SaveActiveRteConfigDataBaseToXml(path);
        }
        else
        {
          HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
            AddNewSystemErrorLog(HyperspaceExceptions.E1004, HyperspaceExceptions.E1004_DS_FailedToStoreRteConfigDb);
        }
      }
      catch (Exception ex)
      {
        HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
            AddNewSystemErrorLog(HyperspaceExceptions.E1004, HyperspaceExceptions.E1004_DS_FailedToStoreRteConfigDb +
            " Exception message: " + ex.Message);
      }


      /* Check if Types File Exists */
      try
      {
        path = projectConfig.GetAbsoluteBaseProjectPath() + projectConfig.DesignSpaceConfiguration.rteTypesFilePath;
        if (true == File.Exists(path))
        {
          // Load Rte Types File
          rteTypes.SaveActiveRteTypesDataBaseToXml(path);
        }
        else
        {
          HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
            AddNewSystemErrorLog(HyperspaceExceptions.E1006, HyperspaceExceptions.E1006_DS_FailedToStoreRteTypesDb);
        }
      }
      catch (Exception ex)
      {
        HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
            AddNewSystemErrorLog(HyperspaceExceptions.E1006, HyperspaceExceptions.E1006_DS_FailedToStoreRteTypesDb +
            " Exception message: " + ex.Message);
      }

      /* Check if Interface File Exists */
      try
      {
        path = projectConfig.GetAbsoluteBaseProjectPath() + projectConfig.DesignSpaceConfiguration.rteInterfaceFilePath;
        if (true == File.Exists(path))
        {
          // Load Rte Interface File
          rteInterfces.SaveActiveRteInterfaceDataBaseToXml(path);
        }
        else
        {
          HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
            AddNewSystemErrorLog(HyperspaceExceptions.E1005, HyperspaceExceptions.E1005_DS_FailedToStoreRteInterfaceDb);
        }
      }
      catch (Exception ex)
      {
        HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
            AddNewSystemErrorLog(HyperspaceExceptions.E1005, HyperspaceExceptions.E1005_DS_FailedToStoreRteInterfaceDb +
            " Exception message: " + ex.Message);
      }

      /* Store module internal behavior files */
      foreach (RteModuleInternalBehavior mibModule in mibList)
      {
        try
        {
          // Check for Rte Module >> Nothing to do here
          if (DesignSpaceGeneral.DesignSpace_Rte_ModuleName == mibModule.ModuleName)
          { continue; /*Skipp this module */ }

          path = projectConfig.GetAbsoluteBaseProjectPath() + mibModule.ModuleType + "/" +
            mibModule.ModuleName + DesignSpaceGeneral.DesignSpace_ModuleMiBFileLocation;
          if (true == File.Exists(path))
          {
            mibModule.SaveActiveRteModuleInternalBehaviorToXml(path);
          }
          else
          {
            // If file dose not exist, create a new file with folders
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            mibModule.SaveActiveRteModuleInternalBehaviorToXml(path);

            HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
            AddNewSystemWarningLog(HyperspaceExceptions.W1008, HyperspaceExceptions.W1008_Msg +
            mibModule.ModuleName  + " Path: "+ path);
          }
        }
        catch (Exception ex)
        {
          HyperspaceSystem.Instance().DesignSpaceSystemLogInstance().
              AddNewSystemErrorLog(HyperspaceExceptions.W1008, HyperspaceExceptions.W1008_Msg +
              mibModule.ModuleName + " >> Exception message: " + ex.Message);
        }
      }

      /* Store Hyperspace Project File */
      HyperspaceProjectConfig.Instance().StoreHyperspaceProjectConfigFile();
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function which fills initial the available sender and server ports list,
    /// which are virtual data lists.
    /// Data source are the MiB data sets.
    /// </summary>
    static public void UpdateAvailablePorts()
    {
      /* Setup available Server / Sender Ports list */
      availableServerPorts.Clear();
      availableSenderPorts.Clear();
      foreach (RteModuleInternalBehavior miB in mibList)
      {
        foreach(RteAvailableServerPort serverPort in miB.AvailableServerPorts)
        {
          availableServerPorts.Add(serverPort);
        }

        foreach(RteAvailableSRPort senderPort in miB.AvailableSRPorts)
        {
          if(RteSRInterfaceImplementationType.SENDER == senderPort.SenderReceiverPort.InterfaceType )
          {
            availableSenderPorts.Add(senderPort);
          }
        }
      }
    }
    #endregion
  }
}
/**
* @}
*/
