﻿/*****************************************************************************************************************************
 * @file        DesignSpace.cs                                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        21.12.2020                                                                                                   *
 * @brief       Implementation of the Main Design Space Window                                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.DesignSpace.MainWindow
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.DesignSpace.MainWindow.Views;
using OsarHyperspace.General.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OsarHyperspace.DesignSpace.DataBar;
using OsarHyperspace.DesignSpace.Logging;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.DesignSpace.MainWindow
{
  /// <summary>
  /// Interaction logic for DesignSpace.xaml
  /// </summary>
  public partial class DesignSpace : Window
  {
    private LoggingV usedLoggingV;
    private DataBarMainV usedDataBarV;
    private Dummy dummyDravArea;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public DesignSpace()
    {
      InitializeComponent();

      // Create module log system
      usedLoggingV = new LoggingV();
      CC_Logging.DataContext = usedLoggingV;

      // Create Data Bar
      usedDataBarV = new DataBarMainV();
      CC_DataBar.DataContext = usedDataBarV;

      //Create Draw Area
      dummyDravArea = new Dummy();
      CC_DrawArea.DataContext = dummyDravArea;

      // Registration of Configuration Space main window in Hyperspace System
      OsarHyperspace.General.HyperspaceSystem.Instance().HyperspaceDesignSpace = this;
    }

    /// <summary>
    /// Runtime initialization of the Window
    /// </summary>
    public void InitializeWindow()
    {
      //moduleSelector.InitializeWindow();
    }

    #region UI Interfaces
    /// <summary>
    /// Event handler for closing the window
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CloseWindow(object sender, System.ComponentModel.CancelEventArgs e)
    {
      usedLoggingV.ShutdownLogging();
    }
    #endregion

    #region System Interfaces

    /// <summary>
    /// Interface to trigger a rte data base update reaction action
    /// Note:
    /// Used in design Space
    /// </summary>
    public void EventRteDataBaseUpdated()
    {
      General.Models.DesignSpaceDataCenter.UpdateAvailablePorts();
      usedDataBarV.EventHandling_UpdateDataBase();
    }

    /// <summary>
    /// Interface to access the project logging System
    /// </summary>
    /// <returns></returns>
    public ProjectLoggingInterface GetProjectLogInstance()
    {
      return usedLoggingV.ProjectLogging;
    }

    /// <summary>
    /// Interface to access the system logging System
    /// </summary>
    /// <returns></returns>
    public SystemLoggingInterface GetSystemLogInstance()
    {
      return usedLoggingV.SystemLogging;
    }
    #endregion
  }
}
/**
* @}
*/

