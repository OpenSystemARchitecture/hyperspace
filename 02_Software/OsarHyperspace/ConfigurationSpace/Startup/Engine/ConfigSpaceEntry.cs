﻿/*****************************************************************************************************************************
 * @file        ConfigSpaceEntry.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        17.12.2020                                                                                                   *
 * @brief       Entry-Point for the Configuration-Space part of the Hyperspace-Tool                                          *
 *                                                                                                                           *
 * @details     Implementation of general starting point >> Setup of necessary elements. >> Check required pre-settings.     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/


/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.ConfigurationSpace.Startup.Engine
* @{
*/
using OsarHyperspace.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.Startup.Engine
{
  /**
   * @brief     Entry point for the Configuration-Space
   */
  public class ConfigSpaceEntry
  {
    private ConfigurationSpace.MainWindow.ConfigurationSpace hyperspaceCfgSpace;
    private Views.ConfigurationStartupWindow startupWindow;

    /**
     * @brief Constructor
     */
    public ConfigSpaceEntry()
    {
      hyperspaceCfgSpace = new ConfigurationSpace.MainWindow.ConfigurationSpace();
      startupWindow = new Views.ConfigurationStartupWindow();
    }

    /**
     * @brief     Startup function for the configuration space
     */
    public void StartupConfigurationSpace()
    {
      /* Start with Startup Screen */
      CheckAndDoInitialSetup();

      /* Start Configuration Space GUI */
      hyperspaceCfgSpace.InitializeWindow();
      hyperspaceCfgSpace.ShowDialog();
    }

    /**
     * @brief       Check if initial project setup screen is required. If it is required, also show it.
     */
    private void CheckAndDoInitialSetup()
    {
      if( ("" == HyperspaceProjectConfig.Instance().BaseProjectFolderPath) ||
          ("" == HyperspaceProjectConfig.Instance().ProjectName) )
      {
        /* Start with Startup Screen */
        startupWindow.ShowDialog();

        /* Store current configuration */
        HyperspaceProjectConfig.Instance().StoreHyperspaceProjectConfigFile();
      }
    }
  }
}
/**
* @}
*/