﻿/*****************************************************************************************************************************
 * @file        ConfigurationStartupWindow.xaml.cs                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.12.2020                                                                                                   *
 * @brief       Implementation of the configuration startup window handler.                                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace.ConfigurationSpace.Startup.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.Startup.Views
{
  /// <summary>
  /// Interaction logic for ConfigurationStartupWindow.xaml
  /// </summary>
  public partial class ConfigurationStartupWindow : Window
  {
    private int statemachine = -1;
    private EmptyScreen contentControlScreen = new EmptyScreen();
    public ConfigurationStartupWindow()
    {
      InitializeComponent();

      ProcessStartupScreenStateMachine(true);
    }

    #region Engine
    /**
     * @brief     State-machine of the startup window
     * @param[in] incrementScreen: Select if next or last screen shall be shown
     * @details   Go to all necessary screens
     */
    private void ProcessStartupScreenStateMachine(bool incrementScreen)
    {
      if(true == incrementScreen)
      { statemachine++; }
      else
      { statemachine--; }

      switch (statemachine)
      {
        case 0:
          {
            L_ContentDescriptor.Content = Models.StartupScreenContent.Screen_Startup_Label;

            // Disable last button
            B_LastScreen.IsEnabled = false;

            // Set initial screen as content
            contentControlScreen.TB_EmptyScreenText.Text = Models.StartupScreenContent.Screen_Startup_Content;
            CC_StartupContent.DataContext = contentControlScreen;
          }
        break;

        case 1:
          {
            L_ContentDescriptor.Content = Models.StartupScreenContent.Screen_BasicProjectCfg_Label;
            CC_StartupContent.DataContext = new MenuBar.Project.ProjectSettings.Views.PS_GeneralV();

            // Enable last button
            B_LastScreen.IsEnabled = true;
          }
        break;

        case 2:
          {
            L_ContentDescriptor.Content = Models.StartupScreenContent.Screen_Closing_Label;
            CC_StartupContent.DataContext = contentControlScreen;

            // Set screen as content
            contentControlScreen.TB_EmptyScreenText.Text = Models.StartupScreenContent.Screen_Closing_Content;
          }
        break;

        case 3:
          {
            this.Close();
          }
        break;

        default:
          {
            MessageBox.Show("Unknown Error: Continue with main-screen", "Failure", MessageBoxButton.OK , MessageBoxImage.Error);
            this.Close();
          }
        break;
      }
    }
    #endregion

    #region UI Actions
    /**
     * @brief     UI Button to return the last screen
     */
    private void B_LastScreen_Click(object sender, RoutedEventArgs e)
    {
      ProcessStartupScreenStateMachine(false);
    }

    /**
     * @brief     UI Button to set the next screen
     */
    private void B_NextScreen_Click(object sender, RoutedEventArgs e)
    {
      ProcessStartupScreenStateMachine(true);
    }
    #endregion
  }
}
/**
* @}
*/
