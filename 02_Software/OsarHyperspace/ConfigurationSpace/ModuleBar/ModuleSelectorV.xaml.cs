﻿/*****************************************************************************************************************************
 * @file        CS_ModuleSelectorV.cs                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View for the Configuration Space Module Selector                                       *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.ModuleBar
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.ConfigurationSpace.ModuleBar.ViewModels;
using OsarHyperspace.ConfigurationSpace.MainWindow.Views;
using System.Reflection;
using OsarHyperspace.General;
using System.IO;
using OsarResources.Generator;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.ModuleBar
{
  /// <summary>
  /// Interaction Logic for the CS_ModuleSelectorV.xaml
  /// </summary>
  public partial class ModuleSelectorV : UserControl
  {
    private List<CS_ModuleCfgVM> moduleSelectorModules = new List<CS_ModuleCfgVM>();
    ProvideNewUserControlView deleate_UpdateModuleView;

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    #region Initialization
    /// <summary>
    /// Constructor
    /// </summary>
    public ModuleSelectorV(ProvideNewUserControlView delUpdateModuleView)
    {
      deleate_UpdateModuleView = delUpdateModuleView;
      InitializeComponent();
    }

    /// <summary>
    /// Runtime initialization function of the Window
    /// </summary>
    public void InitializeWindow()
    {
      InitializeViewModel_ModuleCfg();
    }

    /// <summary>
    /// Initialization of the View Model Module Cfg
    /// </summary>
    private void InitializeViewModel_ModuleCfg()
    {
      /* Request Module List */
      for(int idx = 0; idx < General.HyperspaceProjectConfig.Instance().CfgSpaceModuleList.Count; idx++)
      {
        var module = General.HyperspaceProjectConfig.Instance().CfgSpaceModuleList[idx];
        CS_ModuleCfgVM viewModel = new CS_ModuleCfgVM(ref module);

        if(( module.ModuleType == RteLib.RteConfig.RteConfigModuleTypes.BSW) ||
           ( module.ModuleType == RteLib.RteConfig.RteConfigModuleTypes.CDD ) ||
           ( module.ModuleType == RteLib.RteConfig.RteConfigModuleTypes.MCAL ))
        {
          moduleSelectorModules.Add(viewModel);
        }
      }

      /* Setup necessary Buttons */
      foreach(var module in moduleSelectorModules)
      {
        Button moduleButton = new Button();
        moduleButton.Content = module.ModuleName;
        moduleButton.Name = "Button" + module.ModuleName;
        moduleButton.Margin = new Thickness(5);
        if(RteLib.RteConfig.RteConfigModuleTypes.CDD == module.ModuleType)
        {
          moduleButton.Background = Brushes.LawnGreen;
        }
        else if (RteLib.RteConfig.RteConfigModuleTypes.BSW == module.ModuleType)
        {
          moduleButton.Background = Brushes.LightBlue;
        }
        else if (RteLib.RteConfig.RteConfigModuleTypes.MCAL == module.ModuleType)
        {
          moduleButton.Background = Brushes.Orange;
        }

        moduleButton.Tag = module;
        moduleButton.Click += ModuleButton_Click;

        SP_ModuleButtonList.Children.Add(moduleButton);
      }
    }
    #endregion

    /// <summary>
    /// Interface to inform the system about a change in the project configuration
    /// </summary>
    public void EventProjectConfigurationChanged()
    {
      //Cleanup module config list
      moduleSelectorModules.Clear();
      SP_ModuleButtonList.Children.Clear();

      // Reinitialize View Module with new module configuration
      InitializeViewModel_ModuleCfg();
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Module Button clicked interface >> Load corresponding module dll and load view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ModuleButton_Click(object sender, RoutedEventArgs e)
    {
      Button activeButton = (Button)sender;

      CS_ModuleCfgVM moduleCfgViewModel = ((CS_ModuleCfgVM)activeButton.Tag);

      try
      {
        LoadModuleDll(ref moduleCfgViewModel);

        /* Request configuration view */
        UserControl moduleView = moduleCfgViewModel.ModuleContext.GetView(
        moduleCfgViewModel.GetAbsoluteModuleCfgFilePath(),
        moduleCfgViewModel.GetAbsoluteBaseModulePath());

        /* Set configuration view */
        deleate_UpdateModuleView(moduleView);
      }
      catch (InvalidDataException)
      {
        //MessageBox.Show(ex.Message, "Third Party Module Error", MessageBoxButton.OK, MessageBoxImage.Error);

        /* Set default configuration view */
        deleate_UpdateModuleView(new CS_EmptyModuleCfgV());
      }
      catch(Exception ex)
      {
        string error = General.HyperspaceExceptions.E0004_UnknownThirdPartyModuleFailure +
            " Module Name: \"" + moduleCfgViewModel.ModuleName +
            "\" Configured Lib File: \"" + moduleCfgViewModel.GetAbsoluteModuleLibraryPath() +
            "\" Error message: " + ex;

        // Report error
        HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
          HyperspaceExceptions.E0004, error);

        /* Set default configuration view */
        deleate_UpdateModuleView(new CS_EmptyModuleCfgV());
      }
    }

    #region Helper functions
    /// <summary>
    /// Helper function which loads the module DLL
    /// This function would raise different Exceptions
    /// </summary>
    /// <param name="moduleCfgViewModel">Reference to used module config view model</param>
    private void LoadModuleDll(ref CS_ModuleCfgVM moduleCfgViewModel)
    {
      if (null == moduleCfgViewModel.ModuleLibrary)
      {
        // Check if all needed parameters are available

        // Check if module lib is available
        if (false == File.Exists(moduleCfgViewModel.GetAbsoluteModuleLibraryPath()))
        {
          string error = General.HyperspaceExceptions.E0001_ModuleLibNotFound +
            " Module Name: \"" + moduleCfgViewModel.ModuleName +
            "\" Configured Lib File: \"" + moduleCfgViewModel.GetAbsoluteModuleLibraryPath() +
            "\"";
          // Report error
          HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
            HyperspaceExceptions.E0001, error);

          // Throw exception
          throw new InvalidDataException(error);
        }

        // Check if module base path is available
        if (false == Directory.Exists(moduleCfgViewModel.GetAbsoluteBaseModulePath()))
        {
          string error = General.HyperspaceExceptions.E0002_ModuleBasePathNotFound +
            " Module Name: \"" + moduleCfgViewModel.ModuleName +
            "\" Configured Module Base Path: \"" + moduleCfgViewModel.GetAbsoluteBaseModulePath() +
            "\"";

          // Report error
          HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
            HyperspaceExceptions.E0002, error);

          // Throw exception
          throw new InvalidDataException(error);
        }

        // Check if module config file path is available
        if (false == Directory.Exists(System.IO.Path.GetDirectoryName(moduleCfgViewModel.GetAbsoluteModuleCfgFilePath())))
        {
          string error = General.HyperspaceExceptions.E0003_ModuleConfigFilePathNotFound +
            " Module Name: \"" + moduleCfgViewModel.ModuleName +
            "\" Configured Module Configuration File Path: \"" + System.IO.Path.GetDirectoryName(moduleCfgViewModel.GetAbsoluteModuleCfgFilePath()) +
            "\"";

          // Report error
          HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
            HyperspaceExceptions.E0003, error);

          // Throw exception
          throw new InvalidDataException(error);
        }

        // Everything is fine >> Continue with DLL load operation
        moduleCfgViewModel.ModuleLibrary = Assembly.LoadFrom(@"" + moduleCfgViewModel.GetAbsoluteModuleLibraryPath());

        Type moduleContextType = moduleCfgViewModel.ModuleLibrary.GetType("ModuleLibrary.ModuleContext");

        MethodInfo getInstanceMethode = moduleContextType.GetMethod("GetInstance");

        /* Access the module context interface in loaded DLL */
        object obj = Activator.CreateInstance(moduleContextType);
        moduleCfgViewModel.ModuleContext = (OsarModuleContextInterface)getInstanceMethode.Invoke(obj, null);
      }
    }
    #endregion
  }
}
/**
* @}
*/
