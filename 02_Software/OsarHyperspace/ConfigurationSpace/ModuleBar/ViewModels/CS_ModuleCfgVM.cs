﻿/*****************************************************************************************************************************
 * @file        CS_ModuleCfgVM.cs                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View Mode for the Configuration Space Module Config Data Model                         *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.ModuleBar.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarHyperspace.General;
using RteLib.RteConfig;
using System.Reflection;
using OsarResources.Generator;
using OsarHyperspace.ConfigurationSpace;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.ModuleBar.ViewModels
{
  /// <summary>
  /// Implementation of the View Model for the Used Module Structure
  /// </summary>
  public class CS_ModuleCfgVM : MenuBar.Project.ProjectSettings.ViewModels.PS_ModuleConfigurationCfgVM
  {
    //private CfgSpace_ModuleCfg moduleCfg = new CfgSpace_ModuleCfg();
    private Assembly moduleLibrary = null;
    private OsarModuleContextInterface moduleContext = null;

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    public CS_ModuleCfgVM(ref CfgSpace_ModuleCfg module) : base(ref module)
    {
    }

    /// <summary>
    /// Interface for the module lib DLL
    /// </summary>
    public Assembly ModuleLibrary
    {
      get { return moduleLibrary; }
      set { moduleLibrary = value; }
    }

    /// <summary>
    /// Interface for the module lib "ModuleContext" Instance in DLL
    /// </summary>
    public OsarModuleContextInterface ModuleContext
    {
      get { return moduleContext; }
      set { moduleContext = value; }
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/
