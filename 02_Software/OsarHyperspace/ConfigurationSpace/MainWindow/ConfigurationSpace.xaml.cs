﻿/*****************************************************************************************************************************
 * @file        ConfigurationSpace.cs                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        12.09.2019                                                                                                   *
 * @brief       Implementation of the Main Configuration Space Window                                                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MainWindow
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OsarHyperspace.ConfigurationSpace.MainWindow.Views;
using OsarHyperspace.General.Interfaces;
using OsarHyperspace.ConfigurationSpace.Logging;
using OsarHyperspace.ConfigurationSpace.ModuleBar;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
public delegate void ProvideNewUserControlView(object viewObject);
namespace OsarHyperspace.ConfigurationSpace.MainWindow
{
  /// <summary>
  /// Interaction logic for ConfigurationSpace.xaml
  /// </summary>
  public partial class ConfigurationSpace : Window
  {
    ModuleSelectorV moduleSelector;

    LoggingV usedLoggingV;

    /// <summary>
    /// Default constructor
    /// </summary>
    public ConfigurationSpace()
    {
      InitializeComponent();

      // Create new module selector
      ProvideNewUserControlView del_NewModuleView = new ProvideNewUserControlView(UpdateModuleDataView);
      moduleSelector = new ModuleSelectorV(del_NewModuleView);
      CC_ModuleSelector.DataContext = moduleSelector;

      // Create empty module configuration view
      CC_ModuleData.DataContext = new CS_EmptyModuleCfgV();

      // Create module log system
      usedLoggingV = new LoggingV();
      CC_Logging.DataContext = usedLoggingV;

      // Registration of Configuration Space main window in Hyperspace System
      General.HyperspaceSystem.Instance().HyperspaceConfigurationSpace = this;
    }

    #region UI Interfaces
    /// <summary>
    /// Runtime initialization of the Window
    /// </summary>
    public void InitializeWindow()
    {
      moduleSelector.InitializeWindow();
    }

    /// <summary>
    /// Event handler for closing the window
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CloseWindow(object sender, System.ComponentModel.CancelEventArgs e)
    {
      usedLoggingV.ShutdownLogging();
    }

    /// <summary>
    /// Interface to define a new data context in main window
    /// </summary>
    /// <param name="newView"></param>
    public void UpdateModuleDataView(object newView)
    {
      CC_ModuleData.DataContext = newView;
    }
    #endregion

    #region System Interfaces
    /// <summary>
    /// Event Interface to perform required actions during an project settings update
    /// </summary>
    public void EventProjectConfigurationChanged()
    {
      //Inform module selector about configuration change
      moduleSelector.EventProjectConfigurationChanged();
    }

    /// <summary>
    /// Interface to access the project logging System
    /// </summary>
    /// <returns></returns>
    public ProjectLoggingInterface GetProjectLogInstance()
    {
      return usedLoggingV.ProjectLogging;
    }

    /// <summary>
    /// Interface to access the system logging System
    /// </summary>
    /// <returns></returns>
    public SystemLoggingInterface GetSystemLogInstance()
    {
      return usedLoggingV.SystemLogging;
    }
    #endregion
  }
}
/**
* @}
*/

