﻿/*****************************************************************************************************************************
 * @file        CS_LoggingV.xaml.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Logging View                                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/

/**
* @addtogroup OsarHyperspace.ConfigurationSpace.Logging
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using OsarHyperspace.ConfigurationSpace.Logging.ViewModels;
using OsarHyperspace.General;
using OsarHyperspace.General.Interfaces;
using OsarHyperspace.General.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.Logging
{
  /// <summary>
  /// Interaction logic for CS_LoggingV.xaml
  /// </summary>
  public partial class LoggingV : UserControl
  {
    CS_LoggingViewVM loggingVM = new CS_LoggingViewVM();

    /// <summary>
    /// Constructor
    /// </summary>
    public LoggingV()
    {
      InitializeComponent();

      CC_ProjectLog.Content = loggingVM.ProjectLogView;
      CC_SystemLog.Content = loggingVM.SystemLogView;

      // Register messaging system
      Messenger.Default.Register<LoggingControlEvents>(this, (action) => ReceiveMessage(action));
    }

    /// <summary>
    /// System interface to initiate logging shutdown
    /// </summary>
    public void ShutdownLogging()
    {
      loggingVM.CS_ShutdownLogging();
    }

    #region Attributes
    /// <summary>
    /// Getter for the ProjectLogging Interface
    /// </summary>
    public ProjectLoggingInterface ProjectLogging
    {
      get { return loggingVM; }
    }

    /// <summary>
    /// Getter for the SystemLogging Interface
    /// </summary>
    public SystemLoggingInterface SystemLogging
    {
      get { return loggingVM; }
    }
    #endregion

    /**
     * @brief   Message receiver from the different modules / base view model
     * @details Using GalaSoft.MvvmLight.Messaging mechanism
     */
    private object ReceiveMessage(LoggingControlEvents actualEvent)
    {
      switch (actualEvent)
      {
        case LoggingControlEvents.NEW_PROJECT_LOG:
          {
            
            Dispatcher.Invoke(new Action(() =>
            {
              TC_LogTabControl.SelectedIndex = TI_ProjectLogTabItem.TabIndex;
              loggingVM.ProjectLogView.SV_ScrolBar.ScrollToEnd();
            }));
          }
        break;

        case LoggingControlEvents.NEW_SYSTEM_LOG:
          {
            Dispatcher.Invoke(new Action(() =>
            {
              TC_LogTabControl.SelectedIndex = TI_SystemLogTabItem.TabIndex;
              loggingVM.SystemLogView.SV_ScrolBar.ScrollToEnd();
            }));
          }
        break;

        default:
        break;
      
      }
      return null;
    }
  }
}
/**
* @}
*/
