﻿/*****************************************************************************************************************************
 * @file        CS_LoggingViewVM.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.12.2020                                                                                                   *
 * @brief       Implementation of the view model for the logging mechanism.                                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.Logging.ViewModels
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.General.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OsarHyperspace.General;
using System.Globalization;
using OsarHyperspace.ConfigurationSpace.Logging.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System.Windows.Threading;
using System.Collections.Concurrent;
using System.Threading;
using OsarHyperspace.General.Models;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.Logging.ViewModels
{
  public class CS_LoggingViewVM : ViewModelBase, ProjectLoggingInterface, SystemLoggingInterface
  {
    TextWriter systemLogWriter;
    TextWriter projectLogWriter;

    CS_LoggingSubViewV projectLogView = new CS_LoggingSubViewV();
    CS_LoggingSubViewV systemLogView = new CS_LoggingSubViewV();

    ConcurrentQueue<string> projectLogFileEntries = new ConcurrentQueue<string>();
    ConcurrentQueue<string> systemLogFileEntries = new ConcurrentQueue<string>();

    ThreadStart logWriterThreadDel;
    Thread logWriterThread;
    bool logWriterThreadShutdown = false;

    /// <summary>
    /// Constructor
    /// </summary>
    public CS_LoggingViewVM()
    {
      CreateNewSystemLogWriter();
      CreateNewProjectLogWriter();

      logWriterThreadDel = new ThreadStart(LogWriter);
      logWriterThread = new Thread(logWriterThreadDel);
      logWriterThread.SetApartmentState(ApartmentState.STA);
      logWriterThread.Start();

      /* Copy global system errors */
      foreach(Tuple<string, string> globalSystemError in HyperspaceSystem.Instance().GlobleSystemErrorList)
      {
        AddNewSystemErrorLog(globalSystemError.Item1, globalSystemError.Item2);
      }

      /* Copy global system warnings */
      foreach (Tuple<string, string> globalSystemWarning in HyperspaceSystem.Instance().GlobleSystemWarningList)
      {
        AddNewSystemWarningLog(globalSystemWarning.Item1, globalSystemWarning.Item2);
      }
    }

    /// <summary>
    /// Thread to process the log entries
    /// </summary>
    private void LogWriter()
    {
      do
      {
        Thread.Sleep(100);

        //Write logs from system
        while(0 < systemLogFileEntries.Count)
        {
          string logEntry;
          systemLogFileEntries.TryDequeue(out logEntry);
          systemLogWriter.WriteLine(logEntry);
        }

        //Write logs from project
        foreach (string entry in projectLogFileEntries)
        {
          string logEntry;
          projectLogFileEntries.TryDequeue(out logEntry);
          projectLogWriter.WriteLine(logEntry);
        }

        systemLogWriter.Flush();
        projectLogWriter.Flush();
      } while (false == logWriterThreadShutdown);
    }


    #region Attributes
    /// <summary>
    /// Getter for the projectLogView attribute
    /// </summary>
    public CS_LoggingSubViewV ProjectLogView
    {
      get { return projectLogView; }
    }

    /// <summary>
    /// Getter for the projectLogView attribute
    /// </summary>
    public CS_LoggingSubViewV SystemLogView
    {
      get { return systemLogView; }
    }

    /// <summary>
    /// API to  trigger a logging shutdown
    /// Stops logging thread
    /// </summary>
    public void CS_ShutdownLogging()
    {
      logWriterThreadShutdown = true;
    }
    #endregion

    #region Project Log Interface
    /// <summary>
    /// Interface to add a new Project Error Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewProjectErrorLog(string logName, string logContent)
    {
        projectLogView.AddNewLogMessage(LoggingType.ERROR, logName, logContent);
        ReportViewAnLogEvent(LoggingControlEvents.NEW_PROJECT_LOG);
    }

    /// <summary>
    /// Interface to add a new Project Warning Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewProjectWarningLog(string logName, string logContent)
    {
      projectLogView.AddNewLogMessage(LoggingType.WARNING, logName, logContent);
      ReportViewAnLogEvent(LoggingControlEvents.NEW_PROJECT_LOG);
    }

    /// <summary>
    /// Interface to add a new Project Info Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewProjectInfoLog(string logName, string logContent)
    {
      // Do nothing with this information
    }

    /// <summary>
    /// Interface to add a new Project Logging Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewProjectLoggingLog(string logName, string logContent)
    {
      // Do nothing with this information
    }

    /// <summary>
    /// Interface to add a new Project "all types" Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewProjectAllTypesLog(string logName, string logContent)
    {
      projectLogFileEntries.Enqueue(logName + " " + logContent);
    }

    /// <summary>
    /// Interface to clear the project logs
    /// </summary>
    public void ClearProjectLogs()
    {
      projectLogView.ClearLogMessages();

      //projectLogWriter.Close();
      CreateNewProjectLogWriter();
    }
    #endregion

    #region System Log Interface
    /// <summary>
    /// Interface to add a new System Error Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewSystemErrorLog(string logName, string logContent)
    {
      systemLogView.AddNewLogMessage(LoggingType.ERROR, logName, logContent);
      ReportViewAnLogEvent(LoggingControlEvents.NEW_SYSTEM_LOG);

      systemLogFileEntries.Enqueue(DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + " >> "
        + logName + " >> " + logContent);
    }

    /// <summary>
    /// Interface to add a new System Warning Log Entry
    /// </summary>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public void AddNewSystemWarningLog(string logName, string logContent)
    {
      systemLogView.AddNewLogMessage(LoggingType.WARNING, logName, logContent);
      ReportViewAnLogEvent(LoggingControlEvents.NEW_SYSTEM_LOG);

      systemLogFileEntries.Enqueue(DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + " >> "
        + logName + " >> " + logContent);
    }

    /// <summary>
    /// Interface to clear the System logs
    /// </summary>
    public void ClearSystemLogs()
    {
      systemLogView.ClearLogMessages();

      CreateNewSystemLogWriter();
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to create a new System log writer instance
    /// </summary>
    private void CreateNewSystemLogWriter()
    {
      string pathToSystemLogFile = HyperspaceConfig.Instance().GetAbsoluteHyperspaceConfigDirectory +
        GeneralRessource.HyperspaceLogFilePath + GeneralRessource.HyperspaceConfigSpaceLogFilePath +
        GeneralRessource.HyperspaceSystemLogPath + ".\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".\\" +
        DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".txt";

      Directory.CreateDirectory(Path.GetDirectoryName(pathToSystemLogFile));

      systemLogWriter = new StreamWriter(pathToSystemLogFile, true);
    }

    /// <summary>
    /// Helper function to create a new Project log writer instance
    /// </summary>
    private void CreateNewProjectLogWriter()
    {
      string pathToProjectLogFile = HyperspaceConfig.Instance().GetAbsoluteHyperspaceConfigDirectory +
        GeneralRessource.HyperspaceLogFilePath + GeneralRessource.HyperspaceConfigSpaceLogFilePath +
        GeneralRessource.HyperspaceProjectLogPath + ".\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".\\" +
        DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".txt";

      Directory.CreateDirectory(Path.GetDirectoryName(pathToProjectLogFile));

      projectLogWriter = new StreamWriter(pathToProjectLogFile, true);
    }

    /// <summary>
    /// API to report an specific log event
    /// </summary>
    /// <param name="actualEvent"></param>
    private void ReportViewAnLogEvent(LoggingControlEvents actualEvent)
    {
      Messenger.Default.Send<LoggingControlEvents>(actualEvent);
    }
    #endregion
  }
}
/**
* @}
*/