﻿/*****************************************************************************************************************************
 * @file        CS_LoggingEntryV.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        20.09.2019                                                                                                   *
 * @brief       Implementation of the logging entry view                                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.Logging.Views
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.General.Models;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.Logging.Views
{
  /// <summary>
  /// Interaction logic for CS_LoggingEntryV.xaml
  /// </summary>
  public partial class CS_LoggingEntryV : UserControl
  {
    /// <summary>
    /// Constructor to create a new log entry
    /// </summary>
    /// <param name="logType"></param>
    /// <param name="logName"></param>
    /// <param name="logContent"></param>
    public CS_LoggingEntryV(LoggingType logType, string logName, string logContent)
    {
      InitializeComponent();

      switch (logType)
      {
        case LoggingType.ERROR:
        {
            // Set Image
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri("./../../../Resources/Icons/bug.png", UriKind.Relative);
            image.EndInit();
            IMG_LogImage.Source = image;
            IMG_LogImage.Width = 15;

            // Set Name / Id
            R_LogName.Fill = Brushes.Red;
            L_LogName.Content = logName;

            // Set Log Content
            R_LogContent.Fill = Brushes.Red;
            TB_LogContent.Text = logContent;
        }
        break;

        case LoggingType.WARNING:
        {
            // Set Image
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri("./../../../Resources/Icons/attention.png", UriKind.Relative);
            image.EndInit();
            IMG_LogImage.Source = image;
            IMG_LogImage.Width = 15;

            // Set Name / Id
            R_LogName.Fill = Brushes.Orange;
            L_LogName.Content = logName;

            // Set Log Content
            R_LogContent.Fill = Brushes.Orange;
            TB_LogContent.Text = logContent;
          }
        break;

        default:
        {
            string error = OsarHyperspace.General.HyperspaceExceptions.E0000_GeneralProgrammingFailure +
            "Unhandled enumeration type";

            General.HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
              General.HyperspaceExceptions.E0000, error);

            throw new InvalidProgramException(error);
        }
        break;
      }
    }
  }
}
/**
* @}
*/
