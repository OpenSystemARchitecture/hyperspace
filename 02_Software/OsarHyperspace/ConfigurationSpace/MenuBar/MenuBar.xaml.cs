﻿/*****************************************************************************************************************************
 * @file        MenuBar.xaml.cs                                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        12.09.2019                                                                                                   *
 * @brief       Implementation of the UI Menu Bar for the Configuration Space.                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.ConfigurationSpace.MenuBar;
using OsarHyperspace.General;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar
{
  /// <summary>
  /// Interaction logic for the MenuBar.xaml
  /// </summary>
  public partial class MenuBar : UserControl
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    public MenuBar()
    {
      InitializeComponent();
    }

    //========================================================================================================================
    //=============================================== Menu "File" ============================================================
    //========================================================================================================================
    /// <summary>
    /// Interface to store the current project configuration
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void StoreCurrentConfiguration_Clicked(object sender, EventArgs e)
    {
      HyperspaceProjectConfig.Instance().StoreHyperspaceProjectConfigFile();
    }

    /// <summary>
    /// Interface to exit the current application
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void ExitApplication_Clicked(object sender, EventArgs e)
    {
      HyperspaceSystem.Instance().ShutdownConfigurationSpace();
    }

    //========================================================================================================================
    //============================================== Menu "Project" ==========================================================
    //========================================================================================================================
    /// <summary>
    /// Interface to start the project settings window as dialog
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void ButtonMenuProject_ProjectSetting_Clicked(object sender, EventArgs e)
    {
      Project.ProjectSettings.ProjectSettings menuProjectSettingsView = new Project.ProjectSettings.ProjectSettings();
      menuProjectSettingsView.ShowDialog();
    }

    /// <summary>
    /// Interface to start the Project Validation and Generation Window
    /// Attention: Validation an Generation is the SAME Window!
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void ButtonMenuProject_ProjectValidationGeneration_Clicked(object sender, EventArgs e)
    {
      Project.ValidateGenerate.ValidateAndGenerateModules moduleValidateGenerateView = new Project.ValidateGenerate.ValidateAndGenerateModules();
      moduleValidateGenerateView.ShowDialog();
    }

    #region Engine / Helper functions
    //========================================================================================================================
    //================================================== Other ===============================================================
    //========================================================================================================================
    /// <summary>
    /// Interface to show that the request function is not available. This function starts an error MessageBox with
    /// the corresponding error message.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FunctionNotImplemented(object sender, EventArgs e)
    {
      MessageBox.Show("Function not implemented!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
    }

    /// <summary>
    /// Interface to show basic system information system 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMenueHelp_About_Click(object sender, RoutedEventArgs e)
    {
      string content = "";
      content += GeneralRessource.CreatorInformation;
      content += "\n";
      content += "\n";

      content += "Version:\nv." + OsarHyperspaceVersionClass.getCurrentVersion();
      content += "\n";
      content += "\n";

      content += "License Information:\n";
      content += GeneralRessource.LicenseInformation;

      MessageBox.Show(content, "About Information", MessageBoxButton.OK, MessageBoxImage.Information);
    }

    #endregion
  }
}
/**
* @}
*/