﻿/*****************************************************************************************************************************
 * @file        PS_GeneralVM.cs                                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        17.09.2019                                                                                                   *
 * @brief       Implementation of the View Model for the Project Settings General View                                       *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.ViewModels
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarHyperspace.General;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.ViewModels
{
  /// <summary>
  /// View Model of the Project Settings "General" View
  /// </summary>
  public class PS_GeneralVM : BaseViewModel
  {
    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Constructor
    /// </summary>
    public PS_GeneralVM() : base()
    {
      //Nothing to do
    }

    /// <summary>
    /// View Model interface to set and read the actual Project name.
    /// The used fields are a part of the project configuration so no dedicated data model is needed. Using the
    /// HyperspaceProjectConfig instance as data model.
    /// </summary>
    public string ProjectName
    {
      get { return HyperspaceProjectConfig.Instance().ProjectName; }
      set { HyperspaceProjectConfig.Instance().ProjectName = value; }
    }

    /// <summary>
    /// View Model interface to set and read the actual Project Base Folder Path.
    /// The used fields are a part of the project configuration so no dedicated data model is needed. Using the
    /// HyperspaceProjectConfig instance as data model.
    /// </summary>
    public string ProjectBaseFolderPath
    {
      get { return HyperspaceProjectConfig.Instance().BaseProjectFolderPath; }
      set { HyperspaceProjectConfig.Instance().BaseProjectFolderPath = value; }
    }
    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/
