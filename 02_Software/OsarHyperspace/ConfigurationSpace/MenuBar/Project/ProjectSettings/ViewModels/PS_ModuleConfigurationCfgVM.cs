﻿/*****************************************************************************************************************************
 * @file        PS_ModuleConfigurationCfgVM.cs                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        17.09.2019                                                                                                   *
 * @brief       Implementation of the View Model for the Project Settings Module Configuration Cfg View                      *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.ViewModels
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarHyperspace.General;
using System.IO;
using System.ComponentModel;
using RteLib.RteConfig;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.ViewModels
{
  /// <summary>
  /// View Model of the Project Settings "ModuleConfigurationCfg" View
  /// </summary>
  public class PS_ModuleConfigurationCfgVM : BaseViewModel
  {
    protected CfgSpace_ModuleCfg moduleCfg;

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/

    #region Properties
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="module"></param>
    public PS_ModuleConfigurationCfgVM(ref CfgSpace_ModuleCfg module) : base()
    {
      moduleCfg = module;
    }

    /// <summary>
    /// Interface for Module Name
    /// </summary>
    public String ModuleName
    {
      get { return moduleCfg.ModuleName; }
      set
      {
        moduleCfg.ModuleName = value;
        OnPropertyChanged(new PropertyChangedEventArgs("ModuleType"));
      }
    }

    /// <summary>
    /// Interface for Module Type.
    /// As special here, default values would be set to all of the other fields if possible.
    /// </summary>
    public RteConfigModuleTypes ModuleType
    {
      get { return moduleCfg.ModuleType; }
      set
      {
        moduleCfg.ModuleType = value;
        OnPropertyChanged(new PropertyChangedEventArgs("ModuleType"));
      }
    }

    /// <summary>
    /// Interface for the module base folder
    /// </summary>
    public String ModuleBaseFolder
    {
      get { return moduleCfg.ModuleBaseFolder; }
      set
      {
        moduleCfg.ModuleBaseFolder = value;
        OnPropertyChanged(new PropertyChangedEventArgs("ModuleBaseFolder"));
      }
    }

    /// <summary>
    /// Interface for the Module Library Path
    /// </summary>
    public String ModuleLibPath
    {
      get { return moduleCfg.ModuleLibPath; }
      set
      {
        moduleCfg.ModuleLibPath = value;
        OnPropertyChanged(new PropertyChangedEventArgs("ModuleLibPath"));
      }
    }

    /// <summary>
    /// Interface for the Configuration File Path
    /// </summary>
    public String CfgFilePath
    {
      get { return moduleCfg.CfgFilePath; }
      set
      {
        moduleCfg.CfgFilePath = value;
        OnPropertyChanged(new PropertyChangedEventArgs("CfgFilePath"));
      }
    }

    /// <summary>
    /// Interface to select if the module shall be generated and validated
    /// </summary>
    public bool ValidateAndGenerateModule
    { get { return moduleCfg.ValidateAndGenerateModule; }
      set
      {
        moduleCfg.ValidateAndGenerateModule = value;
        OnPropertyChanged(new PropertyChangedEventArgs("ValidateAndGenerateModule"));
      }
    }

    /// <summary>
    /// Interface for the module generation priority
    /// </summary>
    public UInt16 ModuleGenerationPriority
    {
      get { return moduleCfg.ModuleGenerationPriority; }
      set
      {
        moduleCfg.ModuleGenerationPriority = value;
        OnPropertyChanged(new PropertyChangedEventArgs("ModuleGenerationPriority"));
      }
    }
    #endregion

    #region Actions / Interfaces
    /// <summary>
    /// Interface to request the absolute base mode path. If the path is already absolute,
    /// it would be return without any modification. If an relative path is given,
    /// it would be converted to an absolute by following sequence:
    /// "hyperspaceProjectConfigFilePath" + "baseProjectFolderPath" + "moduleCfg.ModuleBaseFolder"
    /// </summary>
    /// <returns>Absolute path string</returns>
    public string GetAbsoluteBaseModulePath()
    {
      string retVal;

      if (false == System.IO.Path.IsPathRooted(moduleCfg.ModuleBaseFolder))
      {
        retVal = HyperspaceProjectConfig.Instance().GetAbsoluteBaseProjectPath() + moduleCfg.ModuleBaseFolder;
      }
      else
      {
        retVal = moduleCfg.ModuleBaseFolder;
      }

      return retVal;
    }

    /// <summary>
    /// Interface to request the absolute mode config file path. If the path is already absolute,
    /// it would be return without any modification. If an relative path is given,
    /// it would be converted to an absolute by following sequence:
    /// "hyperspaceProjectConfigFilePath" + "baseProjectFolderPath" + "moduleCfg.ModuleBaseFolder" + "moduleCfg.CfgFilePath"
    /// </summary>
    /// <returns>Absolute path string</returns>
    public string GetAbsoluteModuleCfgFilePath()
    {
      string retVal;

      if (false == System.IO.Path.IsPathRooted(moduleCfg.CfgFilePath))
      {
        retVal = GetAbsoluteBaseModulePath() + moduleCfg.CfgFilePath;
      }
      else
      {
        retVal = moduleCfg.CfgFilePath;
      }

      return retVal;
    }

    /// <summary>
    /// Interface to request the absolute mode library path. If the path is already absolute,
    /// it would be return without any modification. If an relative path is given,
    /// it would be converted to an absolute by following sequence:
    /// "hyperspaceProjectConfigFilePath" + "baseProjectFolderPath" + "moduleCfg.ModuleBaseFolder" + "moduleCfg.CfgFilePath"
    /// </summary>
    /// <returns>Absolute path string</returns>
    public string GetAbsoluteModuleLibraryPath()
    {
      string retVal;

      if (false == System.IO.Path.IsPathRooted(moduleCfg.ModuleLibPath))
      {
        retVal = GetAbsoluteBaseModulePath() + moduleCfg.ModuleLibPath;
      }
      else
      {
        retVal = moduleCfg.ModuleLibPath;
      }

      return retVal;
    }
    #endregion

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/
