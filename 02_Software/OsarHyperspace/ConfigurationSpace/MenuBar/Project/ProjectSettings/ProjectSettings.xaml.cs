﻿/*****************************************************************************************************************************
 * @file        ProjectSettings.xaml.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.09.2019                                                                                                   *
 * @brief       Implementation of the View for the Module Validation and Generation Window                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings
{
  /// <summary>
  /// Interaction logic for ProjectSettings.xaml
  /// </summary>
  public partial class ProjectSettings : Window
  {
    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Constructor
    /// </summary>
    public ProjectSettings()
    {
      InitializeComponent();
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Perform closing window actions
    /// >> Store project configuration file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CloseWindow(object sender, System.ComponentModel.CancelEventArgs e)
    {
      // Update Project Configuration
      HyperspaceProjectConfig.Instance().StoreHyperspaceProjectConfigFile();

      // Inform System about project change
      HyperspaceSystem.Instance().EventProjectConfigurationChanged();
    }
  }
}
/**
* @}
*/
