﻿/*****************************************************************************************************************************
 * @file        PS_GeneralV.cs                                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        17.09.2019                                                                                                   *
 * @brief       Implementation of the View functions for the Project Settings General View                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings;
using System.IO;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
{
  /// <summary>
  /// Interaction logic for PS_General.xaml
  /// </summary>
  public partial class PS_GeneralV : UserControl
  {
    /// <summary>
    /// Used View Model
    /// </summary>
    private ViewModels.PS_GeneralVM projSettings = new ViewModels.PS_GeneralVM();

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Constructor
    /// </summary>
    public PS_GeneralV()
    {
      InitializeComponent();

      // Set Data Context where data is binded to
      this.DataContext = projSettings;
    }

    /// <summary>
    /// Select Base Folder Path Button clicked. Start select folder dialog.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SelectBaseFolderPath_Click(object sender, RoutedEventArgs e)
    {
      using(var fbd = new System.Windows.Forms.FolderBrowserDialog())
      {
        System.Windows.Forms.DialogResult result = fbd.ShowDialog();

        if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
        {
          // Set path in view
          TB_ProjectBaseFolderPath.Text = fbd.SelectedPath;

          // Set path in project file
          projSettings.ProjectBaseFolderPath = fbd.SelectedPath;
          
        }
      }
    }

    /// <summary>
    /// Button to convert a absolute folder path to an relative one
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SetBaseFolderPathAsRelative_Click(object sender, RoutedEventArgs e)
    {
      if(true == System.IO.Path.IsPathRooted(TB_ProjectBaseFolderPath.Text))
      {
        var folderUri = new Uri(TB_ProjectBaseFolderPath.Text + "/", UriKind.Absolute);

        string projectFilePath = OsarHyperspace.General.HyperspaceConfig.Instance().LastUsedProjectFile;
        string projectFileFolderPath = System.IO.Path.GetDirectoryName(projectFilePath) + "/";
        var referenceUri = new Uri(projectFileFolderPath, UriKind.Absolute);
        string relativePath = "./" + referenceUri.MakeRelativeUri(folderUri).ToString();

        // Set path in view
        TB_ProjectBaseFolderPath.Text = relativePath;

        // Set path in project file
        projSettings.ProjectBaseFolderPath = relativePath;
      }
      else
      {
        MessageBox.Show("Could not convert path. Path is already relative!", "Conversion failure", MessageBoxButton.OK);
      }
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/
