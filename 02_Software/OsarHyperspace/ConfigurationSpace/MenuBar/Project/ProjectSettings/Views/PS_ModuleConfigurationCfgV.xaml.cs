﻿/*****************************************************************************************************************************
 * @file        PS_ModuleConfigurationCfgV.cs                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        17.09.2019                                                                                                   *
 * @brief       Implementation of the View for the Project Setting Module Configuration Cfg                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarHyperspace.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
{
  /// <summary>
  /// Interaction logic for PS_ModuleConfigurationCfgV.xaml
  /// </summary>
  public partial class PS_ModuleConfigurationCfgV : UserControl
  {
    private ViewModels.PS_ModuleConfigurationCfgVM moduleCfgVM;
    private CfgSpace_ModuleCfg moduleConfiguration;

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="moduleCfg"></param>
    public PS_ModuleConfigurationCfgV(ref CfgSpace_ModuleCfg moduleCfg)
    {
      InitializeComponent();

      moduleConfiguration = moduleCfg;
      moduleCfgVM = new ViewModels.PS_ModuleConfigurationCfgVM(ref moduleCfg);
      this.DataContext = moduleCfgVM;
    }

    /// <summary>
    /// Getter interface for the active module configuration
    /// </summary>
    public CfgSpace_ModuleCfg ModuleConfiguration
    { get { return moduleConfiguration; } }

    /// <summary>
    /// Button to select the module base folder
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_ModuleBaseFolderPath_Click(object sender, RoutedEventArgs e)
    {
      using (var fbd = new System.Windows.Forms.FolderBrowserDialog())
      {
        System.Windows.Forms.DialogResult result = fbd.ShowDialog();

        if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
        {
          // Set path in view
          TB_ModuleBaseFolder.Text = fbd.SelectedPath;

          // Set path in project file
          moduleConfiguration.ModuleBaseFolder = fbd.SelectedPath;
        }
      }
    }

    /// <summary>
    /// Button to convert absolute path into an relative path of Module Base Folder
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_ModuleBaseFolderRelativePath_Click(object sender, RoutedEventArgs e)
    {
      if (true == System.IO.Path.IsPathRooted(TB_ModuleBaseFolder.Text))
      {
        var folderUri = new Uri(TB_ModuleBaseFolder.Text, UriKind.Absolute);

        string projectBaseFolder = OsarHyperspace.General.HyperspaceProjectConfig.Instance().GetAbsoluteBaseProjectPath();
        var referenceUri = new Uri(projectBaseFolder, UriKind.Absolute);
        string relativePath = "./" + referenceUri.MakeRelativeUri(folderUri).ToString();

        // Set path in view
        TB_ModuleBaseFolder.Text = relativePath;

        // Set path in project file
        moduleConfiguration.ModuleBaseFolder = relativePath;
      }
      else
      {
        MessageBox.Show("Could not convert path. Path is already relative!", "Conversion failure", MessageBoxButton.OK);
      }
    }

    /// <summary>
    /// Button to select the module library file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_ModuleLibFilePath_Click(object sender, RoutedEventArgs e)
    {
      using (var openFileDialog = new System.Windows.Forms.OpenFileDialog())
      {
        openFileDialog.InitialDirectory = HyperspaceProjectConfig.Instance().GetAbsoluteBaseProjectPath();
        openFileDialog.Filter = "Module Library |*" + ".dll";

        if ( System.Windows.Forms.DialogResult.OK == openFileDialog.ShowDialog())
        {
          // Set path in view
          TB_ModuleLibraryFile.Text = openFileDialog.FileName;

          // Set path in project file
          moduleConfiguration.ModuleLibPath = openFileDialog.FileName;
        }
      }
    }

    /// <summary>
    /// Button to convert absolute path into an relative path of Module Library File
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_ModuleLibFileRelativePath_Click(object sender, RoutedEventArgs e)
    {
      if (true == System.IO.Path.IsPathRooted(TB_ModuleLibraryFile.Text))
      {
        var folderUri = new Uri(TB_ModuleLibraryFile.Text, UriKind.Absolute);

        string moduleBaseFolder = moduleCfgVM.GetAbsoluteBaseModulePath() + "/";
        var referenceUri = new Uri(moduleBaseFolder, UriKind.Absolute);
        string relativePath = "./" + referenceUri.MakeRelativeUri(folderUri).ToString();

        // Set path in view
        TB_ModuleLibraryFile.Text = relativePath;

        // Set path in project file
        moduleConfiguration.ModuleLibPath = relativePath;
      }
      else
      {
        MessageBox.Show("Could not convert path. Path is already relative!", "Conversion failure", MessageBoxButton.OK);
      }
    }

    private void B_ModuleCfgFilePath_Click(object sender, RoutedEventArgs e)
    {
      using (var openFileDialog = new System.Windows.Forms.OpenFileDialog())
      {
        openFileDialog.InitialDirectory = HyperspaceProjectConfig.Instance().GetAbsoluteBaseProjectPath();
        openFileDialog.Filter = "Module Configuration File |*" + ".xml";

        if (System.Windows.Forms.DialogResult.OK == openFileDialog.ShowDialog())
        {
          // Set path in view
          TB_ModuleConfigurationFile.Text = openFileDialog.FileName;

          // Set path in project file
          moduleConfiguration.CfgFilePath = openFileDialog.FileName;
        }
      }
    }

    /// <summary>
    /// Button to convert absolute path into an relative path of Module Configuration File
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_ModuleCfgFileRelativePath_Click(object sender, RoutedEventArgs e)
    {
      if (true == System.IO.Path.IsPathRooted(TB_ModuleConfigurationFile.Text))
      {
        var folderUri = new Uri(TB_ModuleConfigurationFile.Text, UriKind.Absolute);

        string moduleBaseFolder = moduleCfgVM.GetAbsoluteBaseModulePath() + "/";
        var referenceUri = new Uri(moduleBaseFolder, UriKind.Absolute);
        string relativePath = "./" + referenceUri.MakeRelativeUri(folderUri).ToString();

        // Set path in view
        TB_ModuleConfigurationFile.Text = relativePath;

        // Set path in project file
        moduleConfiguration.CfgFilePath = relativePath;
      }
      else
      {
        MessageBox.Show("Could not convert path. Path is already relative!", "Conversion failure", MessageBoxButton.OK);
      }
    }





    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/
