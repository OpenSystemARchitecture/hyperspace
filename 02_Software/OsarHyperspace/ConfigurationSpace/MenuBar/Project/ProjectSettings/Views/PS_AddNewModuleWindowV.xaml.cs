﻿/*****************************************************************************************************************************
 * @file        PS_AddNewModuleWindowV.xaml.cs                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        17.09.2019                                                                                                   *
 * @brief       Implementation of the View for the Project Settings Add new Module Cfg View                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.ViewModels;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
{
  /// <summary>
  /// Interaction logic for PS_AddNewModuleWindowV.xaml
  /// </summary>
  public partial class PS_AddNewModuleWindowV : Window
  {
    private PS_ModuleConfigurationCfgV usedModuleV;
    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    public PS_AddNewModuleWindowV(ref PS_ModuleConfigurationCfgV moduleCfgV)
    {
      InitializeComponent();

      usedModuleV = moduleCfgV;

      CC_ModuleConfiguration.Content = usedModuleV;
    }

    /// <summary>
    /// Button to confirm new module
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewModule_Click(object sender, RoutedEventArgs e)
    {
      DialogResult = true;
    }
    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/
