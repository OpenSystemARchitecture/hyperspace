﻿/*****************************************************************************************************************************
 * @file        PS_ModuleConfigurationV.cs                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        17.09.2019                                                                                                   *
 * @brief       Implementation of the View for the Project Setting Module Configuration                                      *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings;
using OsarHyperspace.General;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ProjectSettings.Views
{
  /// <summary>
  /// Interaction logic for PS_ModuleConfigurationV.xaml
  /// </summary>
  public partial class PS_ModuleConfigurationV : UserControl
  {
    private List<Views.PS_ModuleConfigurationCfgV> moduleConfigurationViews = new List<Views.PS_ModuleConfigurationCfgV>();

    public PS_ModuleConfigurationV()
    {
      InitializeComponent();

      InitializeViewModel_ModuleCfg();

      // Show first element
      if (0 != moduleConfigurationViews.Count())
      {
        CC_ModuleConfiguration.Content = moduleConfigurationViews[0];
      }

    }

    /// <summary>
    /// Initialization of the View Model Module Cfg
    /// </summary>
    private void InitializeViewModel_ModuleCfg()
    {
      /* Request Module List */
      for(int idx = 0; idx < General.HyperspaceProjectConfig.Instance().CfgSpaceModuleList.Count(); idx++)
      {
        var module = General.HyperspaceProjectConfig.Instance().CfgSpaceModuleList[idx];
        Views.PS_ModuleConfigurationCfgV view = new Views.PS_ModuleConfigurationCfgV(ref module);


        moduleConfigurationViews.Add(view);

        /* Setup necessary Buttons */
        Button moduleButton = new Button();
        moduleButton.Content = module.ModuleName;
        moduleButton.Name = "Button" + module.ModuleName;
        moduleButton.Margin = new Thickness(5);
        if (RteLib.RteConfig.RteConfigModuleTypes.CDD == module.ModuleType)
        {
          moduleButton.Background = Brushes.LawnGreen;
        }
        else if(RteLib.RteConfig.RteConfigModuleTypes.BSW == module.ModuleType)
        {
          moduleButton.Background = Brushes.LightBlue;
        }
        else if (RteLib.RteConfig.RteConfigModuleTypes.MCAL == module.ModuleType)
        {
          moduleButton.Background = Brushes.Orange;
        }
        else
        {
          moduleButton.Background = Brushes.Red;
        }

        moduleButton.Tag = view;
        moduleButton.Click += ModuleButton_Click;

        SP_ModuleButtonList.Children.Add(moduleButton);
      }
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Module Button clicked interface >> Load corresponding module dll and load view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ModuleButton_Click(object sender, RoutedEventArgs e)
    {
      Button activeButton = (Button)sender;

      // Update Content Control
      CC_ModuleConfiguration.Content = (Views.PS_ModuleConfigurationCfgV)activeButton.Tag;
    }

    /// <summary>
    /// Button to add a new Module to the project configuration. Therefor a additional dialog would be started.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewModule_Click(object sender, RoutedEventArgs e)
    {
      // Create new module configuration
      CfgSpace_ModuleCfg moduleCfg = new CfgSpace_ModuleCfg();

      // Create new Module Configuration Cfg View
      PS_ModuleConfigurationCfgV moduleCfgView = new PS_ModuleConfigurationCfgV(ref moduleCfg);

      // Setup new add new module window
      PS_AddNewModuleWindowV moduleWindow = new PS_AddNewModuleWindowV(ref moduleCfgView);

      // Show Window
      moduleWindow.ShowDialog();

      if ((true == moduleWindow.DialogResult.HasValue) && (true == moduleWindow.DialogResult.Value))
      {
        // Add new module to project configuration list
        HyperspaceProjectConfig.Instance().CfgSpaceModuleList.Add(moduleCfg);

        // Clear all internal lists
        moduleConfigurationViews.Clear();
        SP_ModuleButtonList.Children.Clear();

        // Setup new configuration list
        InitializeViewModel_ModuleCfg();

        // Show first element
        if (0 != moduleConfigurationViews.Count())
        {
          CC_ModuleConfiguration.Content = moduleConfigurationViews[0];
        }

      }
      else
      {
        // Noting to do >> Element would not be stored
      }

    }

    /// <summary>
    /// Button to delete active module config from module list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_DeletSelectedModule_Click(object sender, RoutedEventArgs e)
    {
      Views.PS_ModuleConfigurationCfgV activeView = (Views.PS_ModuleConfigurationCfgV)CC_ModuleConfiguration.Content;

      // Remove selected element from project configuration
      HyperspaceProjectConfig.Instance().CfgSpaceModuleList.Remove(activeView.ModuleConfiguration);

      // Clear all internal lists
      moduleConfigurationViews.Clear();
      SP_ModuleButtonList.Children.Clear();

      // Setup new configuration list
      InitializeViewModel_ModuleCfg();

      // Show first element
      if (0 != moduleConfigurationViews.Count())
      {
        CC_ModuleConfiguration.Content = moduleConfigurationViews[0];
      }
    }
  }
}
/**
* @}
*/
