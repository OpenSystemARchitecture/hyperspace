﻿/*****************************************************************************************************************************
 * @file        VG_ModuleCfgVM.cs                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.09.2019                                                                                                   *
 * @brief       Implementation of the View Model for the Module Validation and Generation Window                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using OsarHyperspace.General;
using OsarHyperspace.ConfigurationSpace;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.Models;
using System.Windows.Media;
using System.Threading;
using System.IO;
using System.Reflection;
using OsarResources.Generator;
using System.Windows.Threading;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.ViewModels
{
  /// <summary>
  /// View Model of the Module Cfg View for the Module Validation and Generation Window
  /// </summary>
  public class VG_ModuleCfgVM : ModuleBar.ViewModels.CS_ModuleCfgVM
  {
    private VG_ModuleCfgStatusUiDataM moduleStatusInformation;

    CfgSpace_ModuleCfg activeModule;

    ThreadStart validationThreadDel;
    Thread validationThread;

    ThreadStart generationThreadDel;
    Thread generationThread;

    bool taskActive = false;

    GenInfoType validationGenInfos, generationGenInfos;

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="module"> Used module of the project configuration </param>
    public VG_ModuleCfgVM(ref CfgSpace_ModuleCfg module) : base(ref module)
    {
      // Create status information
      moduleStatusInformation = new VG_ModuleCfgStatusUiDataM();

      activeModule = module;

      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatus"));
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatusColor"));
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatus"));
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatusColor"));
    }

    #region Properties
    /// <summary>
    /// Getter and Setter for the Validation Status String
    /// </summary>
    public string ValidationStatus { get => moduleStatusInformation.ValidationStatus;
      set => moduleStatusInformation.ValidationStatus = value; }

    /// <summary>
    /// Getter and Setter for the Validation Status Color
    /// </summary>
    public Brush ValidationStatusColor { get => moduleStatusInformation.ValidationStatusColor;
      set => moduleStatusInformation.ValidationStatusColor = value; }

    /// <summary>
    /// Getter and Setter for the Generation Status String
    /// </summary>
    public string GenerationStatus { get => moduleStatusInformation.GenerationStatus;
      set => moduleStatusInformation.GenerationStatus = value; }

    /// <summary>
    /// Getter and Setter for the Generation Status Color
    /// </summary>
    public Brush GenerationStatusColor { get => moduleStatusInformation.GenerationStatusColor;
      set => moduleStatusInformation.GenerationStatusColor = value; }

    /// <summary>
    /// Getter for the Task Active Status
    /// </summary>
    public bool IsTaskActive { get => taskActive; }
    #endregion

    #region Actions

    /// <summary>
    /// Interface to start the module validation sequence
    /// </summary>
    public void StartModuleValiation()
    {
      // Change UI Status to in progress
      SetValidationStatusToInProgress();

      try
      {
        LoadModuleDll();

        // Start validation Task
        validationThreadDel = new ThreadStart(ValidationTask);
        validationThread = new Thread(validationThreadDel);
        validationThread.SetApartmentState(ApartmentState.STA);
        validationThread.Start();
      }
      catch (Exception)
      {
        SetValidationStatusToFailed();

        // Forward exception
        throw;
      }
    }

    /// <summary>
    /// Interface to start the module generation sequence
    /// </summary>
    public void StartModuleGeneration()
    {
      // Reset Status Information
      SetValidationStatusToInit();
      SetGenerationStatusToInit();

      try
      {
        LoadModuleDll();

        // Start generation Task
        generationThreadDel = new ThreadStart(GenerationTask);
        generationThread = new Thread(generationThreadDel);
        generationThread.SetApartmentState(ApartmentState.STA);
        generationThread.Start();
      }
      catch (Exception)
      {
        SetGenerationStatusToFailed();

        // Forward exception
        throw;
      }
    }
    #endregion

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    #region Thread Functions
    /// <summary>
    /// Task which performs the validation action
    /// </summary>
    private void ValidationTask()
    {
      taskActive = true;

      try
      {
        validationGenInfos = new GenInfoType();
        validationGenInfos = ModuleContext.GetViewModel(GetAbsoluteModuleCfgFilePath(), GetAbsoluteBaseModulePath()).ValidateConfiguration();

        if (0 == validationGenInfos.error.Count)
        {
          Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
          {
            SetValidationStatusToSuccessful();
          }));
        }
        else
        {
          Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
          {
            SetValidationStatusToFailed();
          }));
        }

        // Write Logs to logging system
        Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
        {
          foreach (string log in validationGenInfos.warning)
          {
            HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().AddNewProjectWarningLog(ModuleName, log);
          }
          foreach (string log in validationGenInfos.error)
          {
            HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().AddNewProjectErrorLog(ModuleName, log);
          }
          foreach (string log in validationGenInfos.info)
          {
            HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().AddNewProjectInfoLog(ModuleName, log);
          }
          foreach (string log in validationGenInfos.log)
          {
            HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().AddNewProjectLoggingLog(ModuleName, log);
          }
          foreach (string log in validationGenInfos.allSeq)
          {
            HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().AddNewProjectAllTypesLog(ModuleName, log);
          }
        }));

      }
      catch (Exception ex)
      {
        Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
        {
          HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
            HyperspaceExceptions.E0004, HyperspaceExceptions.E0004_UnknownThirdPartyModuleFailure +
            " Module Name: " + activeModule.ModuleName + " >> Error message: " + ex);

          SetValidationStatusToFailed();
        }));
      }


      taskActive = false;
    }

    /// <summary>
    /// Task which performs the generation action
    /// </summary>
    void GenerationTask()
    {
      taskActive = true;
      // Before generating data >> Validate configuration
      StartModuleValiation();

      //while(false == validationTask.IsCompleted)
      while(true == validationThread.IsAlive)
      {
        Task.Delay(10);
      }

      // Ensure closing of all related functionaries
      Task.Delay(500);

      // Check if validation has thrown an error
      if (0 == validationGenInfos.error.Count)
      {
        // Validation successful set in progress status
        Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
        {
          SetGenerationStatusToInProgress();
        }));

        // Start generation
        generationGenInfos = new GenInfoType();
        generationGenInfos.error = new List<string>();
        generationGenInfos.warning = new List<string>();
        try
        {
          generationGenInfos = ModuleContext.GetViewModel(GetAbsoluteModuleCfgFilePath(), GetAbsoluteBaseModulePath()).GenerateConfiguration();
        }
        catch (Exception ex)
        {
          Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
          {
            HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
              HyperspaceExceptions.E0004, HyperspaceExceptions.E0004_UnknownThirdPartyModuleFailure +
              " Module Name: " + activeModule.ModuleName + " >> Error message: " + ex);
            
            SetGenerationStatusToFailed();
          }));
        }

        if (0 == generationGenInfos.error.Count)
        {
          // Generation successful 
          Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
          {
            SetGenerationStatusToSuccessful();
          }));
        }
        else
        {
          // Generation failed
          Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
          {
            SetGenerationStatusToFailed();
          }));
        }

        // Write Logs to logging system
        Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
        {
          foreach (string log in generationGenInfos.warning)
          {
            HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().AddNewProjectWarningLog(ModuleName, log);
          }
          foreach (string log in generationGenInfos.error)
          {
            HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().AddNewProjectErrorLog(ModuleName, log);
          }
        }));
      }
      else
      {
        // Validation failed >> Set Status to error
        Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
        {
          SetGenerationStatusToFailed();
        }));
      }

      taskActive = false;
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function which loads the module DLL
    /// This function would raise different Exceptions
    /// </summary>
    private void LoadModuleDll()
    {
      if(null == ModuleLibrary)
      {
        // Check if module lib is available
        if(false == File.Exists(GetAbsoluteModuleLibraryPath()))
        {
          string error = General.HyperspaceExceptions.E0001_ModuleLibNotFound +
            " Module Name: \"" + ModuleName +
            "\" Configured Lib File: \"" + GetAbsoluteModuleLibraryPath() +
            "\"";
          // Report error
          HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
            HyperspaceExceptions.E0001, error);

          // Throw exception
          throw new InvalidDataException(error);
        }

        // Check if module base path is available
        if(false == Directory.Exists(GetAbsoluteBaseModulePath()))
        {
          string error = General.HyperspaceExceptions.E0002_ModuleBasePathNotFound +
            " Module Name: \"" + ModuleName +
            "\" Configured Module Base Path: \"" + GetAbsoluteBaseModulePath() +
            "\"";

          // Report error
          HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
            HyperspaceExceptions.E0002, error);

          // Throw exception
          throw new InvalidDataException(error);
        }

        // Check if module config file path is available
        if (false == Directory.Exists(Path.GetDirectoryName(GetAbsoluteModuleCfgFilePath())))
        {
          string error = General.HyperspaceExceptions.E0003_ModuleConfigFilePathNotFound +
            " Module Name: \"" + ModuleName +
            "\" Configured Module Configuration File Path: \"" + Path.GetDirectoryName(GetAbsoluteModuleCfgFilePath()) +
            "\"";

          // Report error
          HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
            HyperspaceExceptions.E0003, error);

          // Throw exception
          throw new InvalidDataException(error);
        }

        // Everything is fine >> Continue with DLL load operation
        ModuleLibrary = Assembly.LoadFrom(@"" + GetAbsoluteModuleLibraryPath());

        Type moduleContextType = ModuleLibrary.GetType("ModuleLibrary.ModuleContext");

        MethodInfo getInstanceMethode = moduleContextType.GetMethod("GetInstance");

        /* Access the module context interface in loaded DLL */
        object obj = Activator.CreateInstance(moduleContextType);
        ModuleContext = (OsarModuleContextInterface)getInstanceMethode.Invoke(obj, null);
      }
    }

    /// <summary>
    /// Setter interface for the validation status fields in case of initialization
    /// </summary>
    public void SetValidationStatusToInit()
    {
      moduleStatusInformation.SetValidationStatusToInit();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatus"));

      ValidationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatusColor"));
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of initialization
    /// </summary>
    public void SetGenerationStatusToInit()
    {
      moduleStatusInformation.SetGenerationStatusToInit();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatus"));

      GenerationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatusColor"));
    }

    /// <summary>
    /// Setter interface for the validation status fields in case of in progress
    /// </summary>
    private void SetValidationStatusToInProgress()
    {
      moduleStatusInformation.SetValidationStatusToInProgress();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatus"));

      ValidationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatusColor"));
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of in progress
    /// </summary>
    private void SetGenerationStatusToInProgress()
    {
      moduleStatusInformation.SetGenerationStatusToInProgress();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatus"));

      GenerationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatusColor"));
    }

    /// <summary>
    /// Setter interface for the validation status fields in case of in failed
    /// </summary>
    private void SetValidationStatusToFailed()
    {
      moduleStatusInformation.SetValidationStatusToFailed();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatus"));

      ValidationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatusColor"));
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of in failed
    /// </summary>
    private void SetGenerationStatusToFailed()
    {
      moduleStatusInformation.SetGenerationStatusToFailed();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatus"));

      GenerationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatusColor"));
    }

    /// <summary>
    /// Setter interface for the validation status fields in case of in successful
    /// </summary>
    private void SetValidationStatusToSuccessful()
    {
      moduleStatusInformation.SetValidationStatusToSuccessful();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatus"));

      ValidationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("ValidationStatusColor"));
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of in successful
    /// </summary>
    private void SetGenerationStatusToSuccessful()
    {
      moduleStatusInformation.SetGenerationStatusToSuccessful();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatus"));

      GenerationStatusColor.Freeze();
      OnPropertyChanged(new PropertyChangedEventArgs("GenerationStatusColor"));
    }

    #endregion
  }
}
/**
* @}
*/
