﻿/*****************************************************************************************************************************
 * @file        ValidateAndGenerateModules.xaml.cs                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.09.2019                                                                                                   *
 * @brief       Implementation of the View for the Module Validation and Generation Window                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.Models;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.Views;
using OsarHyperspace.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.ViewModels
{
  public class VG_MainVM : ViewModelBase
  {
    private List<VG_ModuleCfgV> moduleViewList = new List<VG_ModuleCfgV>();
    private List<VG_ModuleCfgVM> moduleViewModelList = new List<VG_ModuleCfgVM>();
    private List<VG_ModuleCfgVM> tmpViewModelList = new List<VG_ModuleCfgVM>();

    private List<List<VG_ModuleCfgVM>> moduleGenerationList = new List<List<VG_ModuleCfgVM>>();

    ThreadStart validationThreadDel;
    Thread validationThread;

    ThreadStart generationThreadDel;
    Thread generationThread;

    /**
     * @brief     Constructor
     * @details   Setup internal list
     */
    public VG_MainVM()
    {
      // Initialize internal lists
      for (int idx = 0; idx < General.HyperspaceProjectConfig.Instance().CfgSpaceModuleList.Count; idx++)
      {
        var module = General.HyperspaceProjectConfig.Instance().CfgSpaceModuleList[idx];
        VG_ModuleCfgVM moduleViewModel = new VG_ModuleCfgVM(ref module);
        VG_ModuleCfgV moduleView = new VG_ModuleCfgV(ref moduleViewModel);

        if (( module.ModuleType == RteLib.RteConfig.RteConfigModuleTypes.BSW ) ||
           ( module.ModuleType == RteLib.RteConfig.RteConfigModuleTypes.CDD ) ||
           ( module.ModuleType == RteLib.RteConfig.RteConfigModuleTypes.MCAL ))
        {
          // Add Module into module list
          moduleViewList.Add(moduleView);
          moduleViewModelList.Add(moduleViewModel);
          tmpViewModelList.Add(moduleViewModel);
        }
      }

      CreateGenerationList();
    }

    #region Module Validation
    /**
     * @brief     API to start the validation process of the different modules
     */
    public void ValidateModules()
    {
      // Cleanup Project Logs
      HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().ClearProjectLogs();

      // Reset status fields
      ResetAllElementStatusFiels();

      // Start validation Task
      validationThreadDel = new ThreadStart(ValidationTask);
      validationThread = new Thread(validationThreadDel);
      validationThread.SetApartmentState(ApartmentState.STA);
      validationThread.Start();
    }

    /**
     * @brief     Validation Task
     * @details   Start validation of the selected module due to their generation priority
     */
    private void ValidationTask()
    {
      List<VG_ModuleCfgVM> activeModulsList = new List<VG_ModuleCfgVM>();

      // Process the different Priority Levels
      foreach (List<VG_ModuleCfgVM> prioModuleList in moduleGenerationList)
      {
        activeModulsList = new List<VG_ModuleCfgVM>();
        bool allTasksFinished;

        // Check each module
        foreach(VG_ModuleCfgVM module in prioModuleList)
        {
          if(true == module.ValidateAndGenerateModule)
          {
            activeModulsList.Add(module);

            // Start validation
            try
            {
              module.StartModuleValiation();
            }
            catch (System.IO.InvalidDataException)
            {
              //Nothing to do here >> Already handled and reported by lower layer
            }
            catch (Exception ex)
            {
              Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
              {
                string error = General.HyperspaceExceptions.E0004_UnknownThirdPartyModuleFailure +
                " Module Name: \"" + module.ModuleName +
                "\" Configured Lib File: \"" + module.GetAbsoluteModuleLibraryPath() +
                "\" Error message: " + ex;

                // Report error
                HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
                  HyperspaceExceptions.E0004, error);
              }));
            }
          }
        }

        // Check if generation has finished
        allTasksFinished = true;
        do
        {
          // Sleep before evaluating the task status from the modules
          Thread.Sleep(100);

          foreach (VG_ModuleCfgVM module in activeModulsList)
          {
            // Check module task status
            if(true == module.IsTaskActive)
            {
              allTasksFinished = true;
              break;
            }
          }
        } while (false == allTasksFinished);
      }
    }
    #endregion

    #region Module Generation
    /**
     * @brief     API to start the generation process of the different modules
     */
    public void GenerateModules()
    {
      // Cleanup Project Logs
      HyperspaceSystem.Instance().ConfigSpaceProjectLogInstance().ClearProjectLogs();

      // Reset status fields
      ResetAllElementStatusFiels();

      // Start generation Task
      generationThreadDel = new ThreadStart(GenerationTask);
      generationThread = new Thread(generationThreadDel);
      generationThread.SetApartmentState(ApartmentState.STA);
      generationThread.Start();
    }
    /**
     * @brief     Generation Task
     * @details   Start generation of the selected modules due to their generation priority
     */

    private void GenerationTask()
    {
      List<VG_ModuleCfgVM> activeModulsList = new List<VG_ModuleCfgVM>();

      // Process the different Priority Levels
      foreach (List<VG_ModuleCfgVM> prioModuleList in moduleGenerationList)
      {
        activeModulsList = new List<VG_ModuleCfgVM>();
        bool allTasksFinished;

        // Check each module
        foreach (VG_ModuleCfgVM module in prioModuleList)
        {
          if (true == module.ValidateAndGenerateModule)
          {
            activeModulsList.Add(module);

            // Start generation
            try
            {
              module.StartModuleGeneration();
            }
            catch(System.IO.InvalidDataException)
            {
              //Nothing to do here >> Already handled and reported by lower layer
            }
            catch (Exception ex)
            {
              Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
              {
                string error = General.HyperspaceExceptions.E0004_UnknownThirdPartyModuleFailure +
                " Module Name: \"" + module.ModuleName +
                "\" Configured Lib File: \"" + module.GetAbsoluteModuleLibraryPath() +
                "\" Error message: " + ex;

                // Report error
                HyperspaceSystem.Instance().ConfigSpaceSystemLogInstance().AddNewSystemErrorLog(
                  HyperspaceExceptions.E0004, error);
              }));
            }
          }
        }

        // Check if generation has finished
        allTasksFinished = true;
        do
        {
          // Sleep before evaluating the task status from the modules
          Thread.Sleep(200);

          foreach (VG_ModuleCfgVM module in activeModulsList)
          {
            // Check module task status
            if (true == module.IsTaskActive)
            {
              allTasksFinished = true;
              break;
            }
          }
        } while (false == allTasksFinished);
      }
    }
    #endregion

    #region Attributes
    /**
     * Getter for the ModuleViewList
     */
    public List<VG_ModuleCfgV> ModuleViewList
    { get { return moduleViewList; } }

    /**
     * Getter for the ModuleViewModelList
     */
    public List<VG_ModuleCfgVM> ModuleViewModelList
    { get { return moduleViewModelList; } }

    #endregion

    #region Helper functions 
    /**
     * @brief     Create the generation list with priorities
     */
    private void CreateGenerationList()
    {
      UInt16 prioIdx = 0;
      bool priorityFound = false;
      while (0 < tmpViewModelList.Count)
      {
        for (int idx = 0; idx < tmpViewModelList.Count(); idx++)
        {
          if (prioIdx == tmpViewModelList[idx].ModuleGenerationPriority)
          {
            priorityFound = false;
            // Search for priority in generation list
            foreach (List<VG_ModuleCfgVM> moduleList in moduleGenerationList)
            {
              // Check its priority
              if (moduleList[0].ModuleGenerationPriority == tmpViewModelList[idx].ModuleGenerationPriority)
              {
                moduleList.Add(tmpViewModelList[idx]);
                tmpViewModelList.RemoveAt(idx);
                idx--;
                priorityFound = true;
                break;
              }
            }

            // Check if priority has been found
            if (false == priorityFound)
            {
              moduleGenerationList.Add(new List<VG_ModuleCfgVM>());
              moduleGenerationList.Last().Add(tmpViewModelList[idx]);
              tmpViewModelList.RemoveAt(idx);
              idx--;
            }
          }
        }
        prioIdx++;
      }
    }

    /**
     * @brief     Helper function to reset all element status fields
     */
    private void ResetAllElementStatusFiels()
    {
      foreach(VG_ModuleCfgVM module in moduleViewModelList)
      {
        module.SetValidationStatusToInit();
        module.SetGenerationStatusToInit();
      }
    }
    #endregion
  }
}
/**
* @}
*/
