﻿/*****************************************************************************************************************************
 * @file        ValidateAndGenerateModules.xaml.cs                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.09.2019                                                                                                   *
 * @brief       Implementation of the View for the Module Validation and Generation Window                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.Models;
using OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.ViewModels;
using OsarHyperspace.General;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate
{
  /// <summary>
  /// Interaction logic for ValidateAndGenerateModules.xaml
  /// </summary>
  public partial class ValidateAndGenerateModules : Window
  {
    private VG_MainVM validateGenerateMainVM = new VG_MainVM();

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    public ValidateAndGenerateModules()
    {
      InitializeComponent();

      // Generate generation validation window content
      InitalizeViewModel_ModelCfgList();
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    /// <summary>
    /// Initialize the content of the module validation and generation window with multiple available modules
    /// </summary>
    private void InitalizeViewModel_ModelCfgList()
    {
      // Setup necessary content controls
      foreach(var module in validateGenerateMainVM.ModuleViewList)
      {
        ContentControl moduleContent = new ContentControl();
        moduleContent.Content = module;

        SP_GenerateValidateModules.Children.Add(moduleContent);

      }
    }

    #region UI Actions
    /// <summary>
    /// Perform closing window actions
    /// >> Store project configuration file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CloseWindow(object sender, System.ComponentModel.CancelEventArgs e)
    {
      // Update Project Configuration
      HyperspaceProjectConfig.Instance().StoreHyperspaceProjectConfigFile();

      // Inform System about project change
      HyperspaceSystem.Instance().EventProjectConfigurationChanged();
    }

    /// <summary>
    /// Interface to select all modules to be generated
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SelectAllModules_Click(object sender, RoutedEventArgs e)
    {
      foreach(var module in validateGenerateMainVM.ModuleViewModelList)
      {
        module.ValidateAndGenerateModule = true;
      }
    }

    /// <summary>
    /// Interface to unselect all modules to be generated
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_UnSelectAllModules_Click(object sender, RoutedEventArgs e)
    {
      foreach (var module in validateGenerateMainVM.ModuleViewModelList)
      {
        module.ValidateAndGenerateModule = false;
      }
    }

    /// <summary>
    /// Button to start validation of selected modules
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_ValidateModules_Click(object sender, RoutedEventArgs e)
    {
      validateGenerateMainVM.ValidateModules();
    }

    /// <summary>
    /// Button to start generation of selected modules
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_GenerateModules_Click(object sender, RoutedEventArgs e)
    {
      validateGenerateMainVM.GenerateModules();
    }

    #endregion
  }
}

/**
* @}
*/
