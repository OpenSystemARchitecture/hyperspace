﻿/*****************************************************************************************************************************
 * @file        VG_ModuleCfgStatusUiDataM.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.09.2019                                                                                                   *
 * @brief       Implementation of the Data Model for the Module Validation and Generation Window status signalization        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.Models
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace.ConfigurationSpace.MenuBar.Project.ValidateGenerate.Models
{
  public class VG_ModuleCfgStatusUiDataM
  {
    private string validationStatus;
    private Brush validationStatusColor;

    private string generationStatus;
    private Brush generationStatusColor;



    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                         PUBLIC                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
    public VG_ModuleCfgStatusUiDataM()
    {
      SetValidationStatusToInit();
      SetGenerationStatusToInit();
    }

    /// <summary>
    /// Getter and Setter for the Validation Status String
    /// </summary>
    public string ValidationStatus { get => validationStatus; set => validationStatus = value; }

    /// <summary>
    /// Getter and Setter for the Validation Status Color
    /// </summary>
    public Brush ValidationStatusColor { get => validationStatusColor; set => validationStatusColor = value; }

    /// <summary>
    /// Getter and Setter for the Generation Status String
    /// </summary>
    public string GenerationStatus { get => generationStatus; set => generationStatus = value; }

    /// <summary>
    /// Getter and Setter for the Generation Status Color
    /// </summary>
    public Brush GenerationStatusColor { get => generationStatusColor; set => generationStatusColor = value; }

    /// <summary>
    /// Setter interface for the validation status fields in case of init
    /// </summary>
    public void SetValidationStatusToInit()
    {
      validationStatus = ValidationGenerationResources.StatusInitString;
      ValidationStatusColor = new SolidColorBrush(Colors.Gray);
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of init
    /// </summary>
    public void SetGenerationStatusToInit()
    {
      generationStatus = ValidationGenerationResources.StatusInitString;
      generationStatusColor = new SolidColorBrush(Colors.Gray);
    }

    /// <summary>
    /// Setter interface for the validation status fields in case of in progress
    /// </summary>
    public void SetValidationStatusToInProgress()
    {
      validationStatus = ValidationGenerationResources.StatusInProgressString;
      ValidationStatusColor = new SolidColorBrush(Colors.Orange);
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of in progress
    /// </summary>
    public void SetGenerationStatusToInProgress()
    {
      generationStatus = ValidationGenerationResources.StatusInProgressString;
      generationStatusColor = new SolidColorBrush(Colors.Orange);
    }

    /// <summary>
    /// Setter interface for the validation status fields in case of in failed
    /// </summary>
    public void SetValidationStatusToFailed()
    {
      validationStatus = ValidationGenerationResources.StatusFailedString;
      ValidationStatusColor = new SolidColorBrush(Colors.Red);
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of in failed
    /// </summary>
    public void SetGenerationStatusToFailed()
    {
      generationStatus = ValidationGenerationResources.StatusFailedString;
      generationStatusColor = new SolidColorBrush(Colors.Red);
    }

    /// <summary>
    /// Setter interface for the validation status fields in case of in successful
    /// </summary>
    public void SetValidationStatusToSuccessful()
    {
      validationStatus = ValidationGenerationResources.StatusSuccessfulString;
      ValidationStatusColor = new SolidColorBrush(Colors.Green);
    }

    /// <summary>
    /// Setter interface for the generation status fields in case of in successful
    /// </summary>
    public void SetGenerationStatusToSuccessful()
    {
      generationStatus = ValidationGenerationResources.StatusSuccessfulString;
      generationStatusColor = new SolidColorBrush(Colors.Green);
    }

    /************************************************************************************************************************/
    /* DO NOT CHANGE THIS COMMENT >>                        PRIVATE                           << DO NOT CHANGE THIS COMMENT */
    /************************************************************************************************************************/
  }
}
/**
* @}
*/
