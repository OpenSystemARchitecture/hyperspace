﻿/*****************************************************************************************************************************
 * @file        App.xaml.cs                                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of functionalities from the main module.                                                      *
 *                                                                                                                           *
 * @details     Implementation of general starting point >> Setup of necessary elements.                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarHyperspace
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarHyperspace
{
  /**
    * @brief      Active Program entry class
    */
  public partial class App : Application
  {
    private ConfigurationSpace.Startup.Engine.ConfigSpaceEntry configurationSpaceEntry;
    private DesignSpace.Startup.Engine.DesignSpaceEntry designSpaceEntry;

    /**
     * @brief Application entry point
     */
    private void ApplicationStartup(object sender, StartupEventArgs e)
    {
      this.ShutdownMode = ShutdownMode.OnExplicitShutdown;
      
      Mainfunction();
    }

    /**
      * @brief      Main function
      * @param      String Array with program arguments
      * @retval     None
      * @note       Static program entry point
      */
    private void Mainfunction()
    {
      try
      {
        General.Views.SystemStartupWindow startupWindow = new General.Views.SystemStartupWindow();
        startupWindow.ShowDialog();

        if (true == startupWindow.SpaceSelectorData.StartDesignSpace)
        {
          /* Start Design Space GUI */
          designSpaceEntry = new DesignSpace.Startup.Engine.DesignSpaceEntry();
          designSpaceEntry.StartupDesignSpace();
        }
        else if (true == startupWindow.SpaceSelectorData.StartConfigurationSpace)
        {
          /* Start Configuration Space GUI */
          configurationSpaceEntry = new ConfigurationSpace.Startup.Engine.ConfigSpaceEntry();
          configurationSpaceEntry.StartupConfigurationSpace();
        }
        else
        {
          /* No Space selected >> Shutdown application */
        }
      }
      catch(Exception ex)
      {
        /* No reference given by user >> Exit program execution */
        Application.Current.Shutdown();
      }

      //TODO: Remove Temporary Application Shutdown function
      Application.Current.Shutdown();
    }
  }
}
/**
* @}
*/
