﻿/*****************************************************************************************************************************
 * @file        RteAdvancedDataTypeV.xaml.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        23.12.2020                                                                                                   *
 * @brief       View of the RteAdvancedDataTypes                                                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteTypes.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RteLib.RteTypes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteTypes.Views
{
  /// <summary>
  /// Interaction logic for RteAdvancedDataTypeV.xaml
  /// </summary>
  public partial class RteAdvancedDataTypeV : UserControl
  {
    ViewModels.RteAdvancedDataTypeVM advancedDataTypeVM;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteAdvancedDataType"></param>
    /// <param name="rteTypes"></param>
    public RteAdvancedDataTypeV(RteAdvancedDataTypeStructure rteAdvancedDataType, RteLib.RteTypes.RteTypes rteTypes)
    {
      advancedDataTypeVM = new ViewModels.RteAdvancedDataTypeVM(rteAdvancedDataType, rteTypes);

      InitializeComponent();

      this.DataContext = advancedDataTypeVM;
    }

    #region Properties
    #endregion

    #region UI Actions
    /// <summary>
    /// Event Handler for RadioButtonStandard_Clicked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RadioButtonStandard_Clicked(object sender, RoutedEventArgs e)
    {
      advancedDataTypeVM.SwitchDataTypeContent(RteAdvancedDataTypeType.STANDARD);
    }
    /// <summary>
    /// Event Handler for RadioButtonEnum_Clicked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RadioButtonEnum_Clicked(object sender, RoutedEventArgs e)
    {
      advancedDataTypeVM.SwitchDataTypeContent(RteAdvancedDataTypeType.ENUMERATION);
    }

    /// <summary>
    /// Event Handler for RadioButtonStruct_Clicked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RadioButtonStruct_Clicked(object sender, RoutedEventArgs e)
    {
      advancedDataTypeVM.SwitchDataTypeContent(RteAdvancedDataTypeType.STRUCT);
    }
    #endregion
  }
}
/**
* @}
*/