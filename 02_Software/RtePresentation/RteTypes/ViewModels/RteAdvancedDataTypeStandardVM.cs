﻿/*****************************************************************************************************************************
 * @file        RteAdvancedDataTypeStandardVM.cs                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        23.12.2020                                                                                                   *
 * @brief       View Model of the RteAdvancedDataTypes > Standard Types Part                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteTypes.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteTypes;
using RtePresentation.General.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteTypes.ViewModels
{
  internal class RteAdvancedDataTypeStandardVM : BaseViewModel
  {
    private RteAdvancedDataTypeStructure advancedDataType;      //!< Actual processed element
    private RteLib.RteTypes.RteTypes rteTypesDb;                //!< Types Data base

    private List<object> baseDataTypes;                         //!< List with all data types which are available

    public RteAdvancedDataTypeStandardVM(RteAdvancedDataTypeStructure rteAdvancedDataType, RteLib.RteTypes.RteTypes rteTypes)
    {
      advancedDataType = rteAdvancedDataType;
      rteTypesDb = rteTypes;

      baseDataTypes = new List<object>();

      foreach(RteBaseDataTypeStructure dataType in rteTypesDb.BaseDataTypeList)
      {
        baseDataTypes.Add(dataType);
      }
      foreach (RteAdvancedDataTypeStructure dataType in rteTypesDb.AdvancedDataTypeList)
      {
        baseDataTypes.Add(dataType);
      }
    }

    #region Properties
    /// <summary>
    /// Getter and Setter interface for the advancedDataType member
    /// </summary>
    public RteAdvancedDataTypeStructure AdvancedDataType
    {
      get { return advancedDataType; }
      set 
      {
        advancedDataType = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AdvancedDataType"));
      }
    }

    /// <summary>
    /// Custom interface for the data type name
    /// </summary>
    public String DataTypeName
    {
      get { return AdvancedDataType.DataType.DataTypeName; }
      set { advancedDataType.DataType.DataTypeName = value; }
    }

    /// <summary>
    /// Getter for a list of all available data types
    /// </summary>
    public List<Object> BaseDataTypeList
    {
      get { return baseDataTypes; }
    }

    /// <summary>
    /// Getter for the used base data type
    /// </summary>
    public Object BaseDataType
    {
      get
      {
        Object retVal = null;
        bool elementFound = false;
        foreach (RteAdvancedDataTypeStructure dataType in rteTypesDb.AdvancedDataTypeList)
        {
          if(dataType.DataType == advancedDataType.BaseDataType)
          {
            retVal = dataType;
            elementFound = true;
          }
        }

        if(false == elementFound)
        {
          foreach(RteBaseDataTypeStructure dataType in rteTypesDb.BaseDataTypeList)
          {
            if (dataType == advancedDataType.BaseDataType)
            {
              retVal = dataType;
            }
          }
        }

        return retVal;
      }

      set
      {
        if(typeof(RteLib.RteTypes.RteBaseDataTypeStructure) == value.GetType())
        {
          advancedDataType.BaseDataType = (RteLib.RteTypes.RteBaseDataTypeStructure)value;
        }
        else
        {
          advancedDataType.BaseDataType = ((RteLib.RteTypes.RteAdvancedDataTypeStructure)value).DataType;
        }
        
      }
    }
    #endregion

  }
}
/**
* @}
*/