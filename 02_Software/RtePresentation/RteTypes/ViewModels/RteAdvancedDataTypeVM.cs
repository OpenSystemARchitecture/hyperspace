﻿/*****************************************************************************************************************************
 * @file        RteAdvancedDataTypeVM.cs                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        23.12.2020                                                                                                   *
 * @brief       View Model of the RteAdvancedDataTypes                                                                       *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteTypes.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteTypes;
using RtePresentation.General.ViewModels;
using RtePresentation.RteTypes.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteTypes.ViewModels
{
  internal class RteAdvancedDataTypeVM : BaseViewModel
  {
    private RteAdvancedDataTypeStructure advancedDataType;      //!< Actual processed element
    private RteLib.RteTypes.RteTypes rteTypesDb;                //!< Types Data base

    RteAdvancedDataTypeStandardV standardDataTypeV;             //!< View of the standard data type
    RteAdvancedDataTypeEnumV enumDataTypeV;                     //!< View of the enum data type
    RteAdvancedDataTypeStructV structDataTypeV;                 //!< View of the struct data type

    private object dataTypeContent;                             //!< View object element for sub data type views
    private string notificationText;
    private string notificationImageSource;
    private bool enableEdditing;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteAdvancedDataType">Actual data type to be processed</param>
    /// <param name="rteTypes">Complete types database</param>
    public RteAdvancedDataTypeVM(RteAdvancedDataTypeStructure rteAdvancedDataType, RteLib.RteTypes.RteTypes rteTypes)
    {
      advancedDataType = rteAdvancedDataType;
      rteTypesDb = rteTypes;
      dataTypeContent = new ContentControl();

      //standardDataTypeV = new RteAdvancedDataTypeStandardV(ref advancedDataType, ref rteTypesDb);
      standardDataTypeV = new RteAdvancedDataTypeStandardV(advancedDataType, rteTypesDb);
      enumDataTypeV = new RteAdvancedDataTypeEnumV(advancedDataType, rteTypesDb);
      structDataTypeV = new RteAdvancedDataTypeStructV(advancedDataType, rteTypesDb);

      // Set initial Content Control
      SwitchDataTypeContent(advancedDataType.DataTypeType);

      // Set Notification Information
      if((null != advancedDataType.DataType.UUID) && ("" != advancedDataType.DataType.UUID))
      {
        // UUID Set >> Existing Data type identified
        notificationText = General.Models.RtePresentationResources.Base_NotificationText_ExistingDataType;
        notificationImageSource = General.Models.RtePresentationResources.Base_NotificationText_ExistingElement_ImageSource;
        EnableEdditing = false;
      }
      else
      {
        notificationText = General.Models.RtePresentationResources.Base_NotificationText_NewDataType;
        notificationImageSource = General.Models.RtePresentationResources.Base_NotificationText_NewElement_ImageSource;
        EnableEdditing = true;

        // New data type >> Create a UUID
        advancedDataType.DataType.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      }
    }

    #region Properties
    /// <summary>
    /// Getter and Setter interface for the advancedDataType member
    /// </summary>
    public RteAdvancedDataTypeStructure AdvancedDataType
    {
      get 
      {
        return advancedDataType;
      }
      set
      {
        advancedDataType = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AdvancedDataType"));
      }
    }

    /// <summary>
    /// Getter for the sub data type view
    /// </summary>
    public object SubDataTypeView
    {
      get { return dataTypeContent; }
      set { dataTypeContent = value; }
    }

    /// <summary>
    /// Getter for the Notification image source
    /// </summary>
    public string NotficationImage
    {
      get { return notificationImageSource; }
    }

    /// <summary>
    /// Getter for the Notification Text
    /// </summary>
    public string NotificationText
    {
      get { return notificationText; }
    }

    /// <summary>
    /// Getter and Setter for the enableEdditing flag
    /// </summary>
    public bool EnableEdditing
    {
      get { return enableEdditing; }
      set
      {
        enableEdditing = value;
        standardDataTypeV.IsEnabled = enableEdditing;
        enumDataTypeV.IsEnabled = enableEdditing;
        structDataTypeV.IsEnabled = enableEdditing;
      }
    }

    #endregion

    #region Actions
    /// <summary>
    /// Interface to select the sub view
    /// </summary>
    /// <param name="switchToDataType"></param>
    public void SwitchDataTypeContent(RteLib.RteTypes.RteAdvancedDataTypeType switchToDataType)
    {
      advancedDataType.DataTypeType = switchToDataType;

      switch (switchToDataType)
      {
        case RteAdvancedDataTypeType.STANDARD:
        dataTypeContent = standardDataTypeV;
        break;

        case RteAdvancedDataTypeType.STRUCT:
        dataTypeContent = structDataTypeV;
        break;

        case RteAdvancedDataTypeType.ENUMERATION:
        dataTypeContent = enumDataTypeV;
        break;
      }

      OnPropertyChanged(new PropertyChangedEventArgs("AdvancedDataType.DataTypeType"));
      OnPropertyChanged(new PropertyChangedEventArgs("SubDataTypeView"));
    }
    #endregion
  }
}
/**
* @}
*/