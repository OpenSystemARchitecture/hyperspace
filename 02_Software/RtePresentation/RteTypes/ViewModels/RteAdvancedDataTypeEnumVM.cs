﻿/*****************************************************************************************************************************
 * @file        RteAdvancedDataTypeEnumVM.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        23.12.2020                                                                                                   *
 * @brief       View Model of the RteAdvancedDataTypes > Enum Types Part                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteTypes.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteTypes;
using RtePresentation.General.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteTypes.ViewModels
{
  internal class RteAdvancedDataTypeEnumVM : BaseViewModel
  {
    private RteAdvancedDataTypeStructure advancedDataType;      //!< Actual processed element
    private RteLib.RteTypes.RteTypes rteTypesDb;                //!< Types Data base

    private string newEnumElementName;
    private string newEnumElementId;
    private RteAdvancedEnumDataTypeStructure newEnumElement;
    private RteAdvancedEnumDataTypeStructure selectedEnumElement;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteAdvancedDataType"></param>
    /// <param name="rteTypes"></param>
    public RteAdvancedDataTypeEnumVM(RteAdvancedDataTypeStructure rteAdvancedDataType, RteLib.RteTypes.RteTypes rteTypes)
    {
      advancedDataType = rteAdvancedDataType;
      rteTypesDb = rteTypes;

      newEnumElement = new RteAdvancedEnumDataTypeStructure();
      newEnumElement.EnumId = "";
      newEnumElement.EnumElementName = "";

      newEnumElementName = "";
      newEnumElementId = "";

      selectedEnumElement = new RteAdvancedEnumDataTypeStructure();
    }

    #region Properties
    /// <summary>
    /// Getter and Setter interface for the advancedDataType member
    /// </summary>
    public RteAdvancedDataTypeStructure AdvancedDataType
    {
      get { return advancedDataType; }
      set
      {
        advancedDataType = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AdvancedDataType"));
      }
    }

    /// <summary>
    /// Custom interface for the data type name
    /// </summary>
    public String DataTypeName
    {
      get { return AdvancedDataType.DataType.DataTypeName; }
      set { advancedDataType.DataType.DataTypeName = value; }
    }

    /// <summary>
    /// Getter and Setter for newEnumElementName
    /// </summary>
    public string NewEnumElementName
    {
      get { return newEnumElementName; }
      set { newEnumElementName = value; }
    }

    /// <summary>
    /// Getter and Setter for newEnumElementId
    /// </summary>
    public string NewEnumElementId
    {
      get { return newEnumElementId; }
      set { newEnumElementId = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedEnumElement
    /// </summary>
    public RteAdvancedEnumDataTypeStructure SelectedEnumElement
    {
      get { return selectedEnumElement; }
      set { selectedEnumElement = value; }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to add a new enum element
    /// </summary>
    public void AddNewEnumValue()
    {
      newEnumElement = new RteAdvancedEnumDataTypeStructure();
      newEnumElement.EnumElementName = NewEnumElementName;
      newEnumElement.EnumId = NewEnumElementId;

      AdvancedDataType.EnumElements.Add(newEnumElement);

      NewEnumElementName = "";
      NewEnumElementId = "";

      OnPropertyChanged(new PropertyChangedEventArgs("NewEnumElementName"));
      OnPropertyChanged(new PropertyChangedEventArgs("NewEnumElementId"));
      //OnPropertyChanged(new PropertyChangedEventArgs("AdvancedDataType.EnumElements"));
    }

    /// <summary>
    /// Interface to remove the selected enum element
    /// </summary>
    public void DeleteSelectedEnum()
    {
      AdvancedDataType.EnumElements.Remove(selectedEnumElement);
    }
    #endregion
  }
}
/**
* @}
*/