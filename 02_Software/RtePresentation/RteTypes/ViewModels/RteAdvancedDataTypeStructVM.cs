﻿/*****************************************************************************************************************************
 * @file        RteAdvancedDataTypeStructVM.cs                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.12.2020                                                                                                   *
 * @brief       View Model of the RteAdvancedDataTypes > Struct Types Part                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteTypes.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteTypes;
using RtePresentation.General.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteTypes.ViewModels
{
  internal class RteAdvancedDataTypeStructVM : BaseViewModel
  {
    private RteAdvancedDataTypeStructure advancedDataType;      //!< Actual processed element
    private RteLib.RteTypes.RteTypes rteTypesDb;                //!< Types Data base

    
    private RteAdvancedStructDataTypeStructure selectedStructElement;
    private string newStructElementName;
    private RteAdvancedStructDataTypeStructure newStructElements;
    private RteDataAccessType selectedAccessModifier;
    private object selectedDataType;

    private List<object> baseDataTypes;                         //!< List with all data types which are available

    /// <summary>
    /// Constructor
    /// </summary>
    public RteAdvancedDataTypeStructVM(RteAdvancedDataTypeStructure rteAdvancedDataType, RteLib.RteTypes.RteTypes rteTypes)
    {
      advancedDataType = rteAdvancedDataType;
      rteTypesDb = rteTypes;

      newStructElementName = "";
      newStructElements = new RteAdvancedStructDataTypeStructure();
      selectedAccessModifier = RteDataAccessType.STANDARD;

      // Setup base data type list
      baseDataTypes = new List<object>();
      foreach (RteBaseDataTypeStructure dataType in rteTypesDb.BaseDataTypeList)
      {
        baseDataTypes.Add(dataType);
      }
      foreach (RteAdvancedDataTypeStructure dataType in rteTypesDb.AdvancedDataTypeList)
      {
        baseDataTypes.Add(dataType);
      }
    }

    #region Properties
    /// <summary>
    /// Getter and Setter interface for the advancedDataType member
    /// </summary>
    public RteAdvancedDataTypeStructure AdvancedDataType
    {
      get { return advancedDataType; }
      set
      {
        advancedDataType = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AdvancedDataType"));
      }
    }

    /// <summary>
    /// Custom interface for the data type name
    /// </summary>
    public String DataTypeName
    {
      get { return AdvancedDataType.DataType.DataTypeName; }
      set { advancedDataType.DataType.DataTypeName = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedStructElement
    /// </summary>
    public RteAdvancedStructDataTypeStructure SelectedStructElement
    {
      get { return selectedStructElement; }
      set { selectedStructElement = value; }
    }

    /// <summary>
    /// Getter and Setter for newEnumElementName
    /// </summary>
    public string NewStructElementName
    {
      get { return newStructElementName; }
      set { newStructElementName = value; }
    }

    /// <summary>
    /// Getter for a list of all available data types
    /// </summary>
    public List<Object> BaseDataTypeList
    {
      get { return baseDataTypes; }
    }

    /// <summary>
    /// Getter for a the selectedAccessModifier
    /// </summary>
    public RteDataAccessType SelectedAccessModifier
    {
      get { return selectedAccessModifier; }
      set { selectedAccessModifier = value; }
    }

    /// <summary>
    /// Getter for a the selectedDataType
    /// </summary>
    public object SelectedDataType
    {
      get { return selectedDataType; }
      set { selectedDataType = value; }
    }

    #endregion

    #region Actions
    /// <summary>
    /// Interface to add a new element
    /// </summary>
    public void AddNewElement()
    {
      newStructElements = new RteAdvancedStructDataTypeStructure();

      if (typeof(RteLib.RteTypes.RteBaseDataTypeStructure) == selectedDataType.GetType())
      {
        // Use copy constructor of RteBaseDataTypeStructure
        newStructElements.BaseDataType = new RteBaseDataTypeStructure(
          (RteLib.RteTypes.RteBaseDataTypeStructure)selectedDataType);
      }
      else
      {
        // Use copy constructor of RteBaseDataTypeStructure
        newStructElements.BaseDataType = new RteBaseDataTypeStructure(
          ( (RteLib.RteTypes.RteAdvancedDataTypeStructure)selectedDataType).DataType);
      }

      newStructElements.StructElementName = NewStructElementName;
      newStructElements.BaseDataType.DataAccessType = selectedAccessModifier;

      selectedAccessModifier = RteDataAccessType.STANDARD;
      NewStructElementName = "";
      AdvancedDataType.StructElements.Add(newStructElements);

      selectedAccessModifier = RteDataAccessType.STANDARD;
      NewStructElementName = "";

      OnPropertyChanged(new PropertyChangedEventArgs("SelectedAccessModifier"));
      OnPropertyChanged(new PropertyChangedEventArgs("NewStructElementName"));
      OnPropertyChanged(new PropertyChangedEventArgs("AdvancedDataType.structElements"));
    }

    /// <summary>
    /// Interface to remove the selected struct element
    /// </summary>
    public void DeleteSelectedElement()
    {
      AdvancedDataType.StructElements.Remove(selectedStructElement);
    }
    #endregion
  }
}
/**
* @}
*/