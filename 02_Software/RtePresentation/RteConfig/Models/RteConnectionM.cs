﻿/*****************************************************************************************************************************
 * @file        RteConnectionM.cs                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       Model of the RteConfig >> Representing an Connection                                                         *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteConfig.Models
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteConfig.Models
{
  internal class RteConnectionM
  {
    private string moduleName;
    private RteGeneralInterfaceInfo portInfo;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteConnectionM()
    {
      moduleName = "";
      portInfo = new RteGeneralInterfaceInfo();
    }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteConnectionM(RteConnectionM obj)
    {
      this.ModuleName = obj.ModuleName;
      this.PortInfo = new RteGeneralInterfaceInfo(obj.PortInfo);
    }

    #region Attributes
    public string ModuleName
    {
      get { return moduleName; }
      set { moduleName = value; }
    }

    public RteGeneralInterfaceInfo PortInfo
    {
      get { return portInfo; }
      set { portInfo = value; }
    }
    #endregion

  }


  internal class RteConnection_CSM
  {
    private RteConnectionM serverModule;
    private RteCSInterfaceImplementationType serverType;

    private RteGeneralInterfaceInfo ifBlueprint;

    private RteConnectionM clientModule;
    private RteCSInterfaceImplementationType clientType;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteConnection_CSM()
    {
      serverModule = new RteConnectionM();
      serverType = RteCSInterfaceImplementationType.SERVER;

      ifBlueprint = new RteGeneralInterfaceInfo();

      clientModule = new RteConnectionM();
      clientType = RteCSInterfaceImplementationType.CLIENT;
    }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteConnection_CSM(RteConnection_CSM obj)
    {
      this.ServerModule = new RteConnectionM(obj.ServerModule);
      this.serverType = obj.ServerType;
      this.IfBlueprint = new RteGeneralInterfaceInfo(obj.IfBlueprint);

      this.clientModule = new RteConnectionM(obj.clientModule);
      this.clientType = obj.ClientType;
    }

    #region Attributes
    public RteConnectionM ServerModule
    {
      get { return serverModule; }
      set { serverModule = value; }
    }
    public RteCSInterfaceImplementationType ServerType
    {
      get { return serverType; }
    }

    public RteGeneralInterfaceInfo IfBlueprint
    {
      get { return ifBlueprint; }
      set { ifBlueprint = value; }
    }

    public RteConnectionM ClientModule
    {
      get { return clientModule; }
      set { clientModule = value; }
    }
    public RteCSInterfaceImplementationType ClientType
    {
      get { return clientType; }
    }
    #endregion

  }

  internal class RteConnection_SRM
  {
    private RteConnectionM senderModule;
    private RteSRInterfaceImplementationType senderType;

    private RteGeneralInterfaceInfo ifBlueprint;

    private RteConnectionM receiverModule;
    private RteSRInterfaceImplementationType receiverType;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteConnection_SRM()
    {
      senderModule = new RteConnectionM();
      senderType = RteSRInterfaceImplementationType.SENDER;

      ifBlueprint = new RteGeneralInterfaceInfo();

      receiverModule = new RteConnectionM();
      receiverType = RteSRInterfaceImplementationType.RECEIVER;
    }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteConnection_SRM(RteConnection_SRM obj)
    {
      this.SenderModule = new RteConnectionM(obj.SenderModule);
      this.senderType = obj.SenderType;
      this.IfBlueprint = new RteGeneralInterfaceInfo(obj.IfBlueprint);

      this.ReceiverModule = new RteConnectionM(obj.ReceiverModule);
      this.receiverType = obj.ReceiverType;
    }

    #region Attributes
    public RteConnectionM SenderModule
    {
      get { return senderModule; }
      set { senderModule = value; }
    }
    public RteSRInterfaceImplementationType SenderType
    {
      get { return senderType; }
    }

    public RteGeneralInterfaceInfo IfBlueprint
    {
      get { return ifBlueprint; }
      set { ifBlueprint = value; }
    }

    public RteConnectionM ReceiverModule
    {
      get { return receiverModule; }
      set { receiverModule = value; }
    }
    public RteSRInterfaceImplementationType ReceiverType
    {
      get { return receiverType; }
    }
    #endregion

  }
}
/**
* @}
*/