﻿/*****************************************************************************************************************************
 * @file        RteConnection_OverviewVM.cs                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       View Model of the RteConfig >> Connection Overview Part                                                      *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteConfig.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteConfig;
using RteLib.RteInterface;
using RtePresentation.RteConfig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteConfig.ViewModels
{
  internal class RteConnection_OverviewVM : RteConnection_BaseVM
  {

    private RteConnection_CSM selectedCSConnection;
    private RteConnection_SRM selectedSRConnection;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteConnection_OverviewVM() : base()
    {

    }

    #region Attributes
    public RteConnection_CSM SelectedCSConnection
    {
      get { return selectedCSConnection; }
      set { selectedCSConnection = value; }
    }
    public RteConnection_SRM SelectedSRConnection
    {
      get { return selectedSRConnection; }
      set { selectedSRConnection = value; }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to delete the selected client server connection from the view and the data base
    /// </summary>
    /// <param name=""></param>
    public void DeleteSelectedCSConnection()
    {
      if (null == SelectedCSConnection)
      {
        MessageBox.Show("Please select a port connection", "Alert", MessageBoxButton.OK, MessageBoxImage.Exclamation);
      }
      else
      {
        // Delete Connection
        if (true == rteConfigDb.DeleteClientPortFromCSConnection(selectedCSConnection.ServerModule.PortInfo,
          selectedCSConnection.ClientModule.PortInfo))
        {
          UpdateAllCSConnectionList();
        }
        else
        {
          MessageBox.Show("Selected connection not found in data base. Data base mismatch detected!", "Alert", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
      }
    }

    /// <summary>
    /// Interface to delete the selected sender receiver connection from the view and the data base
    /// </summary>
    /// <param name=""></param>
    public void DeleteSelectedSRConnection()
    {
      if (null == SelectedSRConnection)
      {
        MessageBox.Show("Please select a port connection", "Alert", MessageBoxButton.OK, MessageBoxImage.Exclamation);
      }
      else
      {
        // Delete Connection
        if (true == rteConfigDb.DeleteReceiverPortFromSRConnection(selectedSRConnection.SenderModule.PortInfo,
          selectedSRConnection.ReceiverModule.PortInfo))
        {
          UpdateAllSRConnectionList();
        }
        else
        {
          MessageBox.Show("Selected connection not found in data base. Data base mismatch detected!", "Alert", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
      }
    }
    #endregion
  }
}
/**
* @}
*/