﻿/*****************************************************************************************************************************
 * @file        RteConnectionVM.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       View Model of the RteConfig >> Connection Main Part                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteConfig.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteConfig.ViewModels
{
  internal class RteConnectionVM : RteConnection_BaseVM
  {
    private Views.RteConnection_OverviewV overviewV;
    private Views.RteConnection_ConnectCSV connectCSV;
    private Views.RteConnection_ConnectSRV connectSRV;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="miBList"></param>
    /// <param name="rteTypes"></param>
    /// <param name="rteInterfaces"></param>
    /// <param name="rteConfig"></param>
    public RteConnectionVM(List<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> miBList,
      RteLib.RteTypes.RteTypes rteTypes, RteLib.RteInterface.RteInterfaces rteInterfaces,
      RteLib.RteConfig.RteConfig rteConfig) : base(miBList, rteTypes, rteInterfaces, rteConfig)
    {
      overviewV = new Views.RteConnection_OverviewV();
      connectCSV = new Views.RteConnection_ConnectCSV();
      connectSRV = new Views.RteConnection_ConnectSRV();
    }

    #region Attributes
    public Views.RteConnection_OverviewV OverviewV
    {
      get { return overviewV; }
      set { overviewV = value; }
    }

    public Views.RteConnection_ConnectCSV ConnectCSV
    {
      get { return connectCSV; }
      set { connectCSV = value; }
    }

    public Views.RteConnection_ConnectSRV ConnectSRV
    {
      get { return connectSRV; }
      set { connectSRV = value; }
    }
    #endregion

  }
}
/**
* @}
*/