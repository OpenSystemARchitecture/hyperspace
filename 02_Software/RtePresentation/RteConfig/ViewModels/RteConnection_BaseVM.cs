﻿/*****************************************************************************************************************************
 * @file        RteConnection_BaseVM.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       View Model of the RteConfig >> Connection Base Part                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteConfig.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteConfig;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using RtePresentation.General.ViewModels;
using RtePresentation.RteConfig.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteConfig.ViewModels
{
  internal class RteConnection_BaseVM : BaseViewModel
  {
    static protected List<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> rteMiBList; //!< MiB Data base
    static protected RteLib.RteTypes.RteTypes rteTypesDb;                                         //!< Types Data base
    static protected RteLib.RteInterface.RteInterfaces rteInterfaceDb;                            //!< Interface Data base
    static protected RteLib.RteConfig.RteConfig rteConfigDb;                                      //!< Config Data base

    static protected ObservableCollection<RteConnection_CSM> allCSConnections;
    static protected ObservableCollection<RteConnection_SRM> allSRConnections;


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="miBList">Actual MiB Object</param>
    /// <param name="rteTypes">Rte Types Database</param>
    /// <param name="rteInterfaces">Rte Interface Database</param>
    /// <param name="rteConfig">Rte Config Database</param>
    protected RteConnection_BaseVM(List<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> miBList,
      RteLib.RteTypes.RteTypes rteTypes, RteLib.RteInterface.RteInterfaces rteInterfaces,
      RteLib.RteConfig.RteConfig rteConfig)
    {
      rteMiBList = miBList;
      rteTypesDb = rteTypes;
      rteInterfaceDb = rteInterfaces;
      rteConfigDb = rteConfig;

      allCSConnections = new ObservableCollection<RteConnection_CSM>();
      allSRConnections = new ObservableCollection<RteConnection_SRM>();

      UpdateAllCSConnectionList();
      UpdateAllSRConnectionList();
    }

    /// <summary>
    /// Constructor
    /// </summary>
    protected RteConnection_BaseVM()
    {
    }

    #region Attributes
    public ObservableCollection<RteConnection_CSM> AllCSConnections
    {
      get { return allCSConnections; }
      set { allCSConnections = value; }
    }
    public ObservableCollection<RteConnection_SRM> AllSRConnections
    {
      get { return allSRConnections; }
      set { allSRConnections = value; }
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function which updates the CS Connection List
    /// </summary>
    protected void UpdateAllCSConnectionList()
    {
      allCSConnections.Clear();

      RteConnection_CSM newServerEntry = new RteConnection_CSM();
      RteConnection_CSM newClientServerEntry = new RteConnection_CSM();
      RteCSInterfacePrototype interfacePrototype;
      string moduleName;
      List<RteGeneralInterfaceInfo> clientPortsToBeRemoved;
      List<RteClientServerPortConnection> serverPortsToBeRemoved = new List<RteClientServerPortConnection>();

      foreach (RteClientServerPortConnection csConnection in rteConfigDb.RteClientServerConnections)
      {
        clientPortsToBeRemoved = new List<RteGeneralInterfaceInfo>();

        // Setup Server Port Information
        if (true == Hlp_SearchForServerPort(csConnection.RteServerPort, out interfacePrototype, out moduleName))
        {
          newServerEntry.IfBlueprint = new RteGeneralInterfaceInfo(interfacePrototype.InterfaceBlueprint);
          newServerEntry.ServerModule.PortInfo = new RteGeneralInterfaceInfo(interfacePrototype.InterfacePrototype);
          newServerEntry.ServerModule.ModuleName = moduleName;

        }
        else
        {
          // Server port not found >> Connection outdated >> Remove connection
          serverPortsToBeRemoved.Add(new RteClientServerPortConnection(csConnection));
          break;
        }

        // Setup Client port Information
        foreach (RteGeneralInterfaceInfo clientPort in csConnection.RteClientPorts)
        {
          newClientServerEntry = new RteConnection_CSM(newServerEntry);

          // Setup Client Port Information
          if (true == Hlp_SearchForClientPort(clientPort, out interfacePrototype, out moduleName))
          {
            // Check if Blueprints are equal
            if(newServerEntry.IfBlueprint == interfacePrototype.InterfaceBlueprint)
            {
              newClientServerEntry.ClientModule.PortInfo = new RteGeneralInterfaceInfo(interfacePrototype.InterfacePrototype);
              newClientServerEntry.ClientModule.ModuleName = moduleName;

              // Add connection
              allCSConnections.Add(newClientServerEntry);
            }
            else
            {
              // Blueprints are not equal >> Connection outdated >> Remove connection
              clientPortsToBeRemoved.Add(new RteGeneralInterfaceInfo(clientPort));
              break;
            }
            
          }
          else
          {
            // Client port not found >> Connection outdated >> Remove connection
            clientPortsToBeRemoved.Add(new RteGeneralInterfaceInfo(clientPort));
            break;
          }
        }

        // Remove deprecated client connections
        foreach (RteGeneralInterfaceInfo connection in clientPortsToBeRemoved)
        {
          csConnection.RteClientPorts.Remove(connection);
        }
      }

      // Remove deprecated server connections
      foreach (RteClientServerPortConnection connection in serverPortsToBeRemoved)
      {
        rteConfigDb.RteClientServerConnections.Remove(connection);
      }
    }

    /// <summary>
    /// Helper function which updates the SR Connection List
    /// </summary>
    protected void UpdateAllSRConnectionList()
    {
      allSRConnections.Clear();



      RteConnection_SRM newSenderEntry = new RteConnection_SRM();
      RteConnection_SRM newSenderReceiverEntry = new RteConnection_SRM();
      RteSRInterfacePrototype interfacePrototype;
      string moduleName;
      List<RteGeneralInterfaceInfo> receiverPortsToBeRemoved;
      List<RteSenderReceiverPortConnection> senderPortsToBeRemoved = new List<RteSenderReceiverPortConnection>();

      foreach (RteSenderReceiverPortConnection srConnection in rteConfigDb.RteSenderReceiverConnections)
      {
        receiverPortsToBeRemoved = new List<RteGeneralInterfaceInfo>();

        // Setup Sender Port Information
        if (true == Hlp_SearchForSenderPort(srConnection.RteSenderPort, out interfacePrototype, out moduleName))
        {
          newSenderEntry.IfBlueprint = new RteGeneralInterfaceInfo(interfacePrototype.InterfaceBlueprint);
          newSenderEntry.SenderModule.PortInfo = new RteGeneralInterfaceInfo(interfacePrototype.InterfacePrototype);
          newSenderEntry.SenderModule.ModuleName = moduleName;
        }
        else
        {
          // Server port not found >> Connection outdated >> Remove connection
          senderPortsToBeRemoved.Add(new RteSenderReceiverPortConnection(srConnection));
          break;
        }

        // Setup receiver port Information
        foreach (RteGeneralInterfaceInfo receiverPort in srConnection.RteReceiverPorts)
        {
          newSenderReceiverEntry = new RteConnection_SRM(newSenderEntry);

          // Setup receiver Port Information
          if (true == Hlp_SearchForReceiverPort(receiverPort, out interfacePrototype, out moduleName))
          {
            // Check if Blueprints are equal
            if (newSenderEntry.IfBlueprint == interfacePrototype.InterfaceBlueprint)
            {
              newSenderReceiverEntry.ReceiverModule.PortInfo = new RteGeneralInterfaceInfo(interfacePrototype.InterfacePrototype);
              newSenderReceiverEntry.ReceiverModule.ModuleName = moduleName;

              // Add connection
              allSRConnections.Add(newSenderReceiverEntry);
            }
            else
            {
              // Blueprints are not equal >> Connection outdated >> Remove connection
              receiverPortsToBeRemoved.Add(new RteGeneralInterfaceInfo(receiverPort));
              break;
            }

          }
          else
          {
            // Client port not found >> Connection outdated >> Remove connection
            receiverPortsToBeRemoved.Add(new RteGeneralInterfaceInfo(receiverPort));
            break;
          }
        }

        // Remove deprecated client connections
        foreach (RteGeneralInterfaceInfo connection in receiverPortsToBeRemoved)
        {
          srConnection.RteReceiverPorts.Remove(connection);
        }
      }

      // Remove deprecated server connections
      foreach (RteSenderReceiverPortConnection connection in senderPortsToBeRemoved)
      {
        rteConfigDb.RteSenderReceiverConnections.Remove(connection);
      }
    }

    /// <summary>
    /// Helper function searching for the server port prototype
    /// </summary>
    /// <param name="checkIf"></param>
    /// <param name="serverPortPrototype"></param>
    /// <param name="moudleName"></param>
    /// <returns></returns>
    private bool Hlp_SearchForServerPort(RteGeneralInterfaceInfo checkIf,
      out RteCSInterfacePrototype serverPortPrototype,
      out string moudleName)
    {
      bool elementFound = false;
      int index = 0;
      moudleName = "";
      serverPortPrototype = new RteCSInterfacePrototype();

      foreach (RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior mib in rteMiBList)
      {
        if(true == mib.ContainServerPortPrototype(checkIf, out index))
        {
          elementFound = true;
          serverPortPrototype = new RteCSInterfacePrototype(mib.AvailableServerPorts[index].ServerPort);
          moudleName = mib.ModuleName;
          break;
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Helper function searching for the client port prototype
    /// </summary>
    /// <param name="checkIf"></param>
    /// <param name="clientPortPrototype"></param>
    /// <param name="moudleName"></param>
    /// <returns></returns>
    private bool Hlp_SearchForClientPort(RteGeneralInterfaceInfo checkIf,
      out RteCSInterfacePrototype clientPortPrototype,
      out string moudleName)
    {
      bool elementFound = false;
      int index = 0;
      moudleName = "";
      clientPortPrototype = new RteCSInterfacePrototype();

      foreach (RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior mib in rteMiBList)
      {
        if (true == mib.ContainClientPortPrototype(checkIf, out index))
        {
          elementFound = true;
          clientPortPrototype = new RteCSInterfacePrototype(mib.AvailableClientPorts[index].ClientPort);
          moudleName = mib.ModuleName;
          break;
        }
      }

      return elementFound;
    }


    /// <summary>
    /// Helper function searching for the sender port prototype
    /// </summary>
    /// <param name="checkIf"></param>
    /// <param name="portPrototype"></param>
    /// <param name="moudleName"></param>
    /// <returns></returns>
    private bool Hlp_SearchForSenderPort(RteGeneralInterfaceInfo checkIf,
      out RteSRInterfacePrototype portPrototype,
      out string moudleName)
    {
      bool elementFound = false;
      int index = 0;
      moudleName = "";
      portPrototype = new RteSRInterfacePrototype();

      foreach (RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior mib in rteMiBList)
      {
        if (true == mib.ContainSenderPortPrototype(checkIf, out index))
        {
          elementFound = true;
          portPrototype = new RteSRInterfacePrototype(mib.AvailableSRPorts[index].SenderReceiverPort);
          moudleName = mib.ModuleName;
          break;
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Helper function searching for the receiver port prototype
    /// </summary>
    /// <param name="checkIf"></param>
    /// <param name="portPrototype"></param>
    /// <param name="moudleName"></param>
    /// <returns></returns>
    private bool Hlp_SearchForReceiverPort(RteGeneralInterfaceInfo checkIf,
      out RteSRInterfacePrototype portPrototype,
      out string moudleName)
    {
      bool elementFound = false;
      int index = 0;
      moudleName = "";
      portPrototype = new RteSRInterfacePrototype();

      foreach (RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior mib in rteMiBList)
      {
        if (true == mib.ContainReceiverPortPrototype(checkIf, out index))
        {
          elementFound = true;
          portPrototype = new RteSRInterfacePrototype(mib.AvailableSRPorts[index].SenderReceiverPort);
          moudleName = mib.ModuleName;
          break;
        }
      }

      return elementFound;
    }
    #endregion
  }
}
/**
* @}
*/