﻿/*****************************************************************************************************************************
 * @file        RteConnection_ConnectSRVM.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        09.01.2021                                                                                                   *
 * @brief       View Model of the RteConfig >> Connection Sender-Receiver Part                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteConfig.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteConfig;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteConfig.ViewModels
{
  internal class RteConnection_ConnectSRVM : RteConnection_BaseVM
  {
    private ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> allModulesWithSenderPorts;
    private ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> allModulesWithReceiverPorts;

    private RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior selectedSenderModule;
    private RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior selectedReceiverModule;

    private RteAvailableSRPort selectedSenderPort;
    private RteAvailableSRPort selectedReceiverPort;

    private ObservableCollection<RteAvailableSRPort> availableSenderPorts;
    private ObservableCollection<RteAvailableSRPort> availableReceiverPorts;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteConnection_ConnectSRVM() : base()
    {
      allModulesWithSenderPorts = new ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior>();
      allModulesWithReceiverPorts = new ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior>();
      selectedSenderModule = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      selectedReceiverModule = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      selectedSenderPort = new RteAvailableSRPort();
      selectedReceiverPort = new RteAvailableSRPort();
      availableSenderPorts = new ObservableCollection<RteAvailableSRPort>();
      availableReceiverPorts = new ObservableCollection<RteAvailableSRPort>();

      // Setup ports list
      foreach (RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior mib in rteMiBList)
      {
        if (0 < mib.AvailableSRPorts.Count)
        {
          // Search for sender ports
          foreach(RteAvailableSRPort port in mib.AvailableSRPorts)
          {
            if(RteLib.RteInterface.RteSRInterfaceImplementationType.SENDER == port.SenderReceiverPort.InterfaceType)
            {
              allModulesWithSenderPorts.Add(mib);
              break;
            }
          }
          // Search for receiver ports
          foreach (RteAvailableSRPort port in mib.AvailableSRPorts)
          {
            if (RteLib.RteInterface.RteSRInterfaceImplementationType.RECEIVER == port.SenderReceiverPort.InterfaceType)
            {
              allModulesWithReceiverPorts.Add(mib);
              break;
            }
          }
        }
      }

    }

    #region Attributes
    public ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> AllModulesWithSenderPorts
    {
      get { return allModulesWithSenderPorts; }
      set { allModulesWithSenderPorts = value; }
    }

    public ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> AllModulesWithReceiverPorts
    {
      get { return allModulesWithReceiverPorts; }
      set { allModulesWithReceiverPorts = value; }
    }


    public RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior SelectedSenderModule
    {
      get { return selectedSenderModule; }
      set
      {
        selectedSenderModule = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedSenderModule"));

        Hlp_SetupSenderPortsList();

        if (null == selectedSenderModule)
        {
          SelectedSenderPort = null;
        }
        else
        {
          SelectedSenderPort = availableSenderPorts[0];
        }
      }
    }

    public RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior SelectedReceiverModule
    {
      get { return selectedReceiverModule; }
      set
      {
        selectedReceiverModule = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedReceiverModule"));

        Hlp_SetupReceiverPortsList();

        if (null == selectedReceiverModule)
        {
          SelectedReceiverPort = null;
        }
        else
        {
          SelectedReceiverPort = availableReceiverPorts[0];
        }
      }
    }

    public RteAvailableSRPort SelectedSenderPort
    {
      get { return selectedSenderPort; }
      set
      {
        selectedSenderPort = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedSenderPort"));
      }
    }

    public RteAvailableSRPort SelectedReceiverPort
    {
      get { return selectedReceiverPort; }
      set
      {
        selectedReceiverPort = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedReceiverPort"));
      }
    }

    public ObservableCollection<RteAvailableSRPort> AvailableSenderPorts
    {
      get { return availableSenderPorts; }
      set
      {
        availableSenderPorts = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AvailableSenderPorts"));
      }
    }
    public ObservableCollection<RteAvailableSRPort> AvailableReceiverPorts
    {
      get { return availableReceiverPorts; }
      set
      {
        availableReceiverPorts = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AvailableReceiverPorts"));
      }
    }


    #endregion

    #region Actions
    /// <summary>
    /// Interface to connect the selected ports
    /// </summary>
    public void ConnectPorts()
    {
      bool portFound = false;
      bool receiverPortFound = false;
      RteSenderReceiverPortConnection senderReceiverPortConnection = new RteSenderReceiverPortConnection();

      // Check if port blueprints are equal
      if (selectedSenderPort.SenderReceiverPort.InterfaceBlueprint == selectedReceiverPort.SenderReceiverPort.InterfaceBlueprint)
      {
        // Check if sender port already exist in connection
        foreach (RteSenderReceiverPortConnection srPortConnection in rteConfigDb.RteSenderReceiverConnections)
        {
          if (srPortConnection.RteSenderPort == selectedSenderPort.SenderReceiverPort.InterfacePrototype)
          {
            portFound = true;
            senderReceiverPortConnection = srPortConnection;
            break;
          }
        }


        //Select action for port
        if (true == portFound)
        {
          //Update Port connection
          //Search for receiver port
          foreach (RteGeneralInterfaceInfo receiverPort in senderReceiverPortConnection.RteReceiverPorts)
          {
            if (receiverPort == selectedReceiverPort.SenderReceiverPort.InterfacePrototype)
            {
              receiverPortFound = true;
              break;
            }
          }

          // Check if client port is available
          if (true == receiverPortFound)
          {
            MessageBox.Show("Ports already connected", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
          }
          else
          {
            senderReceiverPortConnection.RteReceiverPorts.Add(selectedReceiverPort.SenderReceiverPort.InterfacePrototype);

            // Cleanup
            SelectedSenderModule = null;
            SelectedReceiverModule = null;

            // Update view
            UpdateAllSRConnectionList();

            MessageBox.Show("Ports connected", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
          }
        }
        else
        {
          //Create new port connection
          senderReceiverPortConnection = new RteSenderReceiverPortConnection();
          senderReceiverPortConnection.RteSenderPort = new RteLib.RteInterface.RteGeneralInterfaceInfo(selectedSenderPort.SenderReceiverPort.InterfacePrototype);
          senderReceiverPortConnection.RteReceiverPorts.Add(SelectedReceiverPort.SenderReceiverPort.InterfacePrototype);

          // Add new port connection
          rteConfigDb.RteSenderReceiverConnections.Add(senderReceiverPortConnection);

          // Cleanup
          SelectedSenderModule = null;
          SelectedReceiverModule = null;

          // Update view
          UpdateAllSRConnectionList();

          MessageBox.Show("Ports connected", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }
      }
      else
      {
        MessageBox.Show("Selected ports have not the same interface blueprint. Connection not possible!", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
      }
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to update the available sender ports
    /// </summary>
    private void Hlp_SetupSenderPortsList()
    {
      AvailableSenderPorts.Clear();

      if (null != SelectedSenderModule)
      {
        foreach (RteAvailableSRPort port in SelectedSenderModule.AvailableSRPorts)
        {
          if (RteLib.RteInterface.RteSRInterfaceImplementationType.SENDER == port.SenderReceiverPort.InterfaceType)
          {
            AvailableSenderPorts.Add(port);
          }
        }
      }

      OnPropertyChanged(new PropertyChangedEventArgs("AvailableSenderPorts"));
    }

    /// <summary>
    /// Helper function to update the available receiver ports
    /// </summary>
    private void Hlp_SetupReceiverPortsList()
    {
      AvailableReceiverPorts.Clear();

      if (null != SelectedReceiverModule)
      {
        foreach (RteAvailableSRPort port in SelectedReceiverModule.AvailableSRPorts)
        {
          if (RteLib.RteInterface.RteSRInterfaceImplementationType.RECEIVER == port.SenderReceiverPort.InterfaceType)
          {
            AvailableReceiverPorts.Add(port);
          }
        }
      }

      OnPropertyChanged(new PropertyChangedEventArgs("AvailableReceiverPorts"));
    }
    #endregion
  }
}
/**
* @}
*/