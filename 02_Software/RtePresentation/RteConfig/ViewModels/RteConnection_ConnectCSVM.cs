﻿/*****************************************************************************************************************************
 * @file        RteConnection_ConnectCSVM.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       View Model of the RteConfig >> Connection Client-Server Part                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteConfig.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteConfig;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteConfig.ViewModels
{
  internal class RteConnection_ConnectCSVM : RteConnection_BaseVM
  {
    private ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> allModulesWithServerPorts;
    private ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> allModulesWithClientPorts;

    private RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior selectedServerModule;
    private RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior selectedClientModule;

    private RteAvailableServerPort selectedServerPort;
    private RteAvailableClientPort selectedClientPort;


    /// <summary>
    /// Constructor
    /// </summary>
    public RteConnection_ConnectCSVM() : base()
    {
      allModulesWithServerPorts = new ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior>();
      allModulesWithClientPorts = new ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior>();
      selectedServerModule = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      selectedClientModule = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      selectedServerPort = new RteAvailableServerPort();
      selectedClientPort = new RteAvailableClientPort();

      // Setup ports list
      foreach (RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior mib in rteMiBList)
      {
        if(0 < mib.AvailableServerPorts.Count)
        {
          allModulesWithServerPorts.Add(mib);
        }
        if (0 < mib.AvailableClientPorts.Count)
        {
          allModulesWithClientPorts.Add(mib);
        }
      }
    }

    #region Attributes
    public ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> AllModulesWithServerPorts
    {
      get { return allModulesWithServerPorts; }
      set { allModulesWithServerPorts = value; }
    }

    public ObservableCollection<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> AllModulesWithClientPorts
    {
      get { return allModulesWithClientPorts; }
      set { allModulesWithClientPorts = value; }
    }

    public RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior SelectedServerModule
    {
      get { return selectedServerModule; }
      set 
      { 
        selectedServerModule = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedServerModule"));

        if(null == selectedServerModule)
        {
          SelectedServerPort = null;
        }
        else
        {
          SelectedServerPort = selectedServerModule.AvailableServerPorts[0];
        }
      }
    }

    public RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior SelectedClientModule
    {
      get { return selectedClientModule; }
      set
      {
        selectedClientModule = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedClientModule"));

        if (null == selectedClientModule)
        {
          SelectedClientPort = null;
        }
        else
        {
          SelectedClientPort = selectedClientModule.AvailableClientPorts[0];
        }
      }
    }

    public RteAvailableServerPort SelectedServerPort
    {
      get { return selectedServerPort; }
      set
      {
        selectedServerPort = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedServerPort"));
      }
    }

    public RteAvailableClientPort SelectedClientPort
    {
      get { return selectedClientPort; }
      set
      {
        selectedClientPort = value;
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedClientPort"));
      }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to connect the selected ports
    /// </summary>
    public void ConnectPorts()
    {
      bool portFound = false;
      bool clientPortFound = false;
      RteClientServerPortConnection clientServerPortConnection = new RteClientServerPortConnection();

      // Check if port blueprints are equal
      if(selectedServerPort.ServerPort.InterfaceBlueprint == selectedClientPort.ClientPort.InterfaceBlueprint)
      {
        // Check if server port already exist in connection
        foreach (RteClientServerPortConnection csPortConnection in rteConfigDb.RteClientServerConnections)
        {
          if(csPortConnection.RteServerPort == selectedServerPort.ServerPort.InterfacePrototype)
          {
            portFound = true;
            clientServerPortConnection = csPortConnection;
            break;
          }
        }
      

        //Select action for port
        if(true == portFound)
        {
          //Update Port connection
          //Search for client port
          foreach(RteGeneralInterfaceInfo clientPort in clientServerPortConnection.RteClientPorts)
          {
            if(clientPort == selectedClientPort.ClientPort.InterfacePrototype)
            {
              clientPortFound = true;
              break;
            }
          }

          // Check if client port is available
          if(true == clientPortFound)
          {
            MessageBox.Show("Ports already connected", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
          }
          else
          {
            clientServerPortConnection.RteClientPorts.Add(selectedClientPort.ClientPort.InterfacePrototype);

            // Cleanup
            SelectedServerModule = null;
            SelectedClientModule = null;

            // Update view
            UpdateAllCSConnectionList();

            MessageBox.Show("Ports connected", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
          }
        }
        else
        {
          //Create new port connection
          clientServerPortConnection = new RteClientServerPortConnection();
          clientServerPortConnection.RteServerPort = new RteLib.RteInterface.RteGeneralInterfaceInfo(selectedServerPort.ServerPort.InterfacePrototype);
          clientServerPortConnection.RteClientPorts.Add(selectedClientPort.ClientPort.InterfacePrototype);

          // Add new port connection
          rteConfigDb.RteClientServerConnections.Add(clientServerPortConnection);

          // Cleanup
          SelectedServerModule = null;
          SelectedClientModule = null;

          // Update view
          UpdateAllCSConnectionList();

          MessageBox.Show("Ports connected", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }
      }
      else
      {
        MessageBox.Show("Selected ports have not the same interface blueprint. Connection not possible!", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
      }
    }
    #endregion

    #region Helper functions
    #endregion
  }
}
/**
* @}
*/