﻿/*****************************************************************************************************************************
 * @file        RteConfigV.xaml.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        02.01.2021                                                                                                   *
 * @brief       View of the RteConfig >> Connection Main Part                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteConfig.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteConfig.Views
{
  /// <summary>
  /// Interaction logic for RteConfigV.xaml
  /// </summary>
  public partial class RteConnectionV : UserControl
  {
    ViewModels.RteConnectionVM usedVM;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteMiBList"></param>
    /// <param name="rteTypes"></param>
    /// <param name="rteInterfaces"></param>
    /// <param name="rteConfig"></param>
    public RteConnectionV(List<RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior> rteMiBList,
      RteLib.RteTypes.RteTypes rteTypes, RteLib.RteInterface.RteInterfaces rteInterfaces,
      RteLib.RteConfig.RteConfig rteConfig)
    {
      usedVM = new ViewModels.RteConnectionVM(rteMiBList, rteTypes, rteInterfaces, rteConfig);

      InitializeComponent();

      this.DataContext = usedVM;
    }
  }
}
/**
* @}
*/