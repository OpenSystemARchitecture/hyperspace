﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehaviorVM.cs                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        29.12.2020                                                                                                   *
 * @brief       View Model of the RteModuleInternalBehavior >> Main Part                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RtePresentation.RteModuleInternalBehavior.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteModuleInternalBehavior.ViewModels
{
  internal class RteModuleInternalBehaviorVM : RteModuleInternalBehavior_BaseVM
  {
    private RteModuleInternalBehavior_OverviewV overviewView;
    private RteModuleInternalBehavior_OsRunnablesV osView;
    private RteModuleInternalBehavior_ServerPortsV serverPortView;
    private RteModuleInternalBehavior_ClientPortsV clientPortView;
    private RteModuleInternalBehavior_SRPortsV senderReceiverPortView;


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteMiB">Actual MiB Object</param>
    /// <param name="rteTypes">Rte Types Database</param>
    /// <param name="rteInterfaces">Rte Interface Database</param>
    /// <param name="rteConfig">Rte Config Database</param>
    public RteModuleInternalBehaviorVM(RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior rteMiB,
      RteLib.RteTypes.RteTypes rteTypes, RteLib.RteInterface.RteInterfaces rteInterfaces,
      RteLib.RteConfig.RteConfig rteConfig) : base(rteMiB, rteTypes, rteInterfaces, rteConfig)
    {
      overviewView = new RteModuleInternalBehavior_OverviewV();
      osView = new RteModuleInternalBehavior_OsRunnablesV();
      serverPortView = new RteModuleInternalBehavior_ServerPortsV();
      clientPortView = new RteModuleInternalBehavior_ClientPortsV();
      senderReceiverPortView = new RteModuleInternalBehavior_SRPortsV();

      // Check if it is a new module
      if ("" == rteMiB.UUID)
      {
        EnableEdditing = true;
        rteMiB.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      }
      else
      {
        EnableEdditing = false;
      }
    }

    #region Attributes
    /// <summary>
    /// Getter and Setter for the overviewView
    /// </summary>
    public RteModuleInternalBehavior_OverviewV OverviewView
    {
      get { return overviewView; }
      set { overviewView = value; }
    }

    /// <summary>
    /// Getter and Setter for the osView
    /// </summary>
    public RteModuleInternalBehavior_OsRunnablesV OsView
    {
      get { return osView; }
      set { osView = value; }
    }

    public RteModuleInternalBehavior_ServerPortsV ServerPortView
    {
      get { return serverPortView; }
      set { serverPortView = value; }
    }

    public RteModuleInternalBehavior_ClientPortsV ClientPortView
    {
      get { return clientPortView; }
      set { clientPortView = value; }
    }

    public RteModuleInternalBehavior_SRPortsV SenderReceiverPortView
    {
      get { return senderReceiverPortView; }
      set { senderReceiverPortView = value; }
    }
    #endregion

    #region UI Actions
    #endregion
  }
}
/**
* @}
*/