﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior_SRPortsVM.cs                                                                       *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        30.12.2020                                                                                                   *
 * @brief       View Model of the RteModuleInternalBehavior >> Sender Receiver Ports Part                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

namespace RtePresentation.RteModuleInternalBehavior.ViewModels
{
  internal class RteModuleInternalBehavior_SRPortsVM : RteModuleInternalBehavior_BaseVM
  {
    private RteAvailableSRPort activeAvailablePort;
    private RteSRInterfaceBlueprint selectedPortBlueprint;
    private RteAvailableSRPort selectedPortPrototype;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteModuleInternalBehavior_SRPortsVM() : base()
    {
      Hlp_CreateANewPortPrototype();
    }

    #region Attributes
    /// <summary>
    /// Getter and Setter for activeAvailablePort
    /// </summary>
    public RteAvailableSRPort ActiveAvailablePort
    {
      get { return activeAvailablePort; }
      set { activeAvailablePort = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedPortBlueprint
    /// </summary>
    public RteSRInterfaceBlueprint SelectedPortBlueprint
    {
      get { return selectedPortBlueprint; }
      set { selectedPortBlueprint = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedPortPrototype
    /// </summary>
    public RteAvailableSRPort SelectedPortPrototype
    {
      get { return selectedPortPrototype; }
      set { selectedPortPrototype = value; }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to add the port prototype
    /// </summary>
    public void AddActivePortPrototype()
    {
      try
      {
        // Add Blueprint interface information
        ActiveAvailablePort.SenderReceiverPort.InterfaceBlueprint = new RteGeneralInterfaceInfo(SelectedPortBlueprint.InterfaceInfo);

        // Add Blueprint runnable information
        foreach (RteElementBlueprint blueprintElement in SelectedPortBlueprint.Elements)
        {
          RteGenericRunnableObject newRunnableInfo = new RteGenericRunnableObject();
          newRunnableInfo.Name = blueprintElement.ElementName;
          newRunnableInfo.UUID = blueprintElement.UUID;
          newRunnableInfo.BasePrototype = new RteGeneralInterfaceInfo(ActiveAvailablePort.SenderReceiverPort.InterfacePrototype);

          ActiveAvailablePort.SenderReceiverPortElements.Add(newRunnableInfo);
        }

        ActualMiB.AvailableSRPorts.Add(ActiveAvailablePort);
        Hlp_CreateANewPortPrototype();
        UpdateAvailableSenderReceiverPortElementsUICollection();

        // Update Backup Blueprint List
        AddNewBackupSenderReceiverPortBlueprint(SelectedPortBlueprint);
      }
      catch (NullReferenceException)
      {
        MessageBox.Show("Please select a interface blueprint", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
      }
    }

    /// <summary>
    /// Interface to delete the port prototype
    /// </summary>
    public void DeleteSelectedPortPrototype()
    {
      try
      {
        ActualMiB.AvailableSRPorts.Remove(selectedPortPrototype);
        UpdateAvailableSenderReceiverPortElementsUICollection();
      }
      catch (NullReferenceException)
      {
        MessageBox.Show("Please select a interface blueprint", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
      }
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to create a new Port Prototype
    /// </summary>
    private void Hlp_CreateANewPortPrototype()
    {
      ActiveAvailablePort = new RteAvailableSRPort();
      ActiveAvailablePort.SenderReceiverPort.InterfacePrototype.UUID =
        OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      ActiveAvailablePort.SenderReceiverPort.InterfaceType = RteSRInterfaceImplementationType.RECEIVER;

      OnPropertyChanged(new PropertyChangedEventArgs("ActiveAvailablePort"));
    }

    /// <summary>
    /// Helper function to update the client port functions
    /// </summary>
    //private void Hlp_UpdateAvailableSRPortFunctionList()
    //{
    //  ObservableCollection<RteGenericRunnableObject> newUiList = new ObservableCollection<RteGenericRunnableObject>();

    //  foreach (RteAvailableSRPort portProttype in ActualMiB.AvailableSRPorts)
    //  {
    //    foreach (RteGenericRunnableObject portRunnable in portProttype.SenderReceiverPortElements)
    //    {
    //      newUiList.Add(portRunnable);
    //    }
    //  }

    //  AllAvailableSenderReceiverPortElements = newUiList;
    //}

    /// <summary>
    /// During startup the prototype will be synchronized with the blueprint
    /// </summary>
    //private void Hlp_SyncSRPrototypewithBlueprint()
    //{
    //  bool blueprintElementFound = false;
    //  bool blueprintFound = false;

    //  // Each Port Prototype
    //  foreach (RteAvailableSRPort port in ActualMiB.AvailableSRPorts)
    //  {
    //    RteSRInterfaceBlueprint blueprintForSync = new RteSRInterfaceBlueprint();
    //    //Search now for blueprint
    //    foreach (RteSRInterfaceBlueprint blueprint in ActualMiB.BackupSRBlueprintList)
    //    {
    //      if (blueprint.InterfaceInfo.UUID == port.SenderReceiverPort.InterfaceBlueprint.UUID)
    //      {
    //        blueprintForSync = blueprint;
    //        blueprintFound = true;
    //      }
    //    }


    //    if (true == blueprintFound)
    //    {
    //      // +++++ Remove all elements which are not longer a part of the blueprint +++++
    //      for (int idx = 0; idx < port.SenderReceiverPortElements.Count; idx++)
    //      {
    //        blueprintElementFound = false;

    //        foreach (RteElementBlueprint blueprintElement in blueprintForSync.Elements)
    //        {
    //          if (port.SenderReceiverPortElements[idx].UUID == blueprintElement.UUID)
    //          {
    //            blueprintElementFound = true;
    //            break;
    //          }
    //        }

    //        // Check if Element has been found
    //        if (false == blueprintElementFound)
    //        {
    //          // Element not found >> Remove it
    //          port.SenderReceiverPortElements.RemoveAt(idx);
    //          idx--;
    //        }
    //      }

    //      // +++++ Add all elements which are new in the blueprint +++++
    //      foreach (RteElementBlueprint blueprintElement in blueprintForSync.Elements)
    //      {
    //        blueprintElementFound = false;

    //        foreach (RteGenericRunnableObject runnable in port.SenderReceiverPortElements)
    //        {
    //          if (runnable.UUID == blueprintElement.UUID)
    //          {
    //            blueprintElementFound = true;
    //            break;
    //          }
    //        }

    //        // Check if Element has been found
    //        if (false == blueprintElementFound)
    //        {
    //          // Element not found >> Add it

    //          RteGenericRunnableObject newRunnable = new RteGenericRunnableObject();
    //          newRunnable.Name = blueprintElement.ElementName;
    //          newRunnable.UUID = blueprintElement.UUID;
    //          port.SenderReceiverPortElements.Add(newRunnable);
    //        }
    //      }
    //    }
    //  }
    //}

    /// <summary>
    /// Update the Backup lists
    /// </summary>
    //private void Hlp_AddNewBackupSenderReceiverPortBlueprint(RteSRInterfaceBlueprint backupInterface)
    //{
    //  if (false == ActualMiB.BackupSRBlueprintList.Contains(backupInterface))
    //  {
    //    ActualMiB.BackupSRBlueprintList.Add(backupInterface);
    //  }
    //}
    #endregion

  }
}
/**
* @}
*/