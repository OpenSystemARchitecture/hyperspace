﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior_ClientPortsVM.cs                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        30.12.2020                                                                                                   *
 * @brief       View Model of the RteModuleInternalBehavior >> Client Ports Part                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

namespace RtePresentation.RteModuleInternalBehavior.ViewModels
{
  internal class RteModuleInternalBehavior_ClientPortsVM : RteModuleInternalBehavior_BaseVM
  {
    private RteAvailableClientPort activeAvailablePort;
    private RteCSInterfaceBlueprint selectedPortBlueprint;
    private RteAvailableClientPort selectedPortPrototype;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteModuleInternalBehavior_ClientPortsVM() : base()
    {
      Hlp_CreateANewPortPrototype();
    }

    #region Attributes
    /// <summary>
    /// Getter and Setter for activeAvailablePort
    /// </summary>
    public RteAvailableClientPort ActiveAvailablePort
    {
      get { return activeAvailablePort; }
      set { activeAvailablePort = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedPortBlueprint
    /// </summary>
    public RteCSInterfaceBlueprint SelectedPortBlueprint
    {
      get { return selectedPortBlueprint; }
      set { selectedPortBlueprint = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedPortPrototype
    /// </summary>
    public RteAvailableClientPort SelectedPortPrototype
    {
      get { return selectedPortPrototype; }
      set { selectedPortPrototype = value; }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to add the port prototype
    /// </summary>
    public void AddActivePortPrototype()
    {
      try
      {
        // Add Blueprint interface information
        ActiveAvailablePort.ClientPort.InterfaceBlueprint = new RteGeneralInterfaceInfo(SelectedPortBlueprint.InterfaceInfo);

        // Add Blueprint runnable information
        foreach (RteFunctionBlueprint blueprintFunction in SelectedPortBlueprint.Functions)
        {
          RteGenericRunnableObject newRunnableInfo = new RteGenericRunnableObject();
          newRunnableInfo.Name = blueprintFunction.FunctionName;
          newRunnableInfo.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
          newRunnableInfo.BasePrototype = new RteGeneralInterfaceInfo(ActiveAvailablePort.ClientPort.InterfacePrototype);

          ActiveAvailablePort.ClientPortFunctions.Add(newRunnableInfo);
        }

        ActualMiB.AvailableClientPorts.Add(ActiveAvailablePort);
        Hlp_CreateANewPortPrototype();
        UpdateAvailableClientPortFunctionsUICollection();

        // Update Backup Blueprint List
        AddNewBackupClientServerPortBlueprint(SelectedPortBlueprint);
      }
      catch (NullReferenceException)
      {
        MessageBox.Show("Please select a interface blueprint", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
      }
    }

    /// <summary>
    /// Interface to delete the port prototype
    /// </summary>
    public void DeleteSelectedPortPrototype()
    {
      try
      {
        ActualMiB.AvailableClientPorts.Remove(selectedPortPrototype);
        UpdateAvailableClientPortFunctionsUICollection();
      }
      catch (NullReferenceException)
      {
        MessageBox.Show("Please select a interface blueprint", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
      }
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to create a new Port Prototype
    /// </summary>
    private void Hlp_CreateANewPortPrototype()
    {
      ActiveAvailablePort = new RteAvailableClientPort();
      ActiveAvailablePort.ClientPort.InterfacePrototype.UUID =
        OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      ActiveAvailablePort.ClientPort.InterfaceType = RteCSInterfaceImplementationType.CLIENT;

      OnPropertyChanged(new PropertyChangedEventArgs("ActiveAvailablePort"));
    }

    /// <summary>
    /// Helper function to update the client port functions
    /// </summary>
    //private void Hlp_UpdateAvailableClientPortFunctionList()
    //{
    //  ObservableCollection<RteGenericRunnableObject> newUiList = new ObservableCollection<RteGenericRunnableObject>();

    //  foreach(RteAvailableClientPort portProttype in ActualMiB.AvailableClientPorts)
    //  {
    //    foreach(RteGenericRunnableObject portRunnable in portProttype.ClientPortFunctions)
    //    {
    //      newUiList.Add(portRunnable);
    //    }
    //  }

    //  AllAvailableClientPortFunctions = newUiList;
    //}

    /// <summary>
    /// During startup the prototype will be synchronized with the blueprint
    /// </summary>
    //private void Hlp_SyncClientPrototypewithBlueprint()
    //{
    //  bool blueprintElementFound = false;
    //  bool blueprintFound = false;

    //  // Each Port Prototype
    //  foreach (RteAvailableClientPort port in ActualMiB.AvailableClientPorts)
    //  {
    //    RteCSInterfaceBlueprint blueprintForSync = new RteCSInterfaceBlueprint();
    //    //Search now for blueprint
    //    foreach (RteCSInterfaceBlueprint blueprint in ActualMiB.BackupCSBlueprintList)
    //    {
    //      if (blueprint.InterfaceInfo.UUID == port.ClientPort.InterfaceBlueprint.UUID)
    //      {
    //        blueprintForSync = blueprint;
    //        blueprintFound = true;
    //      }
    //    }


    //    if (true == blueprintFound)
    //    {
    //      // +++++ Remove all elements which are not longer a part of the blueprint +++++
    //      for (int idx = 0; idx < port.ClientPortFunctions.Count; idx++)
    //      {
    //        blueprintElementFound = false;

    //        foreach (RteFunctionBlueprint blueprintElement in blueprintForSync.Functions)
    //        {
    //          if (port.ClientPortFunctions[idx].UUID == blueprintElement.UUID)
    //          {
    //            blueprintElementFound = true;
    //            break;
    //          }
    //        }

    //        // Check if Element has been found
    //        if (false == blueprintElementFound)
    //        {
    //          // Element not found >> Remove it
    //          port.ClientPortFunctions.RemoveAt(idx);
    //          idx--;
    //        }
    //      }

    //      // +++++ Add all elements which are new in the blueprint +++++
    //      foreach (RteFunctionBlueprint blueprintElement in blueprintForSync.Functions)
    //      {
    //        blueprintElementFound = false;

    //        foreach (RteGenericRunnableObject runnable in port.ClientPortFunctions)
    //        {
    //          if (runnable.UUID == blueprintElement.UUID)
    //          {
    //            blueprintElementFound = true;
    //            break;
    //          }
    //        }

    //        // Check if Element has been found
    //        if (false == blueprintElementFound)
    //        {
    //          // Element not found >> Add 
    //          RteGenericRunnableObject newRunnable = new RteGenericRunnableObject();
    //          newRunnable.Name = blueprintElement.FunctionName;
    //          newRunnable.UUID = blueprintElement.UUID;
    //          port.ClientPortFunctions.Add(newRunnable);
    //        }
    //      }
    //    }
    //  }
    //}


    /// <summary>
    /// Update the Backup lists
    /// </summary>
    //private void Hlp_AddNewBackupClientServerPortBlueprint(RteCSInterfaceBlueprint backupInterface)
    //{
    //  if (false == ActualMiB.BackupCSBlueprintList.Contains(backupInterface))
    //  {
    //    ActualMiB.BackupCSBlueprintList.Add(backupInterface);
    //  }
    //}
    #endregion
  }
}
/**
* @}
*/