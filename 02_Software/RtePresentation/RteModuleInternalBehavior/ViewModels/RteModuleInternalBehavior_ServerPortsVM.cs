﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior_ServerPortsVM.cs                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        30.12.2020                                                                                                   *
 * @brief       View Model of the RteModuleInternalBehavior >> Server Ports Part                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using RtePresentation.RteModuleInternalBehavior.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteModuleInternalBehavior.ViewModels
{
  internal class RteModuleInternalBehavior_ServerPortsVM : RteModuleInternalBehavior_BaseVM
  {
    private RteAvailableServerPort activeAvailablePort;
    private ObservableCollection<ObservableServerPortRunnable> allAvailableServerPortRunnables;
    private RteRunnable activeRunnable;
    private ObservableServerPortRunnable selectedRunnable;
    private RteCSInterfaceBlueprint selectedPortBlueprint;
    private RteAvailableServerPort selectedPortPrototype;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteModuleInternalBehavior_ServerPortsVM() : base()
    {
      activeRunnable = new RteRunnable();

      Hlp_SyncServerPrototypewithBlueprint();

      Hlp_UpdateAvailableServerRunnableList();

      Hlp_CreateANewPortPrototype();
    }


    #region Attributes
    /// <summary>
    /// Getter and Setter for activeAvailablePort
    /// </summary>
    public RteAvailableServerPort ActiveAvailablePort
    {
      get { return activeAvailablePort; }
      set { activeAvailablePort = value; }
    }

    /// <summary>
    /// Getter and Setter for allAvailableServerPortRunnables
    /// </summary>
    public ObservableCollection<ObservableServerPortRunnable> AllAvailableServerPortRunnables
    {
      get { return allAvailableServerPortRunnables; }
      //set { allAvailableServerPortRunnables = value; }
    }

    /// <summary>
    /// Getter and Setter for activeRunnable
    /// </summary>
    public RteRunnable ActiveRunnable
    {
      get { return activeRunnable; }
      set { activeRunnable = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedPortBlueprint
    /// </summary>
    public RteCSInterfaceBlueprint SelectedPortBlueprint
    {
      get { return selectedPortBlueprint; }
      set { selectedPortBlueprint = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedPortPrototype
    /// </summary>
    public RteAvailableServerPort SelectedPortPrototype
    {
      get { return selectedPortPrototype; }
      set { selectedPortPrototype = value; }
    }

    /// <summary>
    /// Getter and Setter for selectedRunnable
    /// </summary>
    public ObservableServerPortRunnable SelectedRunnable
    {
      get { return selectedRunnable; }
      set { selectedRunnable = value; }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Interface to add the port prototype
    /// </summary>
    public void AddActivePortPrototype()
    {
      try
      {
        // Add Blueprint interface information
        ActiveAvailablePort.ServerPort.InterfaceBlueprint = new RteGeneralInterfaceInfo(SelectedPortBlueprint.InterfaceInfo);

        // Add Blueprint runnable information
        foreach(RteFunctionBlueprint blueprintFunction in SelectedPortBlueprint.Functions)
        {
          RteRunnable newRunnable = new RteRunnable();
          RteGeneralInterfaceInfo newRunnableInfo = new RteGeneralInterfaceInfo();
          newRunnableInfo.InterfaceName = blueprintFunction.FunctionName;
          newRunnableInfo.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
          newRunnable.RunnableInfo = newRunnableInfo;

          ActiveAvailablePort.ServerPortRunnables.Add(newRunnable);
        }

        ActualMiB.AvailableServerPorts.Add(ActiveAvailablePort);
        Hlp_CreateANewPortPrototype();
        Hlp_UpdateAvailableServerRunnableList();

        // Update Backup Blueprint List
        AddNewBackupClientServerPortBlueprint(SelectedPortBlueprint);
      }
      catch(NullReferenceException)
      {
        MessageBox.Show("Please select a interface blueprint", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
      }
    }

    /// <summary>
    /// Interface to delete the port prototype
    /// </summary>
    public void DeleteSelectedPortPrototype()
    {
      try
      {
        ActualMiB.AvailableServerPorts.Remove(selectedPortPrototype);
        Hlp_UpdateAvailableServerRunnableList();
      }
      catch (NullReferenceException)
      {
        MessageBox.Show("Please select a interface blueprint", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
      }
    }

    /// <summary>
    /// Interface to update the active runnable
    /// </summary>
    public void UpdateActiveRunnable()
    {
      bool elementFound = false;

      // Check if for runnable in ports list Check with UUID
      // Each Port
      foreach (RteAvailableServerPort port in ActualMiB.AvailableServerPorts)
      {
        // Process each runnable
        foreach (RteRunnable runnable in port.ServerPortRunnables)
        {
          if(runnable.RunnableInfo.UUID == activeRunnable.RunnableInfo.UUID)
          {
            elementFound = true;

            // Replace runnable
            port.ServerPortRunnables.Remove(runnable);
            port.ServerPortRunnables.Add(activeRunnable);
            Hlp_UpdateAvailableServerRunnableList();

            // Reset active runnable >> Not really used
            activeRunnable = new RteRunnable();
            OnPropertyChanged(new PropertyChangedEventArgs("ActiveRunnable"));

            break;

          }
        }

        if(true == elementFound)
        { break; }
      }

      // element not found >> User error
      if(false == elementFound)
      {
        MessageBox.Show("Its not allowed to add new runnable. Only adaption's of existing runnables are allowed." +
          " If you want to create a new server runnable, you have to update the interface blueprint.",
          "Alert", MessageBoxButton.OK, MessageBoxImage.Exclamation);
      }
    }

    /// <summary>
    /// Interface to load the selected Runnable into the active runnable view
    /// </summary>
    public void LoadTheSelectedRunnable()
    {
      try
      {
        // Use copy constructor
        activeRunnable = new RteRunnable(SelectedRunnable.Runnable);

        OnPropertyChanged(new PropertyChangedEventArgs("ActiveRunnable"));
      }
      catch(NullReferenceException)
      {
        MessageBox.Show("Please select a runnable", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
      }
    }

    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to create a new Port Prototype
    /// </summary>
    private void Hlp_CreateANewPortPrototype()
    {
      ActiveAvailablePort = new RteAvailableServerPort();
      ActiveAvailablePort.ServerPort.InterfacePrototype.UUID =
        OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      ActiveAvailablePort.ServerPort.InterfaceType = RteCSInterfaceImplementationType.SERVER;

      OnPropertyChanged(new PropertyChangedEventArgs("ActiveAvailablePort"));
    }

    /// <summary>
    /// Helper function to update the list with all available server port runnables 
    /// </summary>
    private void Hlp_UpdateAvailableServerRunnableList()
    {
      allAvailableServerPortRunnables = new ObservableCollection<ObservableServerPortRunnable>();

      // Each Port
      foreach(RteAvailableServerPort port in ActualMiB.AvailableServerPorts)
      {
        // Process each runnable
        foreach(RteRunnable runnable in port.ServerPortRunnables)
        {
          ObservableServerPortRunnable newObservableRunnable = new ObservableServerPortRunnable();
          newObservableRunnable.Runnable = new RteRunnable(runnable);
          newObservableRunnable.BasePrototype = new RteGeneralInterfaceInfo(port.ServerPort.InterfacePrototype);
          // Create / Update list
          AllAvailableServerPortRunnables.Add(newObservableRunnable);
        }
      }

      OnPropertyChanged(new PropertyChangedEventArgs("AllAvailableServerPortRunnables"));
    }

    /// <summary>
    /// During startup the prototype will be synchronized with the blueprint
    /// </summary>
    private void Hlp_SyncServerPrototypewithBlueprint()
    {
      bool blueprintElementFound = false;
      bool blueprintFound = false;

      // Each Port Prototype
      foreach (RteAvailableServerPort port in ActualMiB.AvailableServerPorts)
      {
        RteCSInterfaceBlueprint blueprintForSync = new RteCSInterfaceBlueprint();
        //Search now for blueprint
        foreach (RteCSInterfaceBlueprint blueprint in ActualMiB.BackupCSBlueprintList)
        {
          if(blueprint.InterfaceInfo.UUID == port.ServerPort.InterfaceBlueprint.UUID)
          {
            blueprintForSync = blueprint;
            blueprintFound = true;
          }
        }


        if(true == blueprintFound)
        {
          // +++++ Remove all elements which are not longer a part of the blueprint +++++
          for(int idx = 0; idx < port.ServerPortRunnables.Count; idx++)
          {
            blueprintElementFound = false;

            foreach(RteFunctionBlueprint blueprintElement in blueprintForSync.Functions)
            {
              if(port.ServerPortRunnables[idx].RunnableInfo.UUID == blueprintElement.UUID)
              {
                blueprintElementFound = true;
                break;
              }
            }

            // Check if Element has been found
            if(false == blueprintElementFound)
            {
              // Element not found >> Remove it
              port.ServerPortRunnables.RemoveAt(idx);
              idx--;
            }
          }

          // +++++ Add all elements which are new in the blueprint +++++
          foreach (RteFunctionBlueprint blueprintElement in blueprintForSync.Functions)
          {
            blueprintElementFound = false;

            foreach(RteRunnable runnable in port.ServerPortRunnables)
            {
              if (runnable.RunnableInfo.UUID == blueprintElement.UUID)
              {
                blueprintElementFound = true;
                break;
              }
            }

            // Check if Element has been found
            if (false == blueprintElementFound)
            {
              // Element not found >> Add it

              RteRunnable newRunnable = new RteRunnable();
              newRunnable.RunnableInfo.InterfaceName = blueprintElement.FunctionName;
              newRunnable.RunnableInfo.UUID = blueprintElement.UUID;
              port.ServerPortRunnables.Add(newRunnable);
            }
          }
        }
      }
    }

    /// <summary>
    /// Update the Backup lists
    /// </summary>
    //private void Hlp_AddNewBackupClientServerPortBlueprint(RteCSInterfaceBlueprint backupInterface)
    //{
    //  if(false == ActualMiB.BackupCSBlueprintList.Contains(backupInterface))
    //  {
    //    ActualMiB.BackupCSBlueprintList.Add(backupInterface);
    //  }
    //}

    #endregion
  }
}
/**
* @}
*/