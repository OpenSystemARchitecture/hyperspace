﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior_OverviewVM.cs                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        29.12.2020                                                                                                   *
 * @brief       View Model of the RteModuleInternalBehavior >> Os Part                                                       *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteModuleInternalBehavior.ViewModels
{
  internal class RteModuleInternalBehavior_OsRunnablesVM : RteModuleInternalBehavior_BaseVM
  {

    //private string activeRunnableName;
    private RteCyclicRunnable activeCyclicRunnable;
    //private UInt16 activeCycleTime;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteModuleInternalBehavior_OsRunnablesVM() : base()
    {
      // Check Init Runnable Settings
      if (0 == ActualMiB.InitRunnables.Count)
      {
        RteRunnable newInitRunnable = new RteRunnable();
        RteGeneralInterfaceInfo initRunnableInfo = new RteGeneralInterfaceInfo();
        initRunnableInfo.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
        newInitRunnable.RunnableInfo = initRunnableInfo;
        ActualMiB.InitRunnables.Add(newInitRunnable);
      }

      Hlp_CreateANewRteCyclicRunnable();
    }

    #region Attributes
    /// <summary>
    /// Getter and Setter for the activeCyclicRunnable
    /// </summary>
    public RteCyclicRunnable ActiveCyclicRunnable
    {
      get { return activeCyclicRunnable; }
      set
      {
        activeCyclicRunnable = value;
        OnPropertyChanged(new PropertyChangedEventArgs("ActiveCyclicRunnable"));
      }
    }

    #endregion

    #region Actions
    /// <summary>
    /// Interface to add a new element
    /// </summary>
    public void AddNewElement()
    {
      bool elementFound = false;

      // Check if function element already exists >> Check with UUID
      foreach (RteCyclicRunnable runnable in ActualMiB.CyclicRunnables)
      {
        if (runnable.Runnable.RunnableInfo.UUID == ActiveCyclicRunnable.Runnable.RunnableInfo.UUID)
        {
          // Replace runnable
          // >> Delete runnable from list
          ActualMiB.CyclicRunnables.Remove(runnable);
          // >> Add new runnable to list
          ActualMiB.CyclicRunnables.Add(activeCyclicRunnable);
          elementFound = true;
          break;
        }
      }

      if (false == elementFound)
      {
        // Add runnable
        ActualMiB.CyclicRunnables.Add(activeCyclicRunnable);
      }

      Hlp_CreateANewRteCyclicRunnable();
    }

    /// <summary>
    /// Interface to create a new element
    /// </summary>
    public void CreateNewElement()
    {
      Hlp_CreateANewRteCyclicRunnable();
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to create a new cyclic rte runnable
    /// </summary>
    private void Hlp_CreateANewRteCyclicRunnable()
    {
      activeCyclicRunnable = new RteCyclicRunnable();
      activeCyclicRunnable.Runnable.RunnableInfo.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();

      OnPropertyChanged(new PropertyChangedEventArgs("ActiveCyclicRunnable"));
    }
    #endregion
  }
}
/**
* @}
*/