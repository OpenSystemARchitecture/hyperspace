﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior_OverviewVM.cs                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        29.12.2020                                                                                                   *
 * @brief       View Model of the RteModuleInternalBehavior >> Overview Part                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteModuleInternalBehavior.ViewModels
{
  internal class RteModuleInternalBehavior_OverviewVM : RteModuleInternalBehavior_BaseVM
  {
    /// <summary>
    /// Constructor
    /// </summary>
    public RteModuleInternalBehavior_OverviewVM() : base()
    {
    }
  }
}
/**
* @}
*/