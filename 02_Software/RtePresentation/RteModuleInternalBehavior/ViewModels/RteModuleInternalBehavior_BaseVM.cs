﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior_BaseVM.cs                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        29.12.2020                                                                                                   *
 * @brief       View Model of the RteModuleInternalBehavior >> Base Class Part                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using RteLib.RteTypes;
using RtePresentation.General.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteModuleInternalBehavior.ViewModels
{
  internal class RteModuleInternalBehavior_BaseVM : BaseViewModel
  {
    static private RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior actualMiB;     //!< Actual processed element
    static private RteLib.RteTypes.RteTypes rteTypesDb;                                      //!< Types Data base
    static private RteLib.RteInterface.RteInterfaces rteInterfaceDb;                         //!< Interface Data base
    static private RteLib.RteConfig.RteConfig rteConfigDb;                                   //!< Config Data base
    
    // UI Elements
    static private bool enableEdditing;
    static private ObservableCollection<RteGenericRunnableObject> allAvailableClientPortFunctions;
    static private ObservableCollection<RteGenericRunnableObject> allAvailableSenderReceiverPortElements;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteMiB">Actual MiB Object</param>
    /// <param name="rteTypes">Rte Types Database</param>
    /// <param name="rteInterfaces">Rte Interface Database</param>
    /// <param name="rteConfig">Rte Config Database</param>
    protected RteModuleInternalBehavior_BaseVM(RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior rteMiB,
      RteLib.RteTypes.RteTypes rteTypes, RteLib.RteInterface.RteInterfaces rteInterfaces,
      RteLib.RteConfig.RteConfig rteConfig)
    {
      actualMiB = rteMiB;
      rteTypesDb = rteTypes;
      rteInterfaceDb = rteInterfaces;
      rteConfigDb = rteConfig;

      // Sync MIB
      SyncMiBWithDataBase();

      // Setup UI Elements
      allAvailableClientPortFunctions = new ObservableCollection<RteGenericRunnableObject>();
      UpdateAvailableClientPortFunctionsUICollection();

      allAvailableSenderReceiverPortElements = new ObservableCollection<RteGenericRunnableObject>();
      UpdateAvailableSenderReceiverPortElementsUICollection();
    }

    /// <summary>
    /// Constructor
    /// </summary>
    protected RteModuleInternalBehavior_BaseVM()
    {
    }

    #region Attributes
    /// <summary>
    /// Getter and setter for the actualMiB
    /// </summary>
    public RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior ActualMiB
    {
      get { return actualMiB; }
      set { actualMiB = value; }
    }

    /// <summary>
    /// Getter and setter for the actualMiB
    /// </summary>
    public RteLib.RteTypes.RteTypes RteTypesDb
    {
      get { return rteTypesDb; }
      //set { rteTypesDb = value; }
    }

    /// <summary>
    /// Getter and setter for the rteInterfaceDb
    /// </summary>
    public RteLib.RteInterface.RteInterfaces RteInterfaceDb
    {
      get { return rteInterfaceDb; }
      //set { rteInterfaceDb = value; }
    }

    /// <summary>
    /// Getter and setter for the rteConfigDb
    /// </summary>
    public RteLib.RteConfig.RteConfig RteConfigDb
    {
      get { return rteConfigDb; }
      //set { rteConfigDb = value; }
    }


    /// <summary>
    /// Getter and Setter for the enableEdditing flag
    /// </summary>
    public bool EnableEdditing
    {
      get { return enableEdditing; }
      set 
      {
        enableEdditing = value;
        OnPropertyChanged(new PropertyChangedEventArgs("EnableEdditing"));
      }
    }

    /// <summary>
    /// Getter and Setter for the allAvailableClientPortFunctions field
    /// </summary>
    public ObservableCollection<RteGenericRunnableObject> AllAvailableClientPortFunctions
    {
      get { return allAvailableClientPortFunctions; }
      set 
      { allAvailableClientPortFunctions = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AllAvailableClientPortFunctions"));
      }
    }

    /// <summary>
    /// Getter and Setter for the allAvailableSenderReceiverPortElements field
    /// </summary>
    public ObservableCollection<RteGenericRunnableObject> AllAvailableSenderReceiverPortElements
    {
      get { return allAvailableSenderReceiverPortElements; }
      set
      {
        allAvailableSenderReceiverPortElements = value;
        OnPropertyChanged(new PropertyChangedEventArgs("AllAvailableSenderReceiverPortElements"));
      }
    }
    #endregion

    #region Actions
    /// <summary>
    /// Helper function which update the UI Collection with all available Client Port functions
    /// </summary>
    protected void UpdateAvailableClientPortFunctionsUICollection()
    {
      allAvailableClientPortFunctions.Clear();
      foreach (RteAvailableClientPort port in ActualMiB.AvailableClientPorts)
      {
        foreach (RteGenericRunnableObject runnableObj in port.ClientPortFunctions)
        {
          allAvailableClientPortFunctions.Add(runnableObj);
        }
      }

      OnPropertyChanged(new PropertyChangedEventArgs("AllAvailableClientPortFunctions"));
    }

    /// <summary>
    /// Helper function which update the UI Collection with all available Sender Receiver Port elements
    /// </summary>
    protected void UpdateAvailableSenderReceiverPortElementsUICollection()
    {
      allAvailableSenderReceiverPortElements.Clear();

      foreach (RteAvailableSRPort port in ActualMiB.AvailableSRPorts)
      {
        foreach (RteGenericRunnableObject runnableObj in port.SenderReceiverPortElements)
        {
          allAvailableSenderReceiverPortElements.Add(runnableObj);
        }
      }

      OnPropertyChanged(new PropertyChangedEventArgs("AllAvailableSenderReceiverPortElements"));
    }
    #endregion

    #region Helper Functions

    /// <summary>
    /// General Sync backup data with database 
    /// </summary>
    protected void SyncMiBWithDataBase()
    {
      int listIdx = 0;
      List<RteCSInterfaceBlueprint> newBackupCSBlueprintList = new List<RteCSInterfaceBlueprint>();
      List<RteSRInterfaceBlueprint> newBackupSRBlueprintList = new List<RteSRInterfaceBlueprint>();

      // Sync backup CS Blueprint with data base
      foreach (RteCSInterfaceBlueprint blueprint in ActualMiB.BackupCSBlueprintList)
      {
        if(true == rteInterfaceDb.IsCSInterfaceAvailable(blueprint.InterfaceInfo, out listIdx))
        {
          newBackupCSBlueprintList.Add(rteInterfaceDb.ClientServerBlueprintList[listIdx]);
        }
      }
      ActualMiB.BackupCSBlueprintList = newBackupCSBlueprintList;

      // Sync backup SR Blueprint with data base
      foreach (RteSRInterfaceBlueprint blueprint in ActualMiB.BackupSRBlueprintList)
      {
        if (true == rteInterfaceDb.IsSRInterfaceAvailable(blueprint.InterfaceInfo, out listIdx))
        {
          newBackupSRBlueprintList.Add(rteInterfaceDb.SenderReceiverBlueprintList[listIdx]);
        }
      }
      ActualMiB.BackupSRBlueprintList = newBackupSRBlueprintList;

      // Sync backup Types
      SyncBackupDataTypesWithBackupBlueprints();

      // Sync CS Prototypes
      SyncClientPrototypewithBlueprint();

      // Sync SR Prototypes
      SyncSRPrototypewithBlueprint();
    }

    /// <summary>
    /// Create a new backup data type list based on the backup interfaces and store them as backup
    /// </summary>
    protected void SyncBackupDataTypesWithBackupBlueprints()
    {
      int listIdx = 0;
      List<RteAdvancedDataTypeStructure> newBackupAdvancedDataTypeList = new List<RteAdvancedDataTypeStructure>();

      // Sync with backup CS Blueprint
      foreach (RteCSInterfaceBlueprint blueprintIf in ActualMiB.BackupCSBlueprintList)
      {
        //Check each function
        foreach (RteFunctionBlueprint fnc in blueprintIf.Functions)
        {
          // Check return value
          if (true == rteTypesDb.IsDataTypeAvailable(fnc.ReturnValue, out listIdx))
          {
            // If list index is < 0 than an base data type has been found >> Only Advanced data types are stored here
            if (0 <= listIdx)
            {
              if (false == newBackupAdvancedDataTypeList.Contains(rteTypesDb.AdvancedDataTypeList[listIdx]))
              {
                // Store Backup Data
                newBackupAdvancedDataTypeList.Add(rteTypesDb.AdvancedDataTypeList[listIdx]);
              }
            }
          }

          // Check each argument
          foreach (RteFunctionArgumentBlueprint arg in fnc.ArgList)
          {
            if (true == rteTypesDb.IsDataTypeAvailable(arg.ArgumentType, out listIdx))
            {
              // If list index is < 0 than an base data type has been found >> Only Advanced data types are stored here
              if(0 <= listIdx)
              {
                if (false == newBackupAdvancedDataTypeList.Contains(rteTypesDb.AdvancedDataTypeList[listIdx]))
                {
                  // Store Backup Data
                  newBackupAdvancedDataTypeList.Add(rteTypesDb.AdvancedDataTypeList[listIdx]);
                }
              }
            }
          }
        }
      }

      // Store new Backup Data
      ActualMiB.BackupAdvancedDataTypeList = newBackupAdvancedDataTypeList;
    }

    /// <summary>
    /// During startup the prototype will be synchronized with the blueprint
    /// </summary>
    protected void SyncClientPrototypewithBlueprint()
    {
      bool blueprintElementFound = false;
      bool blueprintFound = false;

      // Each Port Prototype
      foreach (RteAvailableClientPort port in ActualMiB.AvailableClientPorts)
      {
        RteCSInterfaceBlueprint blueprintForSync = new RteCSInterfaceBlueprint();
        //Search now for blueprint
        foreach (RteCSInterfaceBlueprint blueprint in ActualMiB.BackupCSBlueprintList)
        {
          if (blueprint.InterfaceInfo.UUID == port.ClientPort.InterfaceBlueprint.UUID)
          {
            blueprintForSync = blueprint;
            blueprintFound = true;
          }
        }


        if (true == blueprintFound)
        {
          // +++++ Remove all elements which are not longer a part of the blueprint +++++
          for (int idx = 0; idx < port.ClientPortFunctions.Count; idx++)
          {
            blueprintElementFound = false;

            foreach (RteFunctionBlueprint blueprintElement in blueprintForSync.Functions)
            {
              if (port.ClientPortFunctions[idx].Name == blueprintElement.FunctionName)
              {
                blueprintElementFound = true;
                break;
              }
            }

            // Check if Element has been found
            if (false == blueprintElementFound)
            {
              // Element not found >> Remove it
              port.ClientPortFunctions.RemoveAt(idx);
              idx--;
            }
          }

          // +++++ Add all elements which are new in the blueprint +++++
          foreach (RteFunctionBlueprint blueprintElement in blueprintForSync.Functions)
          {
            blueprintElementFound = false;

            foreach (RteGenericRunnableObject runnable in port.ClientPortFunctions)
            {
              if (runnable.Name == blueprintElement.FunctionName)
              {
                blueprintElementFound = true;
                break;
              }
            }

            // Check if Element has been found
            if (false == blueprintElementFound)
            {
              // Element not found >> Add 
              RteGenericRunnableObject newRunnable = new RteGenericRunnableObject();
              newRunnable.Name = blueprintElement.FunctionName;
              newRunnable.UUID = blueprintElement.UUID;
              port.ClientPortFunctions.Add(newRunnable);
            }
          }
        }
      }
    }

    /// <summary>
    /// During startup the prototype will be synchronized with the blueprint
    /// </summary>
    protected void SyncSRPrototypewithBlueprint()
    {
      bool blueprintElementFound = false;
      bool blueprintFound = false;

      // Each Port Prototype
      foreach (RteAvailableSRPort port in ActualMiB.AvailableSRPorts)
      {
        RteSRInterfaceBlueprint blueprintForSync = new RteSRInterfaceBlueprint();
        //Search now for blueprint
        foreach (RteSRInterfaceBlueprint blueprint in ActualMiB.BackupSRBlueprintList)
        {
          if (blueprint.InterfaceInfo.UUID == port.SenderReceiverPort.InterfaceBlueprint.UUID)
          {
            blueprintForSync = blueprint;
            blueprintFound = true;
          }
        }


        if (true == blueprintFound)
        {
          // +++++ Remove all elements which are not longer a part of the blueprint +++++
          for (int idx = 0; idx < port.SenderReceiverPortElements.Count; idx++)
          {
            blueprintElementFound = false;

            foreach (RteElementBlueprint blueprintElement in blueprintForSync.Elements)
            {
              if (port.SenderReceiverPortElements[idx].Name == blueprintElement.ElementName)
              {
                blueprintElementFound = true;
                break;
              }
            }

            // Check if Element has been found
            if (false == blueprintElementFound)
            {
              // Element not found >> Remove it
              port.SenderReceiverPortElements.RemoveAt(idx);
              idx--;
            }
          }

          // +++++ Add all elements which are new in the blueprint +++++
          foreach (RteElementBlueprint blueprintElement in blueprintForSync.Elements)
          {
            blueprintElementFound = false;

            foreach (RteGenericRunnableObject runnable in port.SenderReceiverPortElements)
            {
              if (runnable.Name == blueprintElement.ElementName)
              {
                blueprintElementFound = true;
                break;
              }
            }

            // Check if Element has been found
            if (false == blueprintElementFound)
            {
              // Element not found >> Add it

              RteGenericRunnableObject newRunnable = new RteGenericRunnableObject();
              newRunnable.Name = blueprintElement.ElementName;
              newRunnable.UUID = blueprintElement.UUID;
              port.SenderReceiverPortElements.Add(newRunnable);
            }
          }
        }
      }
    }

    /// <summary>
    /// Update the CS Blueprint Backup lists
    /// </summary>
    protected void AddNewBackupClientServerPortBlueprint(RteCSInterfaceBlueprint backupInterface)
    {
      if (false == ActualMiB.BackupCSBlueprintList.Contains(backupInterface))
      {
        ActualMiB.BackupCSBlueprintList.Add(backupInterface);
        SyncBackupDataTypesWithBackupBlueprints();
      }
    }

    /// <summary>
    /// Update the SR Blueprint Backup lists
    /// </summary>
    protected void AddNewBackupSenderReceiverPortBlueprint(RteSRInterfaceBlueprint backupInterface)
    {
      if (false == ActualMiB.BackupSRBlueprintList.Contains(backupInterface))
      {
        ActualMiB.BackupSRBlueprintList.Add(backupInterface);
        SyncBackupDataTypesWithBackupBlueprints();
      }
    }

    #endregion
  }
}
