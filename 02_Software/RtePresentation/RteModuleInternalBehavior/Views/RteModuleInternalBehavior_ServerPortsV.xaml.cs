﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior_ServerPortsV.xaml.cs                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        30.12.2020                                                                                                   *
 * @brief       View of the RteModuleInternalBehavior >> Server Ports Part                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteModuleInternalBehavior.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteModuleInternalBehavior;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteModuleInternalBehavior.Views
{
  /// <summary>
  /// Interaction logic for RteModuleInternalBehavior_ServerPortsV.xaml
  /// </summary>
  public partial class RteModuleInternalBehavior_ServerPortsV : UserControl
  {
    ViewModels.RteModuleInternalBehavior_ServerPortsVM usedCSPortVM;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteModuleInternalBehavior_ServerPortsV()
    {
      usedCSPortVM = new ViewModels.RteModuleInternalBehavior_ServerPortsVM();

      InitializeComponent();

      this.DataContext = usedCSPortVM;
    }

    #region UI Elements

    #region Port Prototype
    /// <summary>
    /// Interface to add a new Element to list 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_StoreDefinedPrototype_Click(object sender, RoutedEventArgs e)
    {
      usedCSPortVM.AddActivePortPrototype();

      LV_Port_Prototypes.Items.Refresh();
      LV_PortRunnableList.Items.Refresh();
    }

    /// <summary>
    /// Interface to delete the selected element form list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_DeleteSelectedPrototype_Click(object sender, RoutedEventArgs e)
    {
      usedCSPortVM.DeleteSelectedPortPrototype();
      LV_Port_Prototypes.Items.Refresh();
      LV_PortRunnableList.Items.Refresh();
    }

    #endregion

    #region Port Runnables
    /// <summary>
    /// Interface to add a new Element to list 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_StoreDefinedElement_Click(object sender, RoutedEventArgs e)
    {
      usedCSPortVM.UpdateActiveRunnable();
      LV_PortRunnableList.Items.Refresh();
    }

    /// <summary>
    /// Interface to add a load a selected Element from list 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_LoadSelectedElement_Click(object sender, RoutedEventArgs e)
    {
      usedCSPortVM.LoadTheSelectedRunnable();
    }
    #endregion

    #region Runnable Port Elements
    /// <summary>
    /// Interface to delete the selected element form the Client Port function access list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_Runnable_DeleteSelectedClientPortFunction_Click(object sender, RoutedEventArgs e)
    {
      if (null != LV_Runnable_ClientPortFunctionAccess.SelectedItem)
      {
        usedCSPortVM.ActiveRunnable.AccessToClientPortFunctions.Remove(
        (RteGenericRunnableObject)LV_Runnable_ClientPortFunctionAccess.SelectedItem);

        LV_Runnable_ClientPortFunctionAccess.Items.Refresh();
      }
    }

    /// <summary>
    /// Interface to delete the selected element form the SR Port function access list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_Runnable_DeleteSelectedSRPortElement_Click(object sender, RoutedEventArgs e)
    {
      if (null != LV_Runnable_SenderReceiverPortElementAccess.SelectedItem)
      {
        usedCSPortVM.ActiveRunnable.AccessToSenderReceiverPortElements.Remove(
        (RteGenericRunnableObject)LV_Runnable_SenderReceiverPortElementAccess.SelectedItem);

        LV_Runnable_SenderReceiverPortElementAccess.Items.Refresh();
      }
    }
    #endregion

    #region Drag & Drop Cyclic Runnable
    /// <summary>
    /// Event handler for dropping data into list
    /// Copy element into internal list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LV_Runnable_ClientPortFunctionAccess_Drop(object sender, DragEventArgs e)
    {
      ListView parent = (ListView)sender;
      object data = e.Data.GetData(typeof(RteGenericRunnableObject));

      // Check if source is the correct container
      if (true == LV_AvailableClientPortFunctions.Items.Contains(data))
      {
        // Check if element is already available
        if (false == LV_Runnable_ClientPortFunctionAccess.Items.Contains(data))
        {
          usedCSPortVM.ActiveRunnable.AccessToClientPortFunctions.Add((RteGenericRunnableObject)data);
          LV_Runnable_ClientPortFunctionAccess.Items.Refresh();

          // Data copied
          e.Effects = DragDropEffects.Copy;
        }
        else
        {
          // Data not copied >> Only a move event occurred
          e.Effects = DragDropEffects.Move;
        }
      }
    }

    /// <summary>
    /// Event handler for dropping data into list
    /// Copy element into internal list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LV_Runnable_SenderReceiverPortElementAccess_Drop(object sender, DragEventArgs e)
    {
      ListView parent = (ListView)sender;
      object data = e.Data.GetData(typeof(RteGenericRunnableObject));

      // Check if source is the correct container
      if (true == LV_AvailableSenderReceiverPortElements.Items.Contains(data))
      {
        // Check if element is already available
        if (false == LV_Runnable_SenderReceiverPortElementAccess.Items.Contains(data))
        {
          usedCSPortVM.ActiveRunnable.AccessToSenderReceiverPortElements.Add((RteGenericRunnableObject)data);
          LV_Runnable_SenderReceiverPortElementAccess.Items.Refresh();

          // Data copied
          e.Effects = DragDropEffects.Copy;
        }
        else
        {
          // Data not copied >> Only a move event occurred
          e.Effects = DragDropEffects.Move;
        }
      }
    }
    #endregion

    #region Drag & Drop AvailableClientPortFunctions
    /// <summary>
    /// Event handler to identify a drag operation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LV_AvailableClientPortFunctions_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      ListView parent = (ListView)sender;
      ListView dragSource = null;
      dragSource = parent;
      object data = GetDataFromListBox(dragSource, e.GetPosition(parent));

      if (data != null)
      {
        DragDropEffects retVal = DragDrop.DoDragDrop(parent, data, DragDropEffects.Copy | DragDropEffects.None | DragDropEffects.Move);

        if (retVal == DragDropEffects.Move)
        {
          // Move in own system detected. Do nothing
        }

        if (retVal == DragDropEffects.Copy)
        {
          // Data copied. Do nothing
        }

        if (retVal == DragDropEffects.None)
        {
          // Move in invalid areas detected. Do nothing
        }
      }
    }

    #endregion

    #region Drag & Drop AvailableSenderReceiverPortElements
    /// <summary>
    /// Event handler to identify a drag operation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LV_AvailableSenderReceiverPortElements_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      ListView parent = (ListView)sender;
      ListView dragSource = null;
      dragSource = parent;
      object data = GetDataFromListBox(dragSource, e.GetPosition(parent));

      if (data != null)
      {
        DragDropEffects retVal = DragDrop.DoDragDrop(parent, data, DragDropEffects.Copy | DragDropEffects.None | DragDropEffects.Move);

        if (retVal == DragDropEffects.Move)
        {
          // Move in own system detected. Do nothing
        }

        if (retVal == DragDropEffects.Copy)
        {
          // Data copied. Do nothing
        }

        if (retVal == DragDropEffects.None)
        {
          // Move in invalid areas detected. Do nothing
        }
      }
    }
    #endregion

    #endregion


    #region Helper functions
    /// <summary>
    /// Interface to get the ListView object, the mouse is pointing to.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    private static object GetDataFromListBox(ListView source, Point point)
    {
      UIElement element = source.InputHitTest(point) as UIElement;
      if (element != null)
      {
        object data = DependencyProperty.UnsetValue;
        while (data == DependencyProperty.UnsetValue)
        {
          data = source.ItemContainerGenerator.ItemFromContainer(element);

          if (data == DependencyProperty.UnsetValue)
          {
            element = VisualTreeHelper.GetParent(element) as UIElement;
          }

          if (element == source)
          {
            return null;
          }
        }

        if (data != DependencyProperty.UnsetValue)
        {
          return data;
        }
      }

      return null;
    }

    /// <summary>
    /// Event handler for drag enter 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LV_DragEnter(object sender, DragEventArgs e)
    {
      e.Effects = DragDropEffects.None;
    }

    #endregion
  }
}
/**
* @}
*/