﻿/*****************************************************************************************************************************
 * @file        RteCSInterfaceBlueprintVM.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        27.12.2020                                                                                                   *
 * @brief       View Model of the RteCSInterfaceBlueprint                                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteInterface.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteTypes;
using RtePresentation.General.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteInterface.ViewModels
{
  internal class RteCSInterfaceBlueprintVM : BaseViewModel
  {
    private RteCSInterfaceBlueprint csInterfaceBlueprint;       //!< Actual processed element
    private RteLib.RteTypes.RteTypes rteTypesDb;                //!< Types Data base

    private List<object> baseDataTypes;                         //!< List with all data types which are available

    private string notificationText;
    private string notificationImageSource;
    private bool enableEdditing;


    //Function
    private string activeFunctionName;
    private RteFunctionBlueprint selectedFunction;
    private RteFunctionBlueprint activeFunction;


    // Arguments
    private RteFunctionArgumentBlueprint selectedArgument;
    private string activeArgumentName;
    private object selectedDataType;
    private RteDataAccessType selectedAccessModifier;
    private RteFunctionArgumentBlueprint newArgument;


    // Error type
    private string selectedErrorType;
    private string activeErrorType;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteCSInterfaceBlueprint"></param>
    /// <param name="rteTypes"></param>
    public RteCSInterfaceBlueprintVM(RteCSInterfaceBlueprint rteCSInterfaceBlueprint, RteLib.RteTypes.RteTypes rteTypes)
    {
      csInterfaceBlueprint = rteCSInterfaceBlueprint;
      rteTypesDb = rteTypes;

      // Setup base data type list
      baseDataTypes = new List<object>();
      foreach (RteBaseDataTypeStructure dataType in rteTypesDb.BaseDataTypeList)
      { baseDataTypes.Add(dataType); }
      foreach (RteAdvancedDataTypeStructure dataType in rteTypesDb.AdvancedDataTypeList)
      { baseDataTypes.Add(dataType); }

      // Set Notification Information
      if (( null != rteCSInterfaceBlueprint.InterfaceInfo.UUID ) && ( "" != rteCSInterfaceBlueprint.InterfaceInfo.UUID ))
      {
        // UUID Set >> Existing interface identified
        notificationText = General.Models.RtePresentationResources.Base_NotificationText_ExistingInterfaceBlueprint;
        notificationImageSource = General.Models.RtePresentationResources.Base_NotificationText_ExistingElement_ImageSource;
        EnableEdditing = false;
      }
      else
      {
        notificationText = General.Models.RtePresentationResources.Base_NotificationText_NewInterfaceBlueprint;
        notificationImageSource = General.Models.RtePresentationResources.Base_NotificationText_NewElement_ImageSource;
        EnableEdditing = true;

        // New interface >> Create a UUID
        rteCSInterfaceBlueprint.InterfaceInfo.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      }


      Hlp_CreateNewFunctionBlueprint();

    }

    #region Properties
    /// <summary>
    /// Getter and Setter for the actual element
    /// </summary>
    public RteCSInterfaceBlueprint CSInterfaceBlueprint
    {
      get { return csInterfaceBlueprint; }
      set { csInterfaceBlueprint = value; }
    }

    /// <summary>
    /// Getter for the Notification image source
    /// </summary>
    public string NotficationImage
    {
      get { return notificationImageSource; }
    }

    /// <summary>
    /// Getter for the Notification Text
    /// </summary>
    public string NotificationText
    {
      get { return notificationText; }
    }

    /// <summary>
    /// Getter and Setter for the enableEdditing flag
    /// </summary>
    public bool EnableEdditing
    {
      get { return enableEdditing; }
      set
      {
        enableEdditing = value;
        OnPropertyChanged(new PropertyChangedEventArgs("EnableEdditing"));
      }
    }


    /// <summary>
    /// Getter and Setter for the activeFunction field
    /// </summary>
    public RteFunctionBlueprint ActiveFunction
    {
      get { return activeFunction; }
      set { activeFunction = value; }
    }

    /// <summary>
    /// Getter and Setter for the activeFunctionName field
    /// </summary>
    public string ActiveFunctionName
    {
      get { return activeFunctionName; }
      set { activeFunctionName = value; }
    }

    /// <summary>
    /// Getter and Setter for the selectedFunction field
    /// </summary>
    public RteFunctionBlueprint SelectedFunction
    {
      get { return selectedFunction; }
      set { selectedFunction = value; }
    }

    /// <summary>
    /// Getter and Setter for the selectedErrorType field
    /// </summary>
    public string SelectedErrorType
    {
      get { return selectedErrorType; }
      set { selectedErrorType = value; }
    }

    /// <summary>
    /// Getter and Setter for the activeErrorType field
    /// </summary>
    public string ActiveErrorType
    {
      get { return activeErrorType; }
      set { activeErrorType = value; }
    }




    /// <summary>
    /// Getter and Setter for the activeArgumentName field
    /// </summary>
    public string ActiveArgumentName
    {
      get { return activeArgumentName; }
      set { activeArgumentName = value; }
    }

    /// <summary>
    /// Getter and Setter for the selectedArgument field
    /// </summary>
    public RteFunctionArgumentBlueprint SelectedArgument
    {
      get { return selectedArgument; }
      set { selectedArgument = value; }
    }

    /// <summary>
    /// Getter for a list of all available data types
    /// </summary>
    public List<Object> BaseDataTypeList
    {
      get { return baseDataTypes; }
    }

    /// <summary>
    /// Getter for a the selectedDataType
    /// </summary>
    public object SelectedDataType
    {
      get { return selectedDataType; }
      set { selectedDataType = value; }
    }

    /// <summary>
    /// Getter for a the selectedAccessModifier
    /// </summary>
    public RteDataAccessType SelectedAccessModifier
    {
      get { return selectedAccessModifier; }
      set { selectedAccessModifier = value; }
    }
    #endregion













    #region Actions
    /// <summary>
    /// Interface to add a new element
    /// </summary>
    public void AddNewElement()
    {
      bool elementFound = false;

      // Update active function with user defined elements
      activeFunction.FunctionName = activeFunctionName;

      // Check if function element already exists >> Check with UUID
      foreach(RteFunctionBlueprint fnc in csInterfaceBlueprint.Functions)
      {
        if(fnc.UUID == activeFunction.UUID)
        {
          // Replace function
          // >> Delete function from list
          csInterfaceBlueprint.Functions.Remove(fnc);
          // >> Add new function to list
          csInterfaceBlueprint.Functions.Add(activeFunction);
          elementFound = true;
          break;
        }
      }

      if(false == elementFound)
      {
        // Add functions
        csInterfaceBlueprint.Functions.Add(activeFunction);
      }

      Hlp_CreateNewFunctionBlueprint();
    }

    /// <summary>
    /// Interface to load the selected element
    /// </summary>
    public void LoadSelectedElement()
    {
      try
      {
        // Create a object copy of the selected function
        activeFunction = new RteFunctionBlueprint(selectedFunction);

        // Update active function with user defined elements
        ActiveFunctionName = activeFunction.FunctionName;
        OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunctionName"));

        OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunction"));
        OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunction.AvailableErrorTypes"));
        OnPropertyChanged(new PropertyChangedEventArgs("ActiveErrorType"));
      }
      catch(NullReferenceException ex)
      {
        MessageBox.Show("Please select a function entry", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
        // Do nothing in here >> Selected index is zero
      }
    }

    /// <summary>
    /// Interface to create a new element
    /// </summary>
    public void CreateNewElement()
    {
      Hlp_CreateNewFunctionBlueprint();
    }

    /// <summary>
    /// Interface to remove the selected struct element
    /// </summary>
    public void DeleteSelectedElement()
    {
      csInterfaceBlueprint.Functions.Remove(selectedFunction);
    }







    /// <summary>
    /// Interface to add a new error type
    /// </summary>
    public void AddNewErrorType()
    {
      activeFunction.AvailableErrorTypes.Add(activeErrorType);

      activeErrorType = "";

      OnPropertyChanged(new PropertyChangedEventArgs("ActiveErrorType"));
      OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunction.AvailableErrorTypes"));
    }

    /// <summary>
    /// Interface to remove the selected struct element
    /// </summary>
    public void DeleteSelectedErrorType()
    {
      activeFunction.AvailableErrorTypes.Remove(selectedErrorType);
      OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunction.AvailableErrorTypes"));
    }









    /// <summary>
    /// Interface to add a new error type
    /// </summary>
    public void AddNewArgument()
    {
      try
      {
        // Create a new Argument object
        newArgument = new RteFunctionArgumentBlueprint();
        newArgument.ArgumentName = activeArgumentName;

        if (typeof(RteLib.RteTypes.RteBaseDataTypeStructure) == selectedDataType.GetType())
        {
          // Use copy constructor of RteBaseDataTypeStructure
          newArgument.ArgumentType = new RteBaseDataTypeStructure((RteLib.RteTypes.RteBaseDataTypeStructure)selectedDataType);
        }
        else
        {
          // Use copy constructor of RteBaseDataTypeStructure
          newArgument.ArgumentType = new RteBaseDataTypeStructure(
            ( (RteLib.RteTypes.RteAdvancedDataTypeStructure)selectedDataType ).DataType);
        }

        newArgument.ArgumentType.DataAccessType = selectedAccessModifier;
        activeFunction.ArgList.Add(newArgument);


        //Update Properties
        activeArgumentName = "";
        selectedAccessModifier = RteDataAccessType.STANDARD;

        OnPropertyChanged(new PropertyChangedEventArgs("ActiveArgumentName"));
        OnPropertyChanged(new PropertyChangedEventArgs("SelectedAccessModifier"));
        OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunction"));
      }
      catch(NullReferenceException ex)
      {
        MessageBox.Show("Please select a data type", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
        // Do nothing in here >> Selected index is zero
      }
    }

    /// <summary>
    /// Interface to remove the selected struct element
    /// </summary>
    public void DeleteSelectedArgument()
    {
      activeFunction.ArgList.Remove(selectedArgument);
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to create a new function blueprint object with init values
    /// </summary>
    private void Hlp_CreateNewFunctionBlueprint()
    {
      activeFunction = new RteFunctionBlueprint();
      activeFunction.ReturnValue = new RteLib.RteTypes.RteBaseTypesResource().Rte_ErrorType_Type.DataType;
      activeFunction.AvailableErrorTypes.Add("E_OK");
      activeFunction.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();

      activeFunctionName = "";
      OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunctionName"));

      OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunction"));
      OnPropertyChanged(new PropertyChangedEventArgs("ActiveFunction.AvailableErrorTypes"));
      OnPropertyChanged(new PropertyChangedEventArgs("ActiveErrorType"));
    }
    #endregion
  }
}
/**
* @}
*/