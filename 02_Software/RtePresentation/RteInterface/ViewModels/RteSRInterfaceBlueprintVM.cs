﻿/*****************************************************************************************************************************
 * @file        RteSRInterfaceBlueprintVM.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        27.12.2020                                                                                                   *
 * @brief       View Model of the RteSRInterfaceBlueprint                                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteInterface.ViewModels
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using RteLib.RteTypes;
using RtePresentation.General.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteInterface.ViewModels
{
  internal class RteSRInterfaceBlueprintVM : BaseViewModel
  {
    private RteSRInterfaceBlueprint srInterfaceBlueprint;       //!< Actual processed element
    private RteLib.RteTypes.RteTypes rteTypesDb;                //!< Types Data base

    private List<object> baseDataTypes;                         //!< List with all data types which are available

    private string notificationText;
    private string notificationImageSource;
    private bool enableEdditing;

    private string newElementName;
    private object selectedDataType;
    private RteDataAccessType selectedAccessModifier;

    private RteElementBlueprint selectedElement;
    private RteElementBlueprint newElement;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteSRInterfaceBlueprint"></param>
    /// <param name="rteTypes"></param>
    public RteSRInterfaceBlueprintVM(RteSRInterfaceBlueprint rteSRInterfaceBlueprint, RteLib.RteTypes.RteTypes rteTypes)
    {
      srInterfaceBlueprint = rteSRInterfaceBlueprint;
      rteTypesDb = rteTypes;

      // Setup base data type list
      baseDataTypes = new List<object>();
      foreach (RteBaseDataTypeStructure dataType in rteTypesDb.BaseDataTypeList)
      { baseDataTypes.Add(dataType); }
      foreach (RteAdvancedDataTypeStructure dataType in rteTypesDb.AdvancedDataTypeList)
      { baseDataTypes.Add(dataType); }

      // Set Notification Information
      if (( null != rteSRInterfaceBlueprint.InterfaceInfo.UUID ) && ( "" != rteSRInterfaceBlueprint.InterfaceInfo.UUID ))
      {
        // UUID Set >> Existing interface identified
        notificationText = General.Models.RtePresentationResources.Base_NotificationText_ExistingInterfaceBlueprint;
        notificationImageSource = General.Models.RtePresentationResources.Base_NotificationText_ExistingElement_ImageSource;
        EnableEdditing = false;
      }
      else
      {
        notificationText = General.Models.RtePresentationResources.Base_NotificationText_NewInterfaceBlueprint;
        notificationImageSource = General.Models.RtePresentationResources.Base_NotificationText_NewElement_ImageSource;
        EnableEdditing = true;

        // New interface >> Create a UUID
        rteSRInterfaceBlueprint.InterfaceInfo.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      }
    }

    #region Properties
    /// <summary>
    /// Getter and Setter for the actual element
    /// </summary>
    public RteSRInterfaceBlueprint SRInterfaceBlueprint
    {
      get { return srInterfaceBlueprint; }
      set { srInterfaceBlueprint = value; }
    }

    /// <summary>
    /// Getter for the Notification image source
    /// </summary>
    public string NotficationImage
    {
      get { return notificationImageSource; }
    }

    /// <summary>
    /// Getter for the Notification Text
    /// </summary>
    public string NotificationText
    {
      get { return notificationText; }
    }

    /// <summary>
    /// Getter and Setter for the enableEdditing flag
    /// </summary>
    public bool EnableEdditing
    {
      get { return enableEdditing; }
      set
      {
        enableEdditing = value;
        OnPropertyChanged(new PropertyChangedEventArgs("EnableEdditing"));
      }
    }

    /// <summary>
    /// Getter and Setter for the newElementName member
    /// </summary>
    public string NewElementName
    {
      get { return newElementName; }
      set { newElementName = value; }
    }

    /// <summary>
    /// Getter for a list of all available data types
    /// </summary>
    public List<Object> BaseDataTypeList
    {
      get { return baseDataTypes; }
    }

    /// <summary>
    /// Getter for a the selectedDataType
    /// </summary>
    public object SelectedDataType
    {
      get { return selectedDataType; }
      set { selectedDataType = value; }
    }

    /// <summary>
    /// Getter for a the selectedAccessModifier
    /// </summary>
    public RteDataAccessType SelectedAccessModifier
    {
      get { return selectedAccessModifier; }
      set { selectedAccessModifier = value; }
    }

    /// <summary>
    /// Getter and Setter for the selectedElement
    /// </summary>
    public RteElementBlueprint SelectedElement
    {
      get { return selectedElement; }
      set { selectedElement = value; }
    }

    #endregion

    #region Actions
    /// <summary>
    /// Interface to add a new element
    /// </summary>
    public void AddNewElement()
    {
      newElement = new RteElementBlueprint();
      newElement.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      newElement.ElementName = newElementName;

      if (typeof(RteLib.RteTypes.RteBaseDataTypeStructure) == selectedDataType.GetType())
      {
        // Use copy constructor of RteBaseDataTypeStructure
        newElement.ElementType = new RteBaseDataTypeStructure((RteLib.RteTypes.RteBaseDataTypeStructure)selectedDataType);
      }
      else
      {
        // Use copy constructor of RteBaseDataTypeStructure
        newElement.ElementType = new RteBaseDataTypeStructure(
          ( (RteLib.RteTypes.RteAdvancedDataTypeStructure)selectedDataType ).DataType);
      }

      newElement.ElementType.DataAccessType = selectedAccessModifier;
      SRInterfaceBlueprint.Elements.Add(newElement);


      //Update Properties
      selectedAccessModifier = RteDataAccessType.STANDARD;
      newElementName = "";

      OnPropertyChanged(new PropertyChangedEventArgs("SelectedAccessModifier"));
      OnPropertyChanged(new PropertyChangedEventArgs("NewElementName"));
    }

    /// <summary>
    /// Interface to remove the selected struct element
    /// </summary>
    public void DeleteSelectedElement()
    {
      SRInterfaceBlueprint.Elements.Remove(SelectedElement);
    }
    #endregion
  }
}
/**
* @}
*/