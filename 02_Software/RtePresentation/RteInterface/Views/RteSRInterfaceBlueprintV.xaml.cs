﻿/*****************************************************************************************************************************
 * @file        RteSRInterfaceBlueprintV.xaml.cs                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.12.2020                                                                                                   *
 * @brief       View of the RteSRInterfaceBlueprint                                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteInterface.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteInterface.Views
{
  /// <summary>
  /// Interaction logic for RteSRInterfaceBlueprintV.xaml
  /// </summary>
  public partial class RteSRInterfaceBlueprintV : UserControl
  {
    ViewModels.RteSRInterfaceBlueprintVM usedSRInterfaceBlueprintVM;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteSRInterfaceBlueprint"></param>
    /// <param name="rteTypes"></param>
    public RteSRInterfaceBlueprintV(RteSRInterfaceBlueprint rteSRInterfaceBlueprint, RteLib.RteTypes.RteTypes rteTypes)
    {
      usedSRInterfaceBlueprintVM = new ViewModels.RteSRInterfaceBlueprintVM(rteSRInterfaceBlueprint, rteTypes);

      InitializeComponent();

      this.DataContext = usedSRInterfaceBlueprintVM;
    }

    #region UI Actions
    /// <summary>
    /// Interface to add a new Element to list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_StoreDefinedElement_Click(object sender, RoutedEventArgs e)
    {
      usedSRInterfaceBlueprintVM.AddNewElement();

      LV_Elements.Items.Refresh();
    }

    /// <summary>
    /// Interface to delete the selected element form list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_DeleteSelectedElement_Click(object sender, RoutedEventArgs e)
    {
      usedSRInterfaceBlueprintVM.DeleteSelectedElement();

      LV_Elements.Items.Refresh();
    }
    #endregion
  }
}
/**
* @}
*/