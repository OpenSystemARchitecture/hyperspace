﻿/*****************************************************************************************************************************
 * @file        RteCSInterfaceBlueprintV.xaml.cs                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.12.2020                                                                                                   *
 * @brief       View of the RteCSInterfaceBlueprint                                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RtePresentation.RteInterface.Views
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RtePresentation.RteInterface.Views
{
  /// <summary>
  /// Interaction logic for RteCSInterfaceBlueprintV.xaml
  /// </summary>
  public partial class RteCSInterfaceBlueprintV : UserControl
  {
    ViewModels.RteCSInterfaceBlueprintVM usedCSInterfaceBlueprintVM;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="rteCSInterfaceBlueprint"></param>
    /// <param name="rteTypes"></param>
    public RteCSInterfaceBlueprintV(RteCSInterfaceBlueprint rteCSInterfaceBlueprint, RteLib.RteTypes.RteTypes rteTypes)
    {
      usedCSInterfaceBlueprintVM = new ViewModels.RteCSInterfaceBlueprintVM(rteCSInterfaceBlueprint, rteTypes);

      InitializeComponent();

      this.DataContext = usedCSInterfaceBlueprintVM;
    }



    #region UI Actions
    /// <summary>
    /// Interface to add a new Element to list 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_StoreDefinedElement_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.AddNewElement();

      LV_Elements.Items.Refresh();
    }

    /// <summary>
    /// Interface to load the selected Element to element view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_LoadSelectedElement_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.LoadSelectedElement();

      LV_Elements.Items.Refresh();
    }

    /// <summary>
    /// Interface to create new Element
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_CreateNewElement_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.CreateNewElement();

      LV_Elements.Items.Refresh();
    }

    /// <summary>
    /// Interface to delete the selected element form list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_DeleteSelectedElement_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.DeleteSelectedElement();

      LV_Elements.Items.Refresh();
    }






    /// <summary>
    /// Interface to add a new error type to list 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_StoreDefinedErrorType_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.AddNewErrorType();

      LV_FunctionErrorTypes.Items.Refresh();
    }

    /// <summary>
    /// Interface to delete the selected error type form list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_DeleteSelectedErrorType_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.DeleteSelectedErrorType();

      LV_FunctionErrorTypes.Items.Refresh();
    }















    /// <summary>
    /// Interface to add a new Argument to list 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_StoreDefinedArgument_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.AddNewArgument();

      LV_FunctionArguments.Items.Refresh();
    }

    /// <summary>
    /// Interface to delete the selected argument form list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_DeleteSelectedArgument_Click(object sender, RoutedEventArgs e)
    {
      usedCSInterfaceBlueprintVM.DeleteSelectedArgument();

      LV_FunctionArguments.Items.Refresh();
    }


    #endregion

  }
}
/**
* @}
*/