/******************************************************************************
 * @file      version.cs
 * @author    OSAR S.Reinemuth
 * @proj      RtePresentation
 * @date      Sunday, January 17, 2021
 * @version   Application v. 0.5.1.2
 * @version   Generator   v. 1.2.5.1
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("0.5.1.2")]

namespace RtePresentation.General
{
  static public class RtePresentationVersionClass
  {
    public static int major { get; set; }  //Version of the program
    public static int minor { get; set; }  //Sub version of the program
    public static int patch { get; set; }  //Debug patch of the program
    public static int build { get; set; }  //Count program builds

    static RtePresentationVersionClass()
    {
			major = 0;
			minor = 5;
			patch = 1;
			build = 2;
    }

    public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
  }
}
//!< @version 0.1.0	->	Adding complete data type view and view models.
//!< @version 0.2.0	->	Adding view of sender receiver interface blueprint.
//!< @version 0.2.1	->	Bugfix for naming changes in rte-lib.
//!< @version 0.3.0	->	Adding view of client server interface blueprint.
//!< @version 0.4.0	->	Adding view of module internal behaviors.
//!< @version 0.4.1	->	Update to RteLib version 1.0.0
//!< @version 0.5.0	->	Adding view of rte-config-connection view.
//!< @version 0.5.1	->	Bugfix for rte connection >> Setup available sender and receiver ports of an module.
