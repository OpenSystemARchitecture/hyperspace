# Hyperspace

## General
The OSAR Hyperspace tool is a tool to create / manage and configure a OSAR related project. Therefor the different OSAR compatible modules could be added to the OSAR-Hyperspace environment.

## Abbreviations:
OSAR == Open System ARchitecture

## Useful Links:
- Overall OSAR-Artifactory: http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture
- OSAR - Hyperspace releases: http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Hyperspace/
- OSAR - Documents: http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/